package com.bluebrand.beedriver.presentation.ui.main.incentives.list.active

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bluebrand.beedriver.presentation.ui.main.incentives.list.BaseIncentiveListFragment
import com.bluebrand.beedriver.presentation.ui.main.incentives.list.IncentiveListAdapter

class ActiveIncentivesFragment : BaseIncentiveListFragment() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.fetchActiveIncentives()

        setupListeners()
    }

    private fun setupListeners() {
        binding.layoutRefresh.setOnRefreshListener {
            viewModel.fetchActiveIncentives()
        }
    }

    override fun provideRecyclerAdapter(): RecyclerView.Adapter<*> {
        return IncentiveListAdapter(requireContext(), false)
    }
}