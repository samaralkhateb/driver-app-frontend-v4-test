package com.bluebrand.beedriver.presentation.ui.callback

interface OnLoadingPageListener {
    fun startLoading()
}