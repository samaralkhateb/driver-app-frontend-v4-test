package com.bluebrand.beedriver.presentation.ui.main.transactions.income.daily

import android.content.Context
import android.view.ViewGroup
import com.bluebrand.beedriver.data.model.IncomeDetails
import com.bluebrand.beedriver.data.model.IncomeIncentiveDetails
import com.bluebrand.beedriver.data.model.IncomeOrderDetails
import com.bluebrand.beedriver.databinding.ItemIncomeDetailsIncentiveBinding
import com.bluebrand.beedriver.databinding.ItemIncomeDetailsOrderBinding
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseListAdapter
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseViewHolder
import java.lang.IllegalStateException

class IncomeDetailsAdapter(context: Context) :
    BaseListAdapter<IncomeDetails, BaseViewHolder<IncomeDetails, *>>(context) {

    companion object {
        const val ORDER_TYPE = 1
        const val INCENTIVE_TYPE = 2
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<IncomeDetails, *> {
        return when (viewType) {
            ORDER_TYPE -> {
                val binding =
                    ItemIncomeDetailsOrderBinding
                        .inflate(
                            provideInflater(),
                            parent,
                            false
                        )
                return IncomeOrderDetailsViewHolder(binding) as BaseViewHolder<IncomeDetails, *>
            }

            INCENTIVE_TYPE -> {
                val binding =
                    ItemIncomeDetailsIncentiveBinding
                        .inflate(
                            provideInflater(),
                            parent,
                            false
                        )
                return IncomeIncentiveDetailsViewHolder(binding) as BaseViewHolder<IncomeDetails, *>
            }

            else -> throw IllegalStateException("Undefined view type: ($viewType)")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getData()[position]) {
            is IncomeOrderDetails -> ORDER_TYPE
            is IncomeIncentiveDetails -> INCENTIVE_TYPE
        }
    }

    class IncomeOrderDetailsViewHolder(
        binding: ItemIncomeDetailsOrderBinding
    ) : BaseViewHolder<IncomeOrderDetails, ItemIncomeDetailsOrderBinding>(binding) {
        override fun onBind(item: IncomeOrderDetails, position: Int) {
            binding.order = item
        }
    }

    class IncomeIncentiveDetailsViewHolder(
        binding: ItemIncomeDetailsIncentiveBinding
    ) : BaseViewHolder<IncomeIncentiveDetails, ItemIncomeDetailsIncentiveBinding>(binding) {
        override fun onBind(item: IncomeIncentiveDetails, position: Int) {
            binding.incentive = item
        }
    }
}