package com.bluebrand.beedriver.presentation.ui.base.view.activity

import com.bluebrand.beedriver.presentation.ui.base.view.BaseViewModel
import androidx.databinding.ViewDataBinding
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.bluebrand.beedriver.presentation.util.lifecycle.EventObserver
import com.bluebrand.beedriver.presentation.util.resourses.ProgressDialogUtil

/*
 *  VM : View Model
 *  DB : Data Binding
 *
 */
abstract class MVVMActivity<VM : BaseViewModel, DB : ViewDataBinding> : BindingActivity<DB>() {
    protected lateinit var viewModel: VM

    protected abstract val viewModelId: Int
    protected abstract val viewModelClass: Class<VM>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViewModel()
        setupBaseObservers()
    }

    private fun setupViewModel() {
        viewModel = provideViewModel()
        if (viewModelId > 0) mActivityBinding.setVariable(viewModelId, viewModel)
    }

    protected fun provideViewModel(): VM {
        return ViewModelProvider(this)[viewModelClass]
    }

    protected fun setupBaseObservers() {
        viewModel.snackBarMessage.observe(
            this,
            EventObserver { data ->
                showSnackBar(message = data,
                    action = { viewModel.refresh() })
            }
        )
        viewModel.snackBarMessageResource.observe(
            this,
            EventObserver { data ->
                showSnackBar(
                    message = getString(data),
                    action = { viewModel.refresh() })
            }
        )
        viewModel.toastMessage.observe(
            this,
            EventObserver { data -> showMessage(data) }
        )
        viewModel.toastMessageResource.observe(
            this,
            EventObserver { data -> showMessage(data) }
        )
        viewModel.hideKeyboard.observe(
            this,
            EventObserver { hideKeyboard() }
        )
        viewModel.isLoading.observe(this) { isLoading ->
            if (isLoading) {
                ProgressDialogUtil.showProgressDialog(this)
            } else {
                ProgressDialogUtil.hideProgressDialog()
            }
        }
    }
}