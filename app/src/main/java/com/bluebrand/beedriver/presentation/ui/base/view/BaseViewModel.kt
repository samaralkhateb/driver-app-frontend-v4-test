package com.bluebrand.beedriver.presentation.ui.base.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bluebrand.beedriver.presentation.exception.ExceptionFactory
import com.bluebrand.beedriver.presentation.util.lifecycle.Event
import kotlinx.coroutines.flow.FlowCollector
import java.io.IOException
import java.util.*

typealias ErrorHandler = (suspend FlowCollector<*>.(Throwable) -> Unit)

abstract class BaseViewModel : ViewModel() {
    private val _isLoading = MutableLiveData(false)
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private val _snackBarMessage = MutableLiveData<Event<String>>()
    val snackBarMessage: LiveData<Event<String>>
        get() = _snackBarMessage

    private val _snackBarMessageResource = MutableLiveData<Event<Int>>()
    val snackBarMessageResource: LiveData<Event<Int>>
        get() = _snackBarMessageResource

    private val _toastMessage = MutableLiveData<Event<String>>()
    val toastMessage: LiveData<Event<String>>
        get() = _toastMessage

    private val _toastMessageResource = MutableLiveData<Event<Int>>()
    val toastMessageResource: LiveData<Event<Int>>
        get() = _toastMessageResource

    private val _hideKeyboard = MutableLiveData<Event<Boolean>>()
    val hideKeyboard: LiveData<Event<Boolean>>
        get() = _hideKeyboard

    protected fun showMessage(message: String, inSnackBar: Boolean = false) {
        if (inSnackBar) _snackBarMessage.value = Event(message)
        else _toastMessage.value = Event(message)
    }

    protected fun showMessage(stringId: Int, inSnackBar: Boolean = false) {
        if (inSnackBar) _snackBarMessageResource.value = Event(stringId)
        else _toastMessageResource.value = Event(stringId)
    }


    protected val errorHandler: ErrorHandler =
        { error ->
            error.printStackTrace()
            stopLoading()
            showMessage(error)
        }

    protected val snackErrorHandler: ErrorHandler =
        { error ->
            error.printStackTrace()
            stopLoading()
            showMessage(error, true)
        }

    protected fun showMessage(error: Throwable, inSnackBar: Boolean = false) {
        val message = error.message
        if (message != null && error !is IOException) showMessage(message, inSnackBar)
        else {
            val stringId = ExceptionFactory.getString(error)
            if (stringId > 0) showMessage(stringId, inSnackBar)
        }
    }

    protected fun hideKeyboard() {
        _hideKeyboard.value = Event(true)
    }

    protected fun isLoading(): Boolean {
        return isLoading.value == true
    }

    protected fun startLoading() {
        _isLoading.value = true
    }

    protected fun stopLoading() {
        _isLoading.value = false
    }

    open fun refresh() {}
}