package com.bluebrand.beedriver.presentation.util.permission

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.take
import okhttp3.internal.toImmutableList

object LocationPermissionUtil {
    private val locationPermissions: Array<String> by lazy {
        arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
    }

    private val permissionListeners = mutableListOf<PermissionListener>()

    private val activityResultCallback =
        ActivityResultCallback { permissions: Map<String, Boolean> ->
            if (permissions.getOrDefault(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    false
                ) ||
                permissions.getOrDefault(
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                    false
                )
            ) {
                permissionChanged(true)
            } else {
                permissionChanged(false)
            }
        }

    fun registerForResult(activity: AppCompatActivity): ActivityResultLauncher<Array<String>> {
        return activity.registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions(),
            activityResultCallback
        )
    }

    fun registerForResult(fragment: Fragment): ActivityResultLauncher<Array<String>> {
        return fragment.registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions(),
            activityResultCallback
        )
    }

    fun requestLocationPermission(
        activity: Activity,
        permissionResultLauncher: ActivityResultLauncher<Array<String>>
    ): Flow<Boolean> {
        return callbackFlow {
            val permissionListener = PermissionListener {
                if (isPermissionGranted(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                    && (isPermissionGranted(
                        activity,
                        Manifest.permission.ACCESS_BACKGROUND_LOCATION
                    )
                            || Build.VERSION.SDK_INT < Build.VERSION_CODES.Q)
                )
                    trySend(true)
                else trySend(false)
            }

            registerListener(permissionListener)

            if (isPermissionGranted(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                if (isPermissionGranted(activity, Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                    || Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                    trySend(true)
                } else {
                    permissionResultLauncher.launch(
                        arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                    )
                }
            } else {
                permissionResultLauncher.launch(locationPermissions)
            }

            awaitClose {
                unRegisterListener(permissionListener)
            }
        }.take(1)
    }

    private fun isPermissionGranted(activity: Activity, permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            activity,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun permissionChanged(status: Boolean) {
        permissionListeners.toImmutableList().forEach { it.onPermissionResult(status) }
    }

    private fun registerListener(listener: PermissionListener) {
        permissionListeners.add(listener)
    }

    private fun unRegisterListener(listener: PermissionListener) {
        permissionListeners.remove(listener)
    }

    fun interface PermissionListener {
        fun onPermissionResult(status: Boolean)
    }
}