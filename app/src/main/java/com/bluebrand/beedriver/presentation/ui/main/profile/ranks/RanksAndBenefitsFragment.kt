package com.bluebrand.beedriver.presentation.ui.main.profile.ranks

import android.os.Bundle
import android.view.View
import com.bluebrand.beedriver.BR
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.databinding.FragmentRanksBinding
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.MVVMFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RanksAndBenefitsFragment : MVVMFragment<RanksAndBenefitsViewModel, FragmentRanksBinding>() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRanksInfoRecycler()
    }

    private fun setupRanksInfoRecycler() {
        val adapter = RankInfoListAdapter(requireContext())
        binding.rvRanksInfo.adapter = adapter
    }

    override val layoutId: Int
        get() = R.layout.fragment_ranks

    override val viewModelClass: Class<RanksAndBenefitsViewModel>
        get() = RanksAndBenefitsViewModel::class.java

    override val viewModelId: Int
        get() = BR.viewModel
}