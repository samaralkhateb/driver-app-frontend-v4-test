package com.bluebrand.beedriver.presentation.util.databinding

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bluebrand.beedriver.presentation.ui.common.list.listlivedata.event.ListEvent
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseListAdapter
import androidx.viewpager2.widget.ViewPager2
import android.widget.Spinner
import android.widget.ArrayAdapter

@BindingAdapter("android:observeListEvents")
fun <T> loadData(recyclerView: RecyclerView, listEvent: ListEvent<T>) {
    val adapter = recyclerView.adapter
    if (adapter is BaseListAdapter<*, *>) {
        listEvent.attach(adapter as BaseListAdapter<T, *>)
    }
}

@BindingAdapter("android:loadData")
fun <T> loadData(pager: ViewPager2, data: List<T>?) {
    val adapter = pager.adapter
    if (adapter is BaseListAdapter<*, *>) {
        (adapter as? BaseListAdapter<T, *>)?.submitData(data ?: emptyList())
    }
}

@BindingAdapter("android:loadData")
fun <T> loadRecyclerData(recyclerView: RecyclerView, data: List<T>?) {
    val adapter = recyclerView.adapter
    if (adapter is BaseListAdapter<*, *>) {
        (adapter as? BaseListAdapter<T, *>)?.submitData(data ?: emptyList())
    }
}

@BindingAdapter("android:loadData")
fun <T> loadSpinnerData(spinner: Spinner, data: List<T>?) {
    val adapter = spinner.adapter
    if (adapter is ArrayAdapter<*>) {
        adapter.clear()
        (adapter as? ArrayAdapter<T>)?.addAll(data ?: emptyList())
    }
}