package com.bluebrand.beedriver.presentation.ui.main.transactions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bluebrand.beedriver.data.model.TransactionsInfo
import com.bluebrand.beedriver.data.repository.driver.IDriverRepository
import com.bluebrand.beedriver.presentation.ui.base.view.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class TransactionsViewModel @Inject constructor(
    private val driverRepository: IDriverRepository
) : BaseViewModel() {
    private val _transactionsInfo = MutableLiveData<TransactionsInfo>()
    val transactionsInfo: LiveData<TransactionsInfo>
        get() = _transactionsInfo

    init {
        fetchTransactions()
    }

    override fun refresh() {
        fetchTransactions()
    }

    fun fetchTransactions() {
        if (isLoading()) return
        startLoading()

        driverRepository.getTransactions()
            .catch { error ->
                error.printStackTrace()
                stopLoading()
                showMessage(error, true)
            }
            .onEach {
                stopLoading()
                _transactionsInfo.value = it
            }
            .launchIn(viewModelScope)
    }
}