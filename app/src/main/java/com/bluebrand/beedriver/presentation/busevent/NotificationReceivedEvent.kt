package com.bluebrand.beedriver.presentation.busevent

import com.bluebrand.beedriver.data.model.Notification
import com.bluebrand.beedriver.presentation.util.bus.BusEvent

data class NotificationReceivedEvent(
    val notification: Notification
) : BusEvent()