package com.bluebrand.beedriver.presentation.ui.common.list.listlivedata

import androidx.lifecycle.MutableLiveData
import com.bluebrand.beedriver.presentation.ui.common.list.listlivedata.event.ListEvent
import com.bluebrand.beedriver.presentation.ui.common.list.listlivedata.event.AddEvent
import com.bluebrand.beedriver.presentation.ui.common.list.listlivedata.event.RemoveIndexEvent
import com.bluebrand.beedriver.presentation.ui.common.list.listlivedata.event.ReplaceDataEvent
import com.bluebrand.beedriver.presentation.ui.common.list.listlivedata.event.ClearAllEvent
import java.util.ArrayList

open class ListLiveData<T> : MutableLiveData<ListEvent<T>>() {
    val data: MutableList<T> = ArrayList()

    fun add(item: T) {
        data.add(item)
        val dataToAdd: MutableList<T> = ArrayList()
        dataToAdd.add(item)
        value = AddEvent(data, dataToAdd)
    }

    fun addAll(items: List<T>) {
        data.addAll(items)
        value = AddEvent(data, items)
    }

    fun remove(index: Int) {
        data.removeAt(index)
        value = RemoveIndexEvent(data, index)
    }

    fun replaceData(newItems: List<T>) {
        data.clear()
        data.addAll(newItems)
        value = ReplaceDataEvent(data, newItems)
    }

    fun clear() {
        data.clear()
        value = ClearAllEvent()
    }

    fun size(): Int {
        return data.size
    }
}