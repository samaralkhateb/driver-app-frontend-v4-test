package com.bluebrand.beedriver.presentation.ui.main.transactions

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.model.TransactionsInfo
import com.bluebrand.beedriver.databinding.ActivityTransactionsBinding
import com.bluebrand.beedriver.presentation.ui.base.view.activity.BindingActivity

class TransactionsActivity : BindingActivity<ActivityTransactionsBinding>() {
    companion object {
        const val KEY_TRANSACTIONS_MODEL = "key_transactions_model"

        fun getIntent(context: Context, transactionsInfo: TransactionsInfo): Intent {
            return Intent(context, TransactionsActivity::class.java).apply {
                putExtra(KEY_TRANSACTIONS_MODEL, transactionsInfo)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkExtras()
    }

    private fun checkExtras() {
        val transactionsInfo =
            intent.getSerializableExtra(KEY_TRANSACTIONS_MODEL) as? TransactionsInfo
        if (transactionsInfo != null){
            val adapter = TransactionListAdapter(this)
            mActivityBinding.layoutTransactions.rvTransactions.adapter = adapter

            adapter.submitData(transactionsInfo.transactionList)
        }
    }

    override val layoutId: Int
        get() = R.layout.activity_transactions
}