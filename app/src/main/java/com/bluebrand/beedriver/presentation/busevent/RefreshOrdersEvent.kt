package com.bluebrand.beedriver.presentation.busevent

import com.bluebrand.beedriver.presentation.util.bus.BusEvent

data class RefreshOrdersEvent(
    val ignoreIfPending: Boolean = false,
    val orderId: Long? = null
) : BusEvent()