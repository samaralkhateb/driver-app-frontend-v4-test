package com.bluebrand.beedriver.presentation.busevent

import com.bluebrand.beedriver.presentation.util.bus.BusEvent

data class OrderCanceledEvent(val orderId: Long?) : BusEvent()