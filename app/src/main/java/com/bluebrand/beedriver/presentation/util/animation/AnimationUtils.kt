package com.bluebrand.beedriver.presentation.util.animation

import android.view.animation.AnimationUtils
import android.view.animation.Interpolator
import android.widget.TextView
import com.bluebrand.beedriver.R
import kotlin.math.cos
import kotlin.math.pow

object AnimationUtils {
    fun bounceText(textView: TextView) {
        try {
            val myAnim = AnimationUtils.loadAnimation(textView.context, R.anim.bounce)

            val interpolator = BounceInterpolator(0.1, 15.0)
            myAnim.interpolator = interpolator
            textView.startAnimation(myAnim)
        } catch (e: NullPointerException) {
        }
    }

    class BounceInterpolator(amplitude: Double, frequency: Double) :
        Interpolator {
        private var mAmplitude = 1.0
        private var mFrequency = 10.0
        override fun getInterpolation(time: Float): Float {
            return (-1 * Math.E.pow(-time / mAmplitude) *
                    cos(mFrequency * time) + 1).toFloat()
        }

        init {
            mAmplitude = amplitude
            mFrequency = frequency
        }
    }
}