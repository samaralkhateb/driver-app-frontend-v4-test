package com.bluebrand.beedriver.presentation.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.bluebrand.beedriver.BuildConfig
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.model.Notification
import com.bluebrand.beedriver.data.model.mapper.NotificationMapper
import com.bluebrand.beedriver.presentation.busevent.NotificationReceivedEvent
import com.bluebrand.beedriver.presentation.ui.common.Constants
import com.bluebrand.beedriver.presentation.ui.main.MainActivity
import com.bluebrand.beedriver.presentation.util.bus.Bus
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import dagger.hilt.android.AndroidEntryPoint
import org.json.JSONObject
import javax.inject.Inject

@AndroidEntryPoint
class FirebaseService : FirebaseMessagingService() {

    @Inject
    lateinit var notificationMapper: NotificationMapper

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d(TAG, "New token: $token")
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        Log.d(TAG, "From: ${message.from}")
        Log.d(TAG, "DATA: ${message.data}")

        val type = message.data["ntf_type"]

        val notification = notificationMapper.map(
            JSONObject(message.data as Map<*, *>).toString()
        )

        if (notification != null) {
            if (type != Constants.NTF_ORDER_HIDDEN_CANCEL && type != Constants.NTF_ORDER_HIDDEN_DISPATCH) {
                displayNotification(applicationContext, notification)
            }

            if (type != Constants.NTF_INCENTIVE) {
                Bus.publishEvent(NotificationReceivedEvent(notification))
            }
        }
    }


    companion object {
        const val TAG = "MyFirebaseMsgService"

        fun displayNotification(context: Context, notification: Notification) {
            val intent = Intent(context, MainActivity::class.java).apply {
                putExtra(Constants.EXTRA_NOTIFICATION, notification)
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }

            val pendingIntentFlag =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_ONE_SHOT
                else PendingIntent.FLAG_ONE_SHOT

            val pendingIntent = PendingIntent.getActivity(
                context,
                notification.id,
                intent,
                pendingIntentFlag
            )

            val channelId = context.getString(R.string.app_name)

            val defaultSoundUri =
                Uri.parse(
                    ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + BuildConfig.APPLICATION_ID + "/" + (notification.sound?.resId ?: R.raw.incoming)
                )

            val notificationBuilder = NotificationCompat.Builder(context, channelId)
                .setGroup(channelId)
                .setSmallIcon(R.drawable.logo_app)
                .setVibrate(longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400))
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(notification.text)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

            val notificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val importance = NotificationManager.IMPORTANCE_HIGH
                notificationBuilder.priority = NotificationManager.IMPORTANCE_HIGH
                val channel = NotificationChannel(channelId, channelId, importance)
                channel.enableLights(true)
                channel.lightColor = Color.RED
                val audioAttributes = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build()
                channel.setSound(defaultSoundUri, audioAttributes)
                channel.setShowBadge(true)
                channel.lockscreenVisibility = android.app.Notification.VISIBILITY_PUBLIC
                notificationManager.createNotificationChannel(channel)
            }

            notificationManager.notify(notification.id, notificationBuilder.build())
        }

    }
}