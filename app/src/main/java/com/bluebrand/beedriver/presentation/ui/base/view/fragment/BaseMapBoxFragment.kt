package com.bluebrand.beedriver.presentation.ui.base.view.fragment

import com.bluebrand.beedriver.R
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.bluebrand.beedriver.presentation.ui.base.view.BaseViewModel
import com.bluebrand.beedriver.presentation.util.resourses.BitmapUtil
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.*
import com.mapbox.mapboxsdk.style.layers.Property

abstract class BaseMapBoxFragment<VM : BaseViewModel, DB : ViewDataBinding>
    : MVVMFragment<VM, DB>(), OnMapReadyCallback {

    protected var map: MapboxMap? = null
    protected var style: Style? = null

    protected abstract val mapView: MapView

    private var symbolManager: SymbolManager? = null
    private var lineManager: LineManager? = null
    private var circleManager: CircleManager? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Mapbox.getInstance(requireContext(), getString(R.string.mapbox_access_token))

        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mapView.getMapAsync(this)
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        map = mapboxMap
        mapboxMap.setStyle(Style.MAPBOX_STREETS, this::onStyleReady)
    }

    protected open fun onStyleReady(style: Style) {
        this.style = style
        registerIcons(style)
        initManagers()
    }

    protected open fun registerIcons(style: Style) {
        BitmapUtil.bitmapFromDrawableRes(requireContext(), R.drawable.ic_pin_map)?.let {
            style.addImage(DEFAULT_MARKER, it)
        }
    }

    private fun initManagers() {
        val map = map
        val style = style

        if (map != null && style != null) {
            symbolManager = SymbolManager(mapView, map, style)
            symbolManager?.iconAllowOverlap = true

            circleManager = CircleManager(mapView, map, style, symbolManager?.layerId)
            lineManager = LineManager(mapView, map, style, circleManager?.layerId)
        }
    }

    protected fun moveCamera(latitude: Double, longitude: Double, zoom: Float = DEFAULT_ZOOM) {
        moveCamera(LatLng(latitude, longitude), zoom)
    }

    protected fun moveCamera(latLng: LatLng, zoom: Float = DEFAULT_ZOOM) {
        map?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom.toDouble()))
    }

    protected fun addMarker(
        latitude: Double, longitude: Double,
        iconImageName: String = DEFAULT_MARKER
    ): Symbol? {
        val markerOptions = SymbolOptions()
            .withLatLng(LatLng(latitude, longitude))
            .withIconImage(iconImageName)

        return symbolManager?.create(markerOptions)
    }

    protected fun removeMarker(marker: Symbol) {
        symbolManager?.delete(marker)
    }

    protected fun drawRoute(latLngList: List<LatLng>) {
        lineManager?.deleteAll()
        circleManager?.deleteAll()

        val lineOption = LineOptions()
            .withLineColor("#8F8F8F")
            .withLineJoin(Property.LINE_JOIN_ROUND)
            .withLineWidth(5.0f)
            .withLatLngs(latLngList)

        lineManager?.create(lineOption)

        if (latLngList.isNotEmpty()) {
            drawCircle(latLngList[0])
        }
        if (latLngList.size > 1) {
            drawCircle(latLngList[latLngList.size - 1])
        }
    }

    protected fun drawCircle(latLng: LatLng) {
        val circleOptions = CircleOptions()
            .withCircleRadius(6.0f)
            .withCircleColor("#8F8F8F")
            .withLatLng(latLng)

        circleManager?.create(circleOptions)
    }

    protected open fun moveCamera(vararg latLngList: LatLng) {
        val latLngBoundsBuilder = LatLngBounds.Builder()
        for (latLng in latLngList) latLngBoundsBuilder.include(latLng)
        map?.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBoundsBuilder.build(), 200))
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    companion object {
        var DEFAULT_ZOOM = 16f
        protected var DEFAULT_MARKER = "marker_default"
    }
}