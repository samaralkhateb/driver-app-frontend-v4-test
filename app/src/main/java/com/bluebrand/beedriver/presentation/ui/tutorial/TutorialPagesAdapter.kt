package com.bluebrand.beedriver.presentation.ui.tutorial

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.databinding.SlideLayoutBinding
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseListAdapter
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseViewHolder
import com.bluebrand.beedriver.presentation.ui.callback.OnItemClickListener

class TutorialPagesAdapter(
    context: Context,
    private val onStartButtonClicked: OnItemClickListener<View>
) :
    BaseListAdapter<TutorialPageData, TutorialPagesAdapter.TutorialPageViewHolder>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TutorialPageViewHolder {
        val binding: SlideLayoutBinding =
            DataBindingUtil.inflate(
                provideInflater(),
                R.layout.slide_layout,
                parent,
                false
            )

        return TutorialPageViewHolder(binding)
    }

    inner class TutorialPageViewHolder(binding: SlideLayoutBinding) :
        BaseViewHolder<TutorialPageData, SlideLayoutBinding>(binding) {
        override fun onBind(item: TutorialPageData, position: Int) {
            binding.btnStart.visibility = if (item.showStartButton) View.VISIBLE else View.GONE
            binding.tvHading.setText(item.titleResId)
            binding.tvDescription.setText(item.messageResId)
            binding.ivImage.setImageResource(item.imageResId)

            binding.btnStart.setOnClickListener { onStartButtonClicked.onClick(it, position) }
        }
    }
}

data class TutorialPageData(
    val titleResId: Int,
    val messageResId: Int,
    val imageResId: Int,
    val showStartButton: Boolean = false
)