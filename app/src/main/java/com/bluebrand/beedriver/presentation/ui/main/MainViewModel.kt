package com.bluebrand.beedriver.presentation.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.exception.StatusSyncException
import com.bluebrand.beedriver.data.model.DriverInfo
import com.bluebrand.beedriver.data.model.Notification
import com.bluebrand.beedriver.data.repository.driver.IDriverRepository
import com.bluebrand.beedriver.data.repository.location.ILocationRepository
import com.bluebrand.beedriver.data.repository.socket.ISocketRepository
import com.bluebrand.beedriver.presentation.ui.base.view.BaseViewModel
import com.bluebrand.beedriver.presentation.util.lifecycle.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val driverRepository: IDriverRepository,
    private val locationRepository: ILocationRepository,
    private val socketRepository: ISocketRepository
) : BaseViewModel() {
    private val _driverStatus = MutableLiveData<Boolean>()
    val driverStatus: LiveData<Boolean> = _driverStatus

    private val _driverInfo = MutableLiveData<DriverInfo>()
    val driverInfo: LiveData<DriverInfo> = _driverInfo

    private val _gpsStatus = MutableLiveData<Boolean>()
    val gpsStatus: LiveData<Boolean> = _gpsStatus

    private val _socketConnectionStatus = MutableLiveData<Boolean?>()
    val socketConnectionStatus: LiveData<Boolean?> = _socketConnectionStatus

    private val _socketNotification = MutableLiveData<Event<Notification>>()
    val socketNotification: LiveData<Event<Notification>> = _socketNotification

    var isMapDownloaded: Boolean
        get() = driverRepository.isMapDownloaded()
        set(value) = driverRepository.setMapDownloaded(value)

    init {
        fetchDriverInfo()
    }

    fun fetchGpsStatus() {
        locationRepository
            .onGpsStatusChanged()
            .onEach(_gpsStatus::setValue)
            .launchIn(viewModelScope)

        locationRepository
            .getLocationUpdates(10000)
            .launchIn(viewModelScope)
    }

    fun toggleStatus() {
        if (isLoading()) return
        startLoading()

        val newStatus = !(driverStatus.value ?: false)

        _driverStatus.value = newStatus

        driverRepository.setStatus(newStatus)
            .catch { error ->
                errorHandler.invoke(this, error)
                if (error is StatusSyncException) {
                    _driverStatus.value = newStatus
                } else {
                    _driverStatus.value = !newStatus
                }
            }
            .onEach { result ->
                stopLoading()
                val message = result.message
                if (result.isSuccessful()) {
                    if (message != null) showMessage(message)
                    else showMessage(R.string.msg_staus_updated_successfully)
                } else {
                    _driverStatus.value = !newStatus
                    if (message != null) showMessage(message)
                    else showMessage(R.string.msg_something_went_wrong)
                }
            }
            .launchIn(viewModelScope)
    }

    private fun fetchDriverInfo() {
        driverRepository.getDriverInfo()
            .onEach { info ->
                if (info != null) {
                    _driverStatus.value = info.isOnline
                    _driverInfo.value = info
                }
            }
            .launchIn(viewModelScope)
    }

    fun connectToSocket() {
        socketRepository.getConnectionStatus()
            .dropWhile { _socketConnectionStatus.value == null && it }
            .distinctUntilChanged()
            .debounce { if (it) 0L else 3 * 1000L }
            .filterNot { it == _socketConnectionStatus.value || (_socketConnectionStatus.value == null && it) }
            .onEach {
                _socketConnectionStatus.value = it
            }
            .catch { _socketConnectionStatus.value = false }
            .launchIn(viewModelScope)
    }

    fun listenForSocketNotifications() {
        socketRepository.socketNotificationReceived()
            .onEach {
                _socketNotification.value = Event(it)
            }
            .catch { e -> e.printStackTrace() }
            .launchIn(viewModelScope)
    }

    fun listenForDriverInformationUpdate() {
        socketRepository.driverInfoUpdated()
            .onEach {
                _driverInfo.value = it
            }
            .catch { e -> e.printStackTrace() }
            .launchIn(viewModelScope)
    }
}