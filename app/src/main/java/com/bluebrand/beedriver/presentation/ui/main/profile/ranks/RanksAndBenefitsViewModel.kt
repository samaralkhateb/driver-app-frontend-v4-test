package com.bluebrand.beedriver.presentation.ui.main.profile.ranks

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bluebrand.beedriver.data.model.RankInfo
import com.bluebrand.beedriver.data.repository.driver.DriverRepositoryImpl
import com.bluebrand.beedriver.data.repository.driver.IDriverRepository
import com.bluebrand.beedriver.presentation.ui.base.view.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class RanksAndBenefitsViewModel @Inject constructor(
    private val driverRepository: IDriverRepository
) : BaseViewModel() {
    private val _rankInfoList = MutableLiveData<List<RankInfo>>()
    val rankInfoList: LiveData<List<RankInfo>> = _rankInfoList

    init {
        fetchRanks()
    }

    fun fetchRanks(){
        if (isLoading()) return
        startLoading()

        driverRepository.getRankGuide()
            .catch (errorHandler)
            .onEach {
                stopLoading()
                _rankInfoList.value = it
            }
            .launchIn(viewModelScope)
    }
}