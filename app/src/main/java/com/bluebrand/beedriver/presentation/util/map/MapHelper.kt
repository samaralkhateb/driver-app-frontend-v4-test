package com.bluebrand.beedriver.presentation.util.map

import com.mapbox.mapboxsdk.geometry.LatLng
import org.osmdroid.bonuspack.routing.Road
import org.osmdroid.bonuspack.routing.RoadManager

fun Road.toLatLngList(): List<LatLng> {
    val roadOverlay = RoadManager.buildRoadOverlay(this)
    return roadOverlay.actualPoints.map { LatLng(it.latitude, it.longitude) }
}