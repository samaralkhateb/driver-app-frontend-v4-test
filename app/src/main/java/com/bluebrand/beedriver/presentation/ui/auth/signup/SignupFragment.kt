package com.bluebrand.beedriver.presentation.ui.auth.signup

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.bluebrand.beedriver.BR
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.model.City
import com.bluebrand.beedriver.data.model.Vehicle
import com.bluebrand.beedriver.databinding.FragmentSignupBinding
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.MVVMFragment
import com.bluebrand.beedriver.presentation.util.lifecycle.EventObserver
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class SignupFragment : MVVMFragment<SignupViewModel, FragmentSignupBinding>() {

    private lateinit var navController: NavController

    private lateinit var citesAdapter: ArrayAdapter<City>
    private lateinit var vehiclesAdapter: ArrayAdapter<Vehicle>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        setupCitesSpinner()
        setupVehicleTypesSpinner()

        registerObservers()
        setupClickListeners()

        viewModel.fetchData()
    }

    private fun setupCitesSpinner() {
        citesAdapter = SelectionSpinnerAdapter(R.string.hint_select_city, requireContext())
        binding.spinnerCity.adapter = citesAdapter
    }


    private fun setupVehicleTypesSpinner() {
        vehiclesAdapter = SelectionSpinnerAdapter(R.string.hint_select_vehicle, requireContext())
        binding.spinnerVehicleType.adapter = vehiclesAdapter
    }


    private fun registerObservers() {
        viewModel.dataError.observe(
            viewLifecycleOwner,
            EventObserver { message ->
                showErrorSnackBar(
                    message.takeUnless { it.isEmpty() }
                        ?: getString(R.string.please_check_your_internet)
                )
            }
        )

        viewModel.signupStatus.observe(
            viewLifecycleOwner,
            EventObserver { result ->
                if (result) {
                    showMessage(R.string.msg_signed_up_successfully)
                    navigateToLoginFragment()
                } else showMessage(R.string.msg_something_went_wrong)
            }
        )
    }

    private fun showErrorSnackBar(message: String) {
        Snackbar.make(binding.buttonSinup, message, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.lbl_refresh) { viewModel.fetchData() }
            .setActionTextColor(ContextCompat.getColor(requireContext(), R.color.white))
            .show()
    }

    private fun setupClickListeners() {
        binding.buttonSignIn.setOnClickListener { navigateToLoginFragment() }

        binding.buttonSinup.setOnClickListener {
            if (citesAdapter.isEmpty || vehiclesAdapter.isEmpty
                || binding.spinnerCity.selectedItemPosition == 0
                || binding.spinnerVehicleType.selectedItemPosition == 0) {
                showMessage(R.string.msg_please_fill_all_fields)
            } else {
                viewModel.signup(
                    citesAdapter.getItem(binding.spinnerCity.selectedItemPosition)!!.id,
                    vehiclesAdapter.getItem(binding.spinnerVehicleType.selectedItemPosition)!!.id
                )
            }
        }
    }

    private fun navigateToLoginFragment() {
        navController.navigate(
            SignupFragmentDirections
                .actionRegisterFragmentToLoginFragment()
        )
    }

    override val layoutId: Int
        get() = R.layout.fragment_signup

    override val viewModelClass: Class<SignupViewModel>
        get() = SignupViewModel::class.java

    override val viewModelId: Int
        get() = BR.viewModel
}