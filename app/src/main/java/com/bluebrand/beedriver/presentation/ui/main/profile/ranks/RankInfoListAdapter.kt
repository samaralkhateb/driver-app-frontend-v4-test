package com.bluebrand.beedriver.presentation.ui.main.profile.ranks

import android.content.Context
import android.view.ViewGroup
import com.bluebrand.beedriver.data.model.RankInfo
import com.bluebrand.beedriver.databinding.ItemRankCardBinding
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseListAdapter
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseViewHolder

class RankInfoListAdapter(context: Context) :
    BaseListAdapter<RankInfo, RankInfoListAdapter.RankInfoViewHolder>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RankInfoViewHolder {
        val binding = ItemRankCardBinding.inflate(provideInflater(), parent, false)
        return RankInfoViewHolder(binding)
    }

    class RankInfoViewHolder(
        binding: ItemRankCardBinding
    ) : BaseViewHolder<RankInfo, ItemRankCardBinding>(binding) {
        override fun onBind(item: RankInfo, position: Int) {
            binding.rankInfo = item
        }
    }
}