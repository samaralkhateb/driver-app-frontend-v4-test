package com.bluebrand.beedriver.presentation.ui.main.profile

import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.bluebrand.beedriver.BR
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.databinding.FragmentProfileBinding
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.MVVMFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProfileFragment : MVVMFragment<ProfileViewModel, FragmentProfileBinding>() {
    private lateinit var navController: NavController

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        setupClickListeners()
        registerObservers()
    }

    private fun setupClickListeners() {
        binding.buttonViewMore.setOnClickListener {
            navController.navigate(
                ProfileFragmentDirections
                    .actionProfileFragmentToRanksAndBenefitsFragment()
            )
        }

        binding.layoutArCr.setOnClickListener {
            navController.navigate(
                ProfileFragmentDirections
                    .actionProfileFragmentToArCrHistoryFragment()
            )
        }

        binding.buttonSettings.setOnClickListener {
            navController.navigate(
                ProfileFragmentDirections
                    .actionProfileFragmentToSettingFragment()
            )
        }

        binding.layoutDriverRank.setOnClickListener {
            if (viewModel.profileData.value?.driverInfo?.isVip != true) {
                navController.navigate(
                    ProfileFragmentDirections
                        .actionProfileFragmentToRanksAndBenefitsFragment()
                )
            }
        }
    }

    private fun registerObservers() {
        viewModel.profileData.observe(viewLifecycleOwner) { data ->
            val deliveredOrders = data?.rankDetails?.deliveredOrders?.toFloat() ?: 0f
            val totalOrders = deliveredOrders + (data?.rankDetails?.remainingOrders ?: 0)
            val progress = (deliveredOrders / totalOrders) * 100

            binding.progressRank.value = progress.toInt()
        }
    }

    override val layoutId: Int
        get() = R.layout.fragment_profile

    override val viewModelClass: Class<ProfileViewModel>
        get() = ProfileViewModel::class.java

    override val viewModelId: Int
        get() = BR.viewModel
}