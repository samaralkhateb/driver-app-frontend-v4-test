package com.bluebrand.beedriver.presentation.ui.splash

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.databinding.ActivitySplashBinding
import com.bluebrand.beedriver.presentation.ui.base.view.activity.MVVMActivity
import com.bluebrand.beedriver.presentation.ui.main.MainActivity
import com.bluebrand.beedriver.presentation.ui.tutorial.TutorialActivity
import com.bluebrand.beedriver.presentation.util.lifecycle.EventObserver
import dagger.hilt.android.AndroidEntryPoint


@SuppressLint("CustomSplashScreen")
@AndroidEntryPoint
class SplashActivity : MVVMActivity<SplashScreenViewModel, ActivitySplashBinding>() {
    companion object {
        private const val DURATION = 1000L
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupObservers()

        Handler(Looper.getMainLooper()).postDelayed({
            viewModel.checkIfLoggedIn()
        }, DURATION)
    }

    private fun setupObservers(){
        viewModel.isLoggedIn.observe(
            this,
            EventObserver{isLoggedIn ->
                if (isLoggedIn) navigateToMainActivity()
                else navigateToTutorialActivity()
            }
        )
    }

    private fun navigateToTutorialActivity(){
        startActivity(
            Intent(this, TutorialActivity::class.java)
        )
    }

    private fun navigateToMainActivity(){
        startActivity(
            Intent(this, MainActivity::class.java)
        )
    }

    override val layoutId: Int
        get() = R.layout.activity_splash
    override val viewModelId: Int
        get() = 0
    override val viewModelClass: Class<SplashScreenViewModel>
        get() = SplashScreenViewModel::class.java
}