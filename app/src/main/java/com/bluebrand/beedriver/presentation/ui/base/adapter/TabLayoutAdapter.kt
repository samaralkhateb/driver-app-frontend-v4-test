package com.bluebrand.beedriver.presentation.ui.base.adapter

import android.content.Context
import com.google.android.material.tabs.TabLayout
import android.view.LayoutInflater

/**
 * @param <T> View Data type
</T> */
abstract class TabLayoutAdapter<T>(
    protected val context: Context,
    protected val tabLayout: TabLayout
) {
    private val _data: MutableList<T> = mutableListOf()
    val data: List<T>
        get() = _data

    abstract fun onBindTab(tab: TabLayout.Tab, position: Int)
    abstract fun onCreateTab(inflater: LayoutInflater, position: Int): TabLayout.Tab

    val itemCount: Int
        get() = data.size

    fun setData(data: List<T>) {
        this._data.clear()
        this._data.addAll(data)
        notifyDataSetChanged()
    }

    protected fun createTabs() {
        val inflater = LayoutInflater.from(context)
        for (i in 0 until itemCount) {
            val tab = onCreateTab(inflater, i)
            onBindTab(tab, i)
            tabLayout.addTab(tab)
        }
    }

    fun notifyDataSetChanged() {
        tabLayout.removeAllTabs()
        createTabs()
    }
}