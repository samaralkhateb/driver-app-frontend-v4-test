package com.bluebrand.beedriver.presentation.util.navigation

import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.ActivityResultRegistry
import androidx.activity.result.contract.ActivityResultContract
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner

class ActivityResultObserver<Input, Result>(
    private val activityResultRegistry: ActivityResultRegistry,
    private val activityResultContract: ActivityResultContract<Input, Result>,
    private val key: String
) : DefaultLifecycleObserver {

    private var onActivityResult: OnActivityResult<Result>? = null
    private lateinit var activityResultLauncher: ActivityResultLauncher<Input>

    override fun onCreate(owner: LifecycleOwner) {
        activityResultLauncher = activityResultRegistry.register(
            key,
            owner,
            activityResultContract,
            ::callOnActivityResult
        )
    }

    fun launch(
        input: Input,
        onActivityResult: OnActivityResult<Result>
    ) {
        this.onActivityResult = onActivityResult
        activityResultLauncher.launch(input)
    }

    private fun callOnActivityResult(result: Result) {
         onActivityResult?.onActivityResult(result)
    }

    fun interface OnActivityResult<O> {
        /**
         * Called after receiving a result from the target activity
         */
        fun onActivityResult(result: O)
    }
}