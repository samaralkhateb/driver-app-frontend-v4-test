package com.bluebrand.beedriver.presentation.ui.main.transactions.income.daily

import androidx.recyclerview.widget.RecyclerView
import com.bluebrand.beedriver.presentation.ui.main.transactions.income.base.BaseIncomeFragment
import com.bluebrand.beedriver.presentation.ui.main.transactions.income.base.IncomeInfoListAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DailyIncomeHistoryFragment : BaseIncomeFragment<DailyIncomeHistoryViewModel>() {

    override fun provideRecyclerAdapter(): RecyclerView.Adapter<*> {
        return DailyIncomeListAdapter(requireContext())
    }

    override val viewModelClass: Class<DailyIncomeHistoryViewModel>
        get() = DailyIncomeHistoryViewModel::class.java
}