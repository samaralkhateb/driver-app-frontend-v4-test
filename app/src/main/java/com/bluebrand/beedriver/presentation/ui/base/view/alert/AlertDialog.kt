package com.bluebrand.beedriver.presentation.ui.base.view.alert

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.RelativeLayout
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.databinding.DialogCommonBinding

class AlertDialog(
    private val context: Context
) : IAlertDialog {

    override fun buildDialog(
        title: String?,
        message: String,
        positiveText: String?,
        negativeText: String?,
        onPositiveClickListener: DialogInterface.OnClickListener?,
        onNegativeClickListener: DialogInterface.OnClickListener?,
        cancelable: Boolean,
        tintColorId: Int
    ): Dialog {
        val builder = AlertDialog.Builder(
            context, R.style.AppTheme_Dialog_Alert
        )

        val viewBinding: DialogCommonBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_common,
            null,
            false
        )

        builder.setView(viewBinding.root)
        builder.setCancelable(cancelable)

        val alertDialog = builder.create()
        alertDialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.blackTransparent
                )
            )
        )
        alertDialog.window?.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        alertDialog.window?.setLayout(
            RelativeLayout.LayoutParams.MATCH_PARENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        alertDialog.window?.setGravity(Gravity.CENTER)

        if (title != null) {
            viewBinding.txtTitle.text = title
        } else {
            viewBinding.txtTitle.visibility = View.GONE
        }

        viewBinding.txtNote.text = message

        viewBinding.btnConfirm.text = positiveText ?: context.getString(R.string.lbl_done)
        viewBinding.btnCancel.text = negativeText ?: context.getString(R.string.lbl_cancel)

        viewBinding.btnCancel.visibility = if (cancelable) View.VISIBLE else View.GONE


        viewBinding.btnConfirm.setOnClickListener {
            onPositiveClickListener?.onClick(
                alertDialog,
                AlertDialog.BUTTON_POSITIVE
            )
            alertDialog.dismiss()
        }


        viewBinding.btnCancel.setOnClickListener {
            onNegativeClickListener?.onClick(
                alertDialog,
                AlertDialog.BUTTON_NEGATIVE
            )
            alertDialog.dismiss()
        }

        if (tintColorId == R.color.colorPrimary) {
            viewBinding.txtNote.setTextColor(ContextCompat.getColor(context, R.color.darkGray))
        }else{
            viewBinding.txtNote.setTextColor(ContextCompat.getColor(context, tintColorId))
        }

        viewBinding.btnConfirm.setBackgroundColor(ContextCompat.getColor(context, tintColorId))

        return alertDialog
    }

    override fun showDialogMessage(
        title: String?,
        message: String,
        positiveText: String?,
        negativeText: String?,
        onPositiveClickListener: DialogInterface.OnClickListener?,
        onNegativeClickListener: DialogInterface.OnClickListener?,
        cancelable: Boolean,
        tintColorId: Int
    ) {
        val alertDialog =
            buildDialog(
                title,
                message,
                positiveText,
                negativeText,
                onPositiveClickListener,
                onNegativeClickListener,
                cancelable,
                tintColorId
            )
        alertDialog.show()
    }

    override fun showDialogMessage(
        titleId: Int?,
        messageId: Int,
        positiveText: String?,
        negativeText: String?,
        onPositiveClickListener: DialogInterface.OnClickListener?,
        onNegativeClickListener: DialogInterface.OnClickListener?,
        cancelable: Boolean,
        tintColorId: Int
    ) {
        showDialogMessage(
            if (titleId != null) context.getString(titleId) else "",
            context.getString(messageId),
            positiveText,
            negativeText,
            onPositiveClickListener,
            onNegativeClickListener,
            cancelable
        )
    }

}
