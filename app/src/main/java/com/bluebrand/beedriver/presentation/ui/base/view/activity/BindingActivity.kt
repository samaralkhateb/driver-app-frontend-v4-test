package com.bluebrand.beedriver.presentation.ui.base.view.activity

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.bluebrand.beedriver.R

abstract class BindingActivity<DB : ViewDataBinding> : BaseActivity() {
    protected lateinit var mActivityBinding: DB

    override fun setView() {
        mActivityBinding = DataBindingUtil.setContentView(this, layoutId)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDataBinding()
    }

    open fun showSnackBar(
        message: String,
        actionText: Int = R.string.lbl_refresh,
        action: View.OnClickListener
    ) {
        showSnackBar(
            mActivityBinding.root,
            message,
            getString(actionText),
            action
        )
    }

    private fun setupDataBinding() {
        mActivityBinding.lifecycleOwner = this
        mActivityBinding.executePendingBindings()
    }
}