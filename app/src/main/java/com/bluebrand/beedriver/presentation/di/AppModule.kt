package com.bluebrand.beedriver.presentation.di

import android.app.Service
import android.content.Context
import android.content.SharedPreferences
import android.location.LocationManager
import com.bluebrand.beedriver.data.provider.local.sharedpreferences.LiveSharedPreferences
import com.bluebrand.beedriver.data.provider.local.sharedpreferences.LiveSharedPreferencesImpl
import com.bluebrand.beedriver.data.provider.remote.api.ExceptionFactory
import com.bluebrand.beedriver.data.source.local.cache.AppCache
import com.bluebrand.beedriver.data.source.local.cache.SharedPreferencesCache
import com.bluebrand.beedriver.data.util.constants.RemoteConstants
import com.bluebrand.beedriver.data.util.constants.SharedPreferencesKeys
import com.bluebrand.beedriver.data.util.network.ResponseExceptionFactory
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.client.SocketOptionBuilder
import org.osmdroid.bonuspack.routing.OSRMRoadManager
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Singleton
    @Provides
    fun provideSharedPreferences(@ApplicationContext context: Context): SharedPreferences {
        return context.getSharedPreferences(
            SharedPreferencesKeys.APP_SHARED_KEY,
            Context.MODE_PRIVATE
        )
    }

    @Singleton
    @Provides
    fun provideLiveSharedPreferences(liveSharedPreferences: LiveSharedPreferencesImpl): LiveSharedPreferences {
        return liveSharedPreferences
    }

    @Singleton
    @Provides
    fun provideAppCache(sharedPreferencesCache: SharedPreferencesCache): AppCache {
        return sharedPreferencesCache
    }

    @Singleton
    @Provides
    fun provideExceptionFactory(exceptionFactory: ResponseExceptionFactory): ExceptionFactory {
        return exceptionFactory
    }

    @Provides
    fun provideFusedLocationClient(@ApplicationContext context: Context): FusedLocationProviderClient {
        return LocationServices.getFusedLocationProviderClient(context)
    }

    @Provides
    fun provideLocationManager(@ApplicationContext context: Context): LocationManager {
        return context.getSystemService(Service.LOCATION_SERVICE) as LocationManager
    }

    @Provides
    fun provideOSRMRoadManager(
        @ApplicationContext context: Context,
        appCache: AppCache
    ): OSRMRoadManager {
        return OSRMRoadManager(context).apply {
            setService(RemoteConstants.OSRM.ROUTE_HOST)
            appCache.getSavedToken()?.let { addRequestOption("Authorization=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9") }
        }
    }

    @Provides
    fun provideSocketClient(appCache: AppCache): Socket {
        val option =
            SocketOptionBuilder
                .builder()
                .setExtraHeaders(
                    buildMap { put("token", listOf(appCache.getSavedToken())) }
                )
                .build()
        return IO.socket(RemoteConstants.Socket.SOCKET_HOST, option)
    }
}