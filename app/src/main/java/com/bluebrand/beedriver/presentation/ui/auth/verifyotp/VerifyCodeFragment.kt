package com.bluebrand.beedriver.presentation.ui.auth.verifyotp

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.lifecycle.ViewModelStoreOwner
import com.bluebrand.beedriver.BR
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.databinding.FragmentVerifyOtpBinding
import com.bluebrand.beedriver.presentation.ui.auth.login.LoginViewModel
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.MVVMFragment
import com.bluebrand.beedriver.presentation.ui.main.MainActivity
import com.bluebrand.beedriver.presentation.util.lifecycle.EventObserver
import dagger.hilt.android.AndroidEntryPoint
import java.util.concurrent.TimeUnit


@AndroidEntryPoint
class VerifyCodeFragment : MVVMFragment<LoginViewModel, FragmentVerifyOtpBinding>() {
    private var timer: CountDownTimer? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupClickListeners()
        setupObservers()
    }

    private fun setupClickListeners() {
        binding.buttonResendOtp.setOnClickListener { viewModel.resendOtp() }
        binding.buttonVerify.setOnClickListener { viewModel.verifyOtp(Build.VERSION.RELEASE) }
    }

    private fun setupObservers() {
        viewModel.loginStatus.observe(
            viewLifecycleOwner,
            EventObserver { result ->
                if (result) {
                    navigateToHomeActivity()
                }
            }
        )

        viewModel.otpResend.observe(
            viewLifecycleOwner,
            EventObserver { result ->
                if (result) {
                    startTimer()
                }
            }
        )
    }

    private fun navigateToHomeActivity() {
        activity?.finishAffinity()
        startActivity(Intent(activity, MainActivity::class.java))
    }

    private fun startTimer() {
        val resendOtpButton = binding.buttonResendOtp
        val timeRemainsTextView = binding.tvTimeRemains

        resendOtpButton.visibility = View.GONE
        timeRemainsTextView.visibility = View.VISIBLE

        timer = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val totalSeconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)
                val minutes = (totalSeconds % 3600) / 60
                val seconds = totalSeconds % 60
                val timeString = String.format("%02d:%02d", minutes, seconds)
                timeRemainsTextView.text =
                    getString(R.string.lbl_resendIn)
                        .plus(" ")
                        .plus(timeString)
                        .plus(" ")
                        .plus(getString(R.string.lbl_sec))
            }

            override fun onFinish() {
                resendOtpButton.visibility = View.VISIBLE
                timeRemainsTextView.visibility = View.GONE
            }
        }

        timer?.start()
    }

    override fun onDestroyView() {
        timer?.cancel()
        super.onDestroyView()
    }

    override val layoutId: Int
        get() = R.layout.fragment_verify_otp
    override val viewModelClass: Class<LoginViewModel>
        get() = LoginViewModel::class.java
    override val viewModelOwner: ViewModelStoreOwner
        get() = requireActivity()
    override val viewModelId: Int
        get() = BR.viewModel
}