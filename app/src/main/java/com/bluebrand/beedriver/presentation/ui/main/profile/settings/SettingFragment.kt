package com.bluebrand.beedriver.presentation.ui.main.profile.settings

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.bluebrand.beedriver.BR
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.databinding.FragmentSettingsBinding
import com.bluebrand.beedriver.presentation.busevent.HideBottomBarEvent
import com.bluebrand.beedriver.presentation.busevent.ShowBottomBarEvent
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.MVVMFragment
import com.bluebrand.beedriver.presentation.ui.main.MainActivity
import com.bluebrand.beedriver.presentation.util.application.AppUtil
import com.bluebrand.beedriver.presentation.util.bus.Bus
import com.bluebrand.beedriver.presentation.util.locale.LanguagesCode
import com.bluebrand.beedriver.presentation.util.locale.LocaleManager


class SettingFragment : MVVMFragment<SettingsViewModel, FragmentSettingsBinding>() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()
        setupClickListeners()
        setupLanguagesSelection()
    }

    private fun setupView() {
        binding.txtAppVersion.text = AppUtil.getAppVersionName(requireContext())
        if (LocaleManager.getInstance(requireContext()).savedLanguage == LanguagesCode.English) {
            binding.ckbEnglish.isChecked = true
            binding.ckbArabic.isChecked = false
        } else {
            binding.ckbArabic.isChecked = true
            binding.ckbEnglish.isChecked = false
        }
    }

    private fun setupClickListeners() {
        binding.buttonSave.setOnClickListener {
            if (binding.ckbArabic.isChecked) {
                LocaleManager
                    .getInstance(requireContext())
                    .setNewLocale(requireContext(), LanguagesCode.Arabic)
            } else {
                LocaleManager
                    .getInstance(requireContext())
                    .setNewLocale(requireContext(), LanguagesCode.English)
            }
            activity?.finishAffinity()
            startActivity(Intent(context, MainActivity::class.java))
        }
    }


    private fun setupLanguagesSelection() {
        binding.ckbArabic.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.ckbEnglish.isChecked = false
            }
        }

        binding.ckbEnglish.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.ckbArabic.isChecked = false
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Bus.publishEvent(HideBottomBarEvent)
    }

    override fun onDetach() {
        super.onDetach()
        Bus.publishEvent(ShowBottomBarEvent)
    }

    override val layoutId: Int
        get() = R.layout.fragment_settings

    override val viewModelClass: Class<SettingsViewModel>
        get() = SettingsViewModel::class.java

    override val viewModelId: Int
        get() = BR.viewModel
}