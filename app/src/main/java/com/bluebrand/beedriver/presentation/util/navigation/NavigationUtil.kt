package com.bluebrand.beedriver.presentation.util.navigation

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.annotation.RequiresApi

object NavigationUtil {
    fun dialIntent(phoneNumber: String): Intent {
        return Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null))
    }

    fun mobileNetworksIntent(): Intent {
        return Intent(Settings.ACTION_WIRELESS_SETTINGS)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun batteryOptimizationIntent(): Intent {
        return Intent(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS)
    }
}