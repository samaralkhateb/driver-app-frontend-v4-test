package com.bluebrand.beedriver.presentation.ui.main.transactions.income

import android.os.Bundle
import android.view.View
import com.bluebrand.beedriver.BR
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.databinding.FragmentIncomeBreakDownBinding
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.MVVMFragment
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class IncomeBreakdownFragment : MVVMFragment<IncomeBreakdownViewModel, FragmentIncomeBreakDownBinding>() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupIncentivesPager()
    }

    private fun setupIncentivesPager() {
        val pagerAdapter = IncomePagerAdapter(this)
        binding.viewPager.adapter = pagerAdapter

        TabLayoutMediator(
            binding.tabLayout,
            binding.viewPager
        ) { tab, position ->
            tab.setText(pagerAdapter.getTitle(position))
        }.attach()
    }

    override val layoutId: Int
        get() = R.layout.fragment_income_break_down

    override val viewModelClass: Class<IncomeBreakdownViewModel>
        get() = IncomeBreakdownViewModel::class.java

    override val viewModelId: Int
        get() = BR.viewModel
}