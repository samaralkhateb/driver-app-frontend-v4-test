package com.bluebrand.beedriver.presentation.ui.auth.signup

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.bluebrand.beedriver.R


class SelectionSpinnerAdapter<T>(
    @StringRes private val hintResId: Int,
    context: Context
) : ArrayAdapter<T>(context, R.layout.item_spinner) {
    override fun isEnabled(position: Int): Boolean {
        return position != 0
    }

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = LayoutInflater.from(context).inflate(R.layout.item_spinner, parent, false)

        val itemText = view.findViewById<TextView>(R.id.tv_item_text)

        if (position == 0) {
            itemText.text = context.getString(hintResId)
        } else {
            itemText.text = getItem(position)?.toString()
        }

        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = LayoutInflater.from(context).inflate(R.layout.item_spinner_dropdown, parent, false)

        val itemText = view.findViewById<TextView>(R.id.tv_dropdown_item_text)

        if (position == 0) {
            itemText.text = context.getString(hintResId)
            itemText.setTextColor(ContextCompat.getColor(context, R.color.gray2))
        } else {
            itemText.text = getItem(position)?.toString()
        }

        return view
    }

    override fun getCount(): Int {
        return super.getCount() + 1
    }

    override fun getItem(position: Int): T? {
        return super.getItem(position - 1)
    }
}