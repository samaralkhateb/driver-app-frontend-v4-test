package com.bluebrand.beedriver.presentation.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.repository.location.ILocationRepository
import com.bluebrand.beedriver.data.repository.socket.ISocketRepository
import com.bluebrand.beedriver.presentation.util.device.DeviceUtil
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@AndroidEntryPoint
class DriverLocationService : Service() {

    @Inject
    lateinit var locationRepository: ILocationRepository

    @Inject
    lateinit var socketRepository: ISocketRepository

    private val coroutineScope =
        CoroutineScope(Dispatchers.Main + SupervisorJob())

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                CHANNEL_ID,
                getString(R.string.app_name),
                NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.setShowBadge(false)
            channel.enableVibration(false)
            channel.setSound(null, null)
            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(
                channel
            )
        }
        startForeground(ID_LOCATION, getNotification(getString(R.string.ntf_location_service)))
    }

    private fun getNotification(notificationDescription: String): Notification {
        val notificationBuilder =
            NotificationCompat.Builder(applicationContext, CHANNEL_ID)
                .setSmallIcon(R.drawable.logo_app)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(notificationDescription)
                .setSound(null)
                .setVibrate(longArrayOf(0L))
                .setStyle(NotificationCompat.BigTextStyle().bigText(notificationDescription))
        return notificationBuilder.build()
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand: called.")
        startLocationUpdate()
        return START_NOT_STICKY
    }

    private fun startLocationUpdate() {
        socketRepository.connect()

        locationRepository.getLocationUpdates(LOCATION_UPDATE_INTERVAL)
            .sample(LOCATION_UPDATE_INTERVAL)
            .onEach {
                socketRepository.updateDriverLocation(
                    it,
                    DeviceUtil.getBatteryLevel(applicationContext),
                    DeviceUtil.getSignalStrength(applicationContext),
                )
            }
            .catch { e -> e.printStackTrace() }
            .launchIn(coroutineScope)
    }


    override fun onDestroy() {
        Log.d(TAG, "onDestroy: called.")
        coroutineScope.coroutineContext.cancelChildren()
        socketRepository.disconnect()
        stopForeground(true)
        stopSelf()
        super.onDestroy()
    }

    companion object {
        private const val TAG = "FusedLocationService"

        private const val CHANNEL_ID = "bee_driver_location"
        private const val ID_LOCATION = 1
        private const val LOCATION_UPDATE_INTERVAL: Long = 5 * 1000
    }
}