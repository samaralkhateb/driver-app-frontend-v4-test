package com.bluebrand.beedriver.presentation.ui.main.transactions.income.monthly

import androidx.recyclerview.widget.RecyclerView
import com.bluebrand.beedriver.presentation.ui.main.transactions.income.base.BaseIncomeFragment
import com.bluebrand.beedriver.presentation.ui.main.transactions.income.base.IncomeInfoListAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MonthlyIncomeHistoryFragment : BaseIncomeFragment<MonthlyIncomeHistoryViewModel>(){

    override fun provideRecyclerAdapter(): RecyclerView.Adapter<*> {
        return IncomeInfoListAdapter(requireContext())
    }

    override val viewModelClass: Class<MonthlyIncomeHistoryViewModel>
        get() = MonthlyIncomeHistoryViewModel::class.java
}