package com.bluebrand.beedriver.presentation.util.notification

import android.content.Context
import android.media.AudioManager
import android.media.SoundPool
import com.bluebrand.beedriver.R

class SoundPoolManager private constructor(context: Context) {

    private var playing = false
    private var loaded = false
    private var playingCalled = false
    private val volume: Float
    private var soundPool: SoundPool? = null
    private var ringingSoundId = 0
    private var ringingStreamId = 0

    init {
        // AudioManager audio settings for adjusting the volume
        val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val actualVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC).toFloat()
        val maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC).toFloat()
        volume = actualVolume / maxVolume

        // Load the sounds
        val maxStreams = 1
        soundPool = SoundPool.Builder()
            .setMaxStreams(maxStreams)
            .build()
        soundPool?.setOnLoadCompleteListener({ soundPool, sampleId, status ->
            loaded = true
            if (playingCalled) {
                playRinging()
                playingCalled = false
            }
        })
        ringingSoundId = soundPool?.load(context, R.raw.incoming, 1)!!
    }

    companion object {
        private var instance: SoundPoolManager? = null
        fun getInstance(context: Context): SoundPoolManager? {
            if (instance == null) {
                instance = SoundPoolManager(context)
            }
            return instance
        }
    }

    fun playRinging() {
        if (loaded && !playing) {
            ringingStreamId = soundPool!!.play(ringingSoundId, volume, volume, 1, -1, 1f)
            playing = true
        } else {
            playingCalled = true
        }
    }

    fun stopRinging() {
        if (playing) {
            soundPool!!.stop(ringingStreamId)
            playing = false
        }
    }

    fun release() {
        if (soundPool != null) {
            soundPool!!.unload(ringingSoundId)
            soundPool!!.release()
            soundPool = null
        }
        instance = null
    }

}