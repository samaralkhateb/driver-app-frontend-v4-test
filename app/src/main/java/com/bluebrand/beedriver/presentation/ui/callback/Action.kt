package com.bluebrand.beedriver.presentation.ui.callback

interface Action<T> {
    fun apply(model: T)
}