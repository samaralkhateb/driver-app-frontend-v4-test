package com.bluebrand.beedriver.presentation.ui.main.incentives.list.history

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bluebrand.beedriver.presentation.ui.main.incentives.list.BaseIncentiveListFragment
import com.bluebrand.beedriver.presentation.ui.main.incentives.list.IncentiveListAdapter

class IncentiveHistoryFragment : BaseIncentiveListFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.fetchIncentiveHistory()

        setupListeners()
    }

    private fun setupListeners() {
        binding.layoutRefresh.setOnRefreshListener {
            viewModel.fetchIncentiveHistory()
        }
    }

    override fun provideRecyclerAdapter(): RecyclerView.Adapter<*> {
        return IncentiveListAdapter(requireContext(), true)
    }
}