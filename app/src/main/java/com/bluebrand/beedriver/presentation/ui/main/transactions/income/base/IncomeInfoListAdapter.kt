package com.bluebrand.beedriver.presentation.ui.main.transactions.income.base

import android.content.Context
import android.view.ViewGroup
import com.bluebrand.beedriver.data.model.IncomeInfo
import com.bluebrand.beedriver.databinding.ItemIncomeInfoBinding
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseListAdapter
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseViewHolder

open class IncomeInfoListAdapter(context: Context) :
    BaseListAdapter<IncomeInfo, BaseViewHolder<IncomeInfo, *>>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<IncomeInfo, *> {
        val binding = ItemIncomeInfoBinding.inflate(provideInflater(), parent, false)
        return IncomeInfoViewHolder(binding)
    }

    class IncomeInfoViewHolder(binding: ItemIncomeInfoBinding) :
        BaseViewHolder<IncomeInfo, ItemIncomeInfoBinding>(binding) {
        override fun onBind(item: IncomeInfo, position: Int) {
            binding.incomeInfo = item
        }
    }
}