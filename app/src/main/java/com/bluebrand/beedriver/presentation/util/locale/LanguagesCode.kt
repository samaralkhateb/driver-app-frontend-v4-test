package com.bluebrand.beedriver.presentation.util.locale

object LanguagesCode {
    var Abkhazian = "ab"
    var Afar = "aa"
    var Afrikaans = "af"
    var Akan = "ak"
    var Albanian = "sq"
    var Amharic = "am"
    var Arabic = "ar"
    var Aragonese = "an"
    var Armenian = "hy"
    var Assamese = "as"
    var Avaric = "av"
    var Avestan = "ae"
    var Aymara = "ay"
    var Azerbaijani = "az"
    var Bambara = "bm"
    var Bashkir = "ba"
    var Basque = "eu"
    var Belarusian = "be"
    var Bengali = "bn"
    var Bihari = "bh"
    var Bislama = "bi"
    var Bosnian = "bs"
    var Breton = "br"
    var Bulgarian = "bg"
    var Burmese = "my"
    var Catalan = "ca"
    var Chamorro = "ch"
    var Chechen = "ce"
    var Chichewa_Chewa_Nyanja = "ny"
    var Nyanja = "ny"
    var Chinese = "zh"
    var ChineseSimplified = "zh-Hans"
    var ChineseTraditional = "zh-Hant"
    var Chuvash = "cv"
    var Cornish = "kw"
    var Corsican = "co"
    var Cree = "cr"
    var Croatian = "hr"
    var Czech = "cs"
    var Danish = "da"
    var Divehi_Dhivehi_Maldivian = "dv"
    var Dutch = "nl"
    var Dzongkha = "dz"

    @JvmField
    var English = "en"
    var Esperanto = "eo"
    var Estonian = "et"
    var Ewe = "ee"
    var Faroese = "fo"
    var Fijian = "fj"
    var Finnish = "fi"
    var French = "fr"
    var Fula_Fulah_Pulaar_Pular = "ff"
    var Galician = "gl"
    var GaelicScottish = "gd"
    var GaelicManx = "gv"
    var Georgian = "ka"
    var German = "de"
    var Greek = "el"
    var Greenlandic = "kl"
    var Guarani = "gn"
    var Gujarati = "gu"
    var HaitianCreole = "ht"
    var Hausa = "ha"
    var Hebrew = "he"
    var Herero = "hz"
    var Hindi = "hi"
    var HiriMotu = "ho"
    var Hungarian = "hu"
    var Icelandic = "is"
    var Ido = "io"
    var Igbo = "ig"
    var Indonesian = "in"
    var Interlingua = "ia"
    var Interlingue = "ie"
    var Inuktitut = "iu"
    var Inupiak = "ik"
    var Irish = "ga"
    var Italian = "it"
    var Japanese = "ja"
    var Javanese = "jv"
    var Kalaallisut_Greenlandic = "kl"
    var Kannada = "kn"
    var Kanuri = "kr"
    var Kashmiri = "ks"
    var Kazakh = "kk"
    var Khmer = "km"
    var Kikuyu = "ki"
    var KinyarwandaRwanda = "rw"
    var Kirundi = "rn"
    var Kyrgyz = "ky"
    var Komi = "kv"
    var Kongo = "kg"
    var Korean = "ko"
    var Kurdish = "ku"
    var Kwanyama = "kj"
    var Lao = "lo"
    var Latin = "la"
    var Latvian = "lv"
    var Limburgish = "li"
    var Lingala = "ln"
    var Lithuanian = "lt"
    var Luga_Katanga = "lu"
    var Luganda_Ganda = "lg"
    var Luxembourgish = "lb"
    var Manx = "gv"
    var Macedonian = "mk"
    var Malagasy = "mg"
    var Malay = "ms"
    var Malayalam = "ml"
    var Maltese = "mt"
    var Maori = "mi"
    var Marathi = "mr"
    var Marshallese = "mh"
    var Moldavian = "mo"
    var Mongolian = "mn"
    var Nauru = "na"
    var Navajo = "nv"
    var Ndonga = "ng"
    var Northern_Ndebele = "nd"
    var Nepali = "ne"
    var Norwegian = "no"
    var Norwegian_bokmal = "nb"
    var Norwegian_nynorsk = "nn"
    var Nuosu = "ii"
    var Occitan = "oc"
    var Ojibwe = "oj"
    var Slavonic_Bulgarian = "cu"
    var Oriya = "or"
    var Oromo = "om"
    var Ossetian = "os"
    var Pāli = "pi"
    var Pashto_Pushto = "ps"
    var Persian = "fa"
    var Polish = "pl"
    var Portuguese = "pt"
    var Punjabi = "pa"
    var Quechua = "qu"
    var Romansh = "rm"
    var Romanian = "ro"
    var Russian = "ru"
    var Sami = "se"
    var Samoan = "sm"
    var Sango = "sg"
    var Sanskrit = "sa"
    var Serbian = "sr"
    var Serbo_Croatian = "sh"
    var Sesotho = "st"
    var Setswana = "tn"
    var Shona = "sn"
    var SichuanYi = "ii"
    var Sindhi = "sd"
    var Sinhalese = "si"
    var Siswati = "ss"
    var Slovak = "sk"
    var Slovenian = "sl"
    var Somali = "so"
    var SouthernNdebele = "nr"
    var Spanish = "es"
    var Sundanese = "su"
    var Swahili = "sw"
    var Swati = "ss"
    var Swedish = "sv"
    var Tagalog = "tl"
    var Tahitian = "ty"
    var Tajik = "tg"
    var Tamil = "ta"
    var Tatar = "tt"
    var Telugu = "te"
    var Thai = "th"
    var Tibetan = "bo"
    var Tigrinya = "ti"
    var Tonga = "to"
    var Tsonga = "ts"
    var Turkish = "tr"
    var Turkmen = "tk"
    var Twi = "tw"
    var Uyghur = "ug"
    var Ukrainian = "uk"
    var Urdu = "ur"
    var Uzbek = "uz"
    var Venda = "ve"
    var Vietnamese = "vi"
    var Volapuk = "vo"
    var Wallon = "wa"
    var Welsh = "cy"
    var Wolof = "wo"
    var WesternFrisian = "fy"
    var Xhosa = "xh"
    var Yiddish = "yi"
    var Yoruba = "yo"
    var Zhuang_Chuang = "za"
    var Zulu = "zu"
}