package com.bluebrand.beedriver.presentation.ui.main.home

import android.location.Location
import android.media.MediaPlayer
import android.os.Bundle
import android.view.View
import androidx.activity.result.ActivityResultLauncher
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelStoreOwner
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.bluebrand.beedriver.BR
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.model.Notification
import com.bluebrand.beedriver.data.model.OrderDetails
import com.bluebrand.beedriver.databinding.FragmentHomeBinding
import com.bluebrand.beedriver.presentation.busevent.OrderCanceledEvent
import com.bluebrand.beedriver.presentation.busevent.RefreshOrdersEvent
import com.bluebrand.beedriver.presentation.service.FirebaseService
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.BaseMapBoxFragment
import com.bluebrand.beedriver.presentation.ui.common.Constants
import com.bluebrand.beedriver.presentation.ui.main.MainActivity
import com.bluebrand.beedriver.presentation.ui.main.home.order.NewOrderDialog
import com.bluebrand.beedriver.presentation.util.bus.Bus
import com.bluebrand.beedriver.presentation.util.lifecycle.EventObserver
import com.bluebrand.beedriver.presentation.util.locale.LocaleManager
import com.bluebrand.beedriver.presentation.util.navigation.NavigationUtil
import com.bluebrand.beedriver.presentation.util.permission.LocationPermissionUtil
import com.bluebrand.beedriver.presentation.util.resourses.ProgressDialogUtil
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.Symbol
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import java.time.LocalDate
import java.time.format.TextStyle


@AndroidEntryPoint
class HomeFragment : BaseMapBoxFragment<HomeViewModel, FragmentHomeBinding>() {

    private lateinit var navController: NavController
    private lateinit var permissionResultLauncher: ActivityResultLauncher<Array<String>>

    private var newOrderDialog: NewOrderDialog? = null

    private var marker: Symbol? = null

    private var mediaPlayer: MediaPlayer? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)
        permissionResultLauncher = LocationPermissionUtil.registerForResult(this)

        requestLocationPermissions()
        showDate()
        setupListeners()
        registerObservers()

        viewModel.listenForDriverInformationUpdate()
        viewModel.listenForNewOrders()
    }

    private fun showDate() {
        val locale = LocaleManager.getLocale(resources)

        val weekDay = LocalDate.now().dayOfWeek.getDisplayName(TextStyle.FULL, locale)
        val monthDay = LocalDate.now().dayOfMonth
        val month = LocalDate.now().month.getDisplayName(TextStyle.FULL, locale)

        binding.tvDay.text = weekDay
        binding.tvMonth.text = monthDay.toString().plus("-").plus(month)
    }

    private fun setupListeners() {
        binding.layoutArCr.root.setOnClickListener {
            navController.navigate(
                HomeFragmentDirections
                    .actionHomeFragmentToArCrHistoryFragment()
            )
        }

        binding.buttonContactSupport.setOnClickListener {
            startActivity(
                NavigationUtil.dialIntent(viewModel.supportPhoneNumber)
            )
        }

        binding.buttonLastOrder.setOnClickListener {
            viewModel.toggleLastOrder()
        }

        binding.buttonAutoAccept.setOnClickListener {
            viewModel.toggleAutoAccept()
        }

        binding.buttonMyLocation.setOnClickListener {
            viewModel.driverLocation.value?.let { updateMarkerLocation(it, true) }
        }

        binding.buttonActiveOrders.setOnClickListener {
            openOngoingOrdersFragment()
        }
    }

    private fun openOngoingOrdersFragment() {
        if (navController.currentDestination?.id == R.id.homeFragment) {
            navController.navigate(
                HomeFragmentDirections
                    .actionHomeFragmentToOnGoingOrderFragment()
            )
        }
    }

    private val currentPendingOrderObserver = Observer { order: OrderDetails? ->
        if (order != null && baseActivity?.isActivityRunning != true) {
            mediaPlayer = MediaPlayer.create(requireContext(), R.raw.incoming).apply {
                isLooping = true
            }
            mediaPlayer?.start()

            FirebaseService.displayNotification(
                requireContext(),
                Notification(
                    System.currentTimeMillis().toInt(),
                    Constants.NTF_ORDER_DISPATCH,
                    getString(R.string.msg_new_order_received),
                    null,
                    null,
                    Notification.Sound.VERY_ANNOYING
                )
            )
        }
    }

    private fun registerObservers() {
        viewModel.isLoading.observe(viewLifecycleOwner) { isLoading: Boolean ->
            if (isLoading) {
                ProgressDialogUtil.showProgressDialog(requireContext())
            } else {
                ProgressDialogUtil.hideProgressDialog()
            }
        }

        viewModel.driverLocation.observe(viewLifecycleOwner, this::updateMarkerLocation)

        viewModel.currentPendingOrder.removeObserver(currentPendingOrderObserver)
        viewModel.currentPendingOrder.observeForever(currentPendingOrderObserver)

        viewModel.currentPendingOrder.observe(requireActivity()) {
            if (it != null) {
                showNewOrderDialog()
            } else {
                newOrderDialog?.dismiss()
            }
        }

        viewModel.openOngoingOrders.observe(
            viewLifecycleOwner,
            EventObserver { result -> if (result) openOngoingOrdersFragment() }
        )

        Bus.eventFlowOf(RefreshOrdersEvent::class.java)
            .onEach {
                if (!it.ignoreIfPending
                    || it.orderId == null
                    || it.orderId != viewModel.currentPendingOrder.value?.orderId
                ) {
                    if (navController.currentDestination?.id != R.id.homeFragment) {
                        navController.popBackStack(R.id.homeFragment, false)
                    }
                    viewModel.fetchDriverOrders()
                }
            }
            .launchIn(requireActivity().lifecycleScope)

        Bus.eventFlowOf(OrderCanceledEvent::class.java)
            .onEach {
                viewModel.removeOrderById(it.orderId)
            }
            .launchIn(requireActivity().lifecycleScope)
    }

    override fun onStyleReady(style: Style) {
        super.onStyleReady(style)
        viewModel.driverLocation.value?.let { updateMarkerLocation(it, true) }
    }

    private fun updateMarkerLocation(location: Location, adjustZoom: Boolean = false) {
        marker?.let { removeMarker(it) }
        marker = addMarker(location.latitude, location.longitude)

        moveCamera(
            location.latitude,
            location.longitude,
            if (adjustZoom) DEFAULT_ZOOM
            else map?.cameraPosition?.zoom?.toFloat() ?: DEFAULT_ZOOM
        )
    }

    private fun requestLocationPermissions() {
        LocationPermissionUtil
            .requestLocationPermission(requireActivity(), permissionResultLauncher)
            .onEach { result ->
                if (result) onLocationPermissionGranted()
                else showLocationPermissionDialog()
            }
            .launchIn(lifecycleScope)
    }

    private fun onLocationPermissionGranted() {
        viewModel.fetchLocationUpdates()
    }

    private fun showLocationPermissionDialog() {
        (baseActivity as? MainActivity)?.showLocationPermissionDialog()
    }

    private fun showNewOrderDialog() {
        newOrderDialog?.dismiss()
        newOrderDialog = NewOrderDialog().also {
            it.showNow(requireActivity().supportFragmentManager, null)
        }
    }

    override fun onResume() {
        super.onResume()
        if (viewModel.currentPendingOrder.value == null) {
            viewModel.fetchDriverOrders()
        }

        mediaPlayer?.stop()
    }

    override val mapView: MapView
        get() = binding.mapHome

    override val layoutId: Int
        get() = R.layout.fragment_home

    override val viewModelClass: Class<HomeViewModel>
        get() = HomeViewModel::class.java

    override val viewModelOwner: ViewModelStoreOwner
        get() = requireActivity()

    override val viewModelId: Int
        get() = BR.viewModel
}
