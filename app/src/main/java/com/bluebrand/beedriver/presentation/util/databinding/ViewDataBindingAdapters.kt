package com.bluebrand.beedriver.presentation.util.databinding

import android.graphics.Paint
import android.graphics.Typeface
import android.os.Build
import android.text.Html
import android.view.View
import android.webkit.WebView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.model.WarningThresholds

@BindingAdapter("android:visibility")
fun viewVisibility(view: View, isVisible: Boolean) {
    view.visibility = if (isVisible) View.VISIBLE else View.INVISIBLE
}

@BindingAdapter("existence")
fun viewExistence(view: View, isExists: Boolean) {
    view.visibility = if (isExists) View.VISIBLE else View.GONE
}

@BindingAdapter("goneIfEmpty")
fun goneIfEmpty(textView: TextView, check: Boolean) {
    if (check) {
        if (textView.text == null || textView.text.toString() == "") {
            textView.visibility = View.GONE
        }
    }
}

@BindingAdapter("android:text")
fun integerText(textView: TextView, number: Int) {
    textView.text = number.toString()
}

@BindingAdapter("android:text")
fun doubleText(textView: TextView, number: Double) {
    textView.text = number.toString()
}

@BindingAdapter("android:htmlText")
fun htmlText(textView: TextView, htmlText: String) {
    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N)
        textView.text = Html.fromHtml(htmlText, Html.FROM_HTML_MODE_COMPACT)
    else textView.text = Html.fromHtml(htmlText)
}

@BindingAdapter("android:textPaintFlags")
fun textPaintFlag(textView: TextView, flags: Int) {
    textView.paintFlags = textView.paintFlags or flags
}

@BindingAdapter("android:strikeThroughText")
fun strikeThroughText(textView: TextView, strikeThrough: Boolean) {
    if (strikeThrough) textPaintFlag(textView, Paint.STRIKE_THRU_TEXT_FLAG)
}

@BindingAdapter("android:colorSchemeResources")
fun swipeRefreshColors(view: SwipeRefreshLayout, color: Int) {
    view.setColorSchemeColors(color)
}

@BindingAdapter("android:orientation")
fun recyclerViewOrientation(recyclerView: RecyclerView, orientation: Int) {
    recyclerView.layoutManager = LinearLayoutManager(recyclerView.context, orientation, false)
}

@BindingAdapter("isBold")
fun setBold(view: TextView, isBold: Boolean) {
    view.setTypeface(null, if (isBold) Typeface.BOLD else Typeface.NORMAL)
}

@BindingAdapter("hint")
fun setHint(textView: TextView, stringId: Int) {
    if (textView.text.isEmpty()) {
        val text = textView.context.getString(stringId)
        textView.text = text
    }
}

@BindingAdapter("underline")
fun setUnderline(textView: TextView, underline: Boolean) {
    if (underline) {
        textView.paintFlags = (textView.paintFlags or Paint.UNDERLINE_TEXT_FLAG);
    }
}

// SearchView
@BindingAdapter("searchQuery")
fun setSearchQuery(searchView: SearchView, query: String) {
    if (searchView.query != null && searchView.query.toString() != query)
        searchView.setQuery(
            query,
            false
        )
}

@InverseBindingAdapter(attribute = "searchQuery")
fun getSearchQuery(searchView: SearchView): String? {
    return searchView.query?.toString()
}

@BindingAdapter("initWebView")
fun initWebView(webView: WebView, url: String?) {
    val webSettings = webView.settings
    webSettings.javaScriptEnabled = true
    webSettings.domStorageEnabled = true
    webView.loadUrl(url!!)
}

@BindingAdapter(
    value = ["app:swipeRefreshOffsetStart", "app:swipeRefreshOffsetEnd"],
    requireAll = true
)
fun setSwipeRefreshOffset(swipeRefreshOffset: SwipeRefreshLayout, start: Float, end: Float) {
    swipeRefreshOffset.setProgressViewOffset(false, start.toInt(), end.toInt())
}

@BindingAdapter(
    value = ["app:arValue", "app:arWarningThresholds"],
    requireAll = true
)
fun setArProgressColor(
    progressBar: ProgressBar,
    arValue: Int?,
    arWarningThresholds: WarningThresholds?
) {
    if (arValue == null || arWarningThresholds == null) return

    progressBar.progressDrawable = when (arValue) {
        in 0..arWarningThresholds.blockingThreshold ->
            ContextCompat.getDrawable(progressBar.context, R.drawable.bg_progressbar_red)

        in arWarningThresholds.blockingThreshold + 1..arWarningThresholds.warningThreshold ->
            ContextCompat.getDrawable(progressBar.context, R.drawable.bg_progressbar_yellow)

        else -> ContextCompat.getDrawable(progressBar.context, R.drawable.bg_progressbar_primary)
    }

    progressBar.progress = arValue
}

@BindingAdapter(
    value = ["app:crValue", "app:crWarningThresholds"],
    requireAll = true
)
fun setCrProgressColor(
    progressBar: ProgressBar,
    crValue: Int?,
    crWarningThresholds: WarningThresholds?
) {
    if (crValue == null || crWarningThresholds == null) return

    progressBar.progressDrawable = when (crValue) {
        in 100 downTo crWarningThresholds.blockingThreshold ->
            ContextCompat.getDrawable(progressBar.context, R.drawable.bg_progressbar_red)

        in crWarningThresholds.blockingThreshold - 1 downTo crWarningThresholds.warningThreshold ->
            ContextCompat.getDrawable(progressBar.context, R.drawable.bg_progressbar_yellow)

        else -> ContextCompat.getDrawable(progressBar.context, R.drawable.bg_progressbar_primary)
    }

    progressBar.progress = crValue
}

@BindingAdapter(
    value = ["app:arValue", "app:arWarningThresholds"],
    requireAll = true
)
fun setArTextColor(
    textView: TextView,
    arValue: Int?,
    arWarningThresholds: WarningThresholds?
) {
    if (arValue == null || arWarningThresholds == null) return
    val colorId = when (arValue) {
        in 0..arWarningThresholds.blockingThreshold -> R.color.red
        in arWarningThresholds.blockingThreshold + 1..arWarningThresholds.warningThreshold -> R.color.yellow
        else -> R.color.colorPrimary
    }

    textView.setTextColor(ContextCompat.getColor(textView.context, colorId))
    textView.text = String.format("%d", arValue).plus("%")
}

@BindingAdapter(
    value = ["app:crValue", "app:crWarningThresholds"],
    requireAll = true
)
fun setCrTextColor(
    textView: TextView,
    crValue: Int?,
    crWarningThresholds: WarningThresholds?
) {
    if (crValue == null || crWarningThresholds == null) return
    val colorId = when (crValue) {
        in 100 downTo crWarningThresholds.blockingThreshold -> R.color.red
        in crWarningThresholds.blockingThreshold - 1 downTo crWarningThresholds.warningThreshold -> R.color.yellow
        else -> R.color.colorPrimary
    }

    textView.setTextColor(ContextCompat.getColor(textView.context, colorId))
    textView.text = String.format("%d", crValue).plus("%")
}