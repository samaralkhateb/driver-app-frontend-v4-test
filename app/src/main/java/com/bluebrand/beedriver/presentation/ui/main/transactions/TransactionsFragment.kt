package com.bluebrand.beedriver.presentation.ui.main.transactions

import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.bluebrand.beedriver.BR
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.databinding.FragmentTransacationsBinding
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.MVVMFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TransactionsFragment : MVVMFragment<TransactionsViewModel, FragmentTransacationsBinding>() {

    private lateinit var navController: NavController
    private lateinit var adapter: TransactionListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        setupTransactionsRecycler()
        setupClickListeners()
        registerObservers()
    }

    private fun setupTransactionsRecycler() {
        adapter = TransactionListAdapter(requireContext())
        binding.layoutTransactions.rvTransactions.adapter = adapter
    }

    private fun setupClickListeners() {
        binding.buttonMaximize.setOnClickListener {
            openInLandscapeMode()
        }

        binding.buttonIncomeBreakdown.setOnClickListener { navigateToIncomeBreakdownFragment() }
    }

    private fun openInLandscapeMode() {
        startActivity(
            TransactionsActivity.getIntent(requireContext(), viewModel.transactionsInfo.value!!)
        )
    }

    private fun navigateToIncomeBreakdownFragment() {
        navController.navigate(
            TransactionsFragmentDirections
                .actionTransactionsFragmentToIncomeBreakdownFragment()
        )
    }


    private fun registerObservers() {
        viewModel.transactionsInfo
            .observe(viewLifecycleOwner) { transactionInfo ->
                adapter.submitData(transactionInfo?.transactionList ?: emptyList())
            }
    }


    override val layoutId: Int
        get() = R.layout.fragment_transacations

    override val viewModelClass: Class<TransactionsViewModel>
        get() = TransactionsViewModel::class.java

    override val viewModelId: Int
        get() = BR.viewModel
}