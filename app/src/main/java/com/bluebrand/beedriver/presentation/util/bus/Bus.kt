package com.bluebrand.beedriver.presentation.util.bus

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch


object Bus {
    private val events = MutableSharedFlow<BusEvent>()

    fun <T : BusEvent> eventFlowOf(type: Class<T>): Flow<T> {
        return events
            .filter { type.isInstance(it) }
            .map { type.cast(it) }
            .filterNotNull()
    }

    fun <T : BusEvent> publishEvent(event: T) {
        GlobalScope.launch(Dispatchers.IO) {
            events.emit(event)
        }
    }
}