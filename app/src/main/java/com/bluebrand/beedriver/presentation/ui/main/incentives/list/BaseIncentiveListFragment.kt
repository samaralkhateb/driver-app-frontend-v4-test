package com.bluebrand.beedriver.presentation.ui.main.incentives.list

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bluebrand.beedriver.BR
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.databinding.FragmentIncentiveListBinding
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.MVVMFragment
import com.bluebrand.beedriver.presentation.ui.main.incentives.IncentivesViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
abstract class BaseIncentiveListFragment :
    MVVMFragment<IncentivesViewModel, FragmentIncentiveListBinding>() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupIncentiveRecycler()
    }

    private fun setupIncentiveRecycler() {
        binding.rvIncentives.adapter = provideRecyclerAdapter()
    }

    protected abstract fun provideRecyclerAdapter(): RecyclerView.Adapter<*>

    override val layoutId: Int
        get() = R.layout.fragment_incentive_list

    override val viewModelClass: Class<IncentivesViewModel>
        get() = IncentivesViewModel::class.java

    override val viewModelId: Int
        get() = BR.viewModel
}