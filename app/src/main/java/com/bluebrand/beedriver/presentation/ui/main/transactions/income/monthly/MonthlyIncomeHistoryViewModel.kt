package com.bluebrand.beedriver.presentation.ui.main.transactions.income.monthly

import androidx.lifecycle.viewModelScope
import com.bluebrand.beedriver.data.repository.driver.IDriverRepository
import com.bluebrand.beedriver.presentation.ui.main.transactions.income.base.BaseIncomeViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class MonthlyIncomeHistoryViewModel @Inject constructor(
    private val driverRepository: IDriverRepository
) : BaseIncomeViewModel(){

    override fun fetchData() {
        if (isLoading()) return
        startLoading()

        driverRepository.getMonthlyIncomeHistory()
            .catch(snackErrorHandler)
            .onEach {
                stopLoading()
                setIncomeData(it)
            }
            .launchIn(viewModelScope)
    }
}