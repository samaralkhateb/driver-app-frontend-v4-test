package com.bluebrand.beedriver.presentation.ui.main.home.order.restaurant

import android.os.Bundle
import android.view.View
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.ViewModelStoreOwner
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.model.OrderItem
import com.bluebrand.beedriver.databinding.FragmentOrderItemsBinding
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.MVVMFragment
import com.bluebrand.beedriver.presentation.ui.callback.OnItemSelectedListener
import com.bluebrand.beedriver.presentation.ui.main.home.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OrderItemsFragment : MVVMFragment<HomeViewModel, FragmentOrderItemsBinding>() {

    private lateinit var navController: NavController
    private lateinit var orderItemsAdapter: OrderItemsAdapter

    private val checkedItemsMap = mutableMapOf<OrderItem, Boolean>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        setupOrderItemsRecycler()
        registerListeners()
    }

    private val onItemCheckedListener = OnItemSelectedListener<OrderItem> { item, isChecked ->
        checkedItemsMap[item] = isChecked
        binding.buttonOrderReceived.isEnabled =
            orderItemsAdapter.getData().all { checkedItemsMap.getOrDefault(it, false) }
    }

    private fun setupOrderItemsRecycler() {
        orderItemsAdapter = OrderItemsAdapter(onItemCheckedListener, requireContext())
        binding.rvOrderItems.adapter = orderItemsAdapter
    }

    private fun registerListeners() {
        binding.buttonOrderReceived.setOnClickListener {
            showCheckAllItemsDialog()
        }
    }

    private fun showCheckAllItemsDialog() {
        showDialogMessage(
            title = "",
            message = getString(R.string.lbl_checkAllItem),
            positiveListener = { _, _ -> showPaymentGateway() }
        )
    }

    private fun showPaymentGateway() {
        navController.navigate(
            OrderItemsFragmentDirections
                .actionOrderItemsFragmentToRestaurantPaymentGatewayDialog()
        )
    }

    override val layoutId: Int
        get() = R.layout.fragment_order_items

    override val viewModelClass: Class<HomeViewModel>
        get() = HomeViewModel::class.java

    override val viewModelOwner: ViewModelStoreOwner
        get() = requireActivity()

    override val viewModelId: Int
        get() = BR.viewModel
}