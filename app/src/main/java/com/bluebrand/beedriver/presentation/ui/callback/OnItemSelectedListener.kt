package com.bluebrand.beedriver.presentation.ui.callback

fun interface OnItemSelectedListener<T> {
    fun onItemSelected(item: T, isSelected: Boolean)
}