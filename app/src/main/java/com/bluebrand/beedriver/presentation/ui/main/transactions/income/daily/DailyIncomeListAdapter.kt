package com.bluebrand.beedriver.presentation.ui.main.transactions.income.daily

import android.content.Context
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.MotionLayout
import com.bluebrand.beedriver.data.model.IncomeInfo
import com.bluebrand.beedriver.databinding.ItemIncomeDailyBinding
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseViewHolder
import com.bluebrand.beedriver.presentation.ui.main.transactions.income.base.IncomeInfoListAdapter
import com.bluebrand.beedriver.presentation.util.locale.LanguagesCode
import com.bluebrand.beedriver.presentation.util.locale.LocaleManager


class DailyIncomeListAdapter(context: Context) : IncomeInfoListAdapter(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DailyIncomeViewHolder {
        val binding = ItemIncomeDailyBinding.inflate(provideInflater(), parent, false)
        return DailyIncomeViewHolder(binding)
    }

    inner class DailyIncomeViewHolder(
        binding: ItemIncomeDailyBinding
    ) : BaseViewHolder<IncomeInfo, ItemIncomeDailyBinding>(binding) {
        private var adapter = IncomeDetailsAdapter(binding.root.context)

        init {
            binding.rvIncomeDetails.adapter = adapter
        }

        override fun onBind(item: IncomeInfo, position: Int) {
            binding.incomeInfo = item
            adapter.submitData(item.incomeDetails)

            binding.motionLayoutIncome.setTransitionListener(object :
                MotionLayout.TransitionListener {
                override fun onTransitionTrigger(
                    motionLayout: MotionLayout?,
                    triggerId: Int,
                    positive: Boolean,
                    progress: Float
                ) {
                }

                override fun onTransitionStarted(
                    motionLayout: MotionLayout?,
                    startId: Int,
                    endId: Int
                ) {

                }

                override fun onTransitionChange(
                    motionLayout: MotionLayout?,
                    startId: Int,
                    endId: Int,
                    progress: Float
                ) {
                    val mirrored = if (LocaleManager.savedLanguage == LanguagesCode.Arabic) -1 else 1
                    binding.ivArrow.rotation = mirrored * 90f * progress
                }

                override fun onTransitionCompleted(motionLayout: MotionLayout?, currentId: Int) {
                }
            })
        }
    }
}