package com.bluebrand.beedriver.presentation.ui.main.home.order

import android.content.DialogInterface
import android.media.MediaPlayer
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.lifecycle.ViewModelStoreOwner
import androidx.lifecycle.lifecycleScope
import com.bluebrand.beedriver.BR
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.model.OrderDetails
import com.bluebrand.beedriver.databinding.DialogNewOrderBinding
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.MVVMFragmentDialog
import com.bluebrand.beedriver.presentation.ui.main.home.HomeViewModel
import com.bluebrand.beedriver.presentation.util.animation.AnimationUtils
import com.ncorti.slidetoact.SlideToActView
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*


@AndroidEntryPoint
class NewOrderDialog : MVVMFragmentDialog<HomeViewModel, DialogNewOrderBinding>() {

    private lateinit var mediaPlayer: MediaPlayer
    private var timer: CountDownTimer? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setFullScreen()

        isCancelable = false


        mediaPlayer = MediaPlayer.create(requireContext(), R.raw.incoming)
        mediaPlayer.isLooping = true
        mediaPlayer.start()

        registerListeners()

        viewModel.currentPendingOrder.value?.let { checkRemainingTime(it) }
        viewModel.orderReceived()
    }

    private fun checkRemainingTime(orderDetails: OrderDetails) {
        val date =
            SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).parse(orderDetails.acceptanceTimeout)
        val remainingTime =  date?.time?.minus(System.currentTimeMillis()) ?: 0
        if (remainingTime <= 0) {
            viewModel.removeOrderById(orderDetails.orderId)
            showMessage(R.string.msg_order_time_passed)
            dismiss()
        } else {
            startTimer(orderDetails, remainingTime)
        }
    }

    private fun startTimer(orderDetails: OrderDetails, remainingTime: Long) {
        timer?.cancel()

        timer = object : CountDownTimer(remainingTime, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                binding.tvTimeRemains.text = (millisUntilFinished / 1000).toString()
                val millisPassed = remainingTime - millisUntilFinished
                binding.progressTimer.value = ((1.0 * millisPassed / remainingTime) * 100).toInt()
                AnimationUtils.bounceText(binding.tvTimeRemains)
            }

            override fun onFinish() {
                viewModel.removeOrderById(orderDetails.orderId)
                showMessage(R.string.msg_order_time_passed)

                lifecycleScope.launchWhenResumed {
                    dismiss()
                }
            }
        }
        timer?.start()
    }

    private fun registerListeners() {
        binding.buttonAccept.onSlideCompleteListener =
            object : SlideToActView.OnSlideCompleteListener {
                override fun onSlideComplete(view: SlideToActView) {
                    timer?.cancel()
                    viewModel.acceptOrder()
                    dismiss()
                }
            }

        binding.buttonDecline.setOnClickListener {
            timer?.cancel()
            viewModel.declineOrder()
            dismiss()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        timer?.cancel()
        mediaPlayer.stop()
    }

    override val layoutId: Int
        get() = R.layout.dialog_new_order

    override val viewModelClass: Class<HomeViewModel>
        get() = HomeViewModel::class.java

    override val viewModelOwner: ViewModelStoreOwner
        get() = requireActivity()

    override val viewModelId: Int
        get() = BR.viewModel
}