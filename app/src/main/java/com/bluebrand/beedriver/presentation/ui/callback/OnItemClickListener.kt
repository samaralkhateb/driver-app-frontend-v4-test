package com.bluebrand.beedriver.presentation.ui.callback

fun interface OnItemClickListener<T> {
    fun onClick(item: T, position: Int)
}