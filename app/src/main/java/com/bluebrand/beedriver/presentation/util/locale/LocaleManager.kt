package com.bluebrand.beedriver.presentation.util.locale

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.os.LocaleList
import androidx.annotation.RequiresApi
import androidx.core.content.edit
import androidx.core.os.ConfigurationCompat
import androidx.core.os.LocaleListCompat
import com.bluebrand.beedriver.data.util.constants.SharedPreferencesKeys
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import java.util.*

class LocaleManager private constructor(context: Context, sharedPreferencesName: String) {
    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(sharedPreferencesName, Context.MODE_PRIVATE)

    private val _resourcesFlow = MutableSharedFlow<Resources>()

    val resourcesFlow
        get() = _resourcesFlow.asSharedFlow()

    val savedLanguage: String?
        get() = sharedPreferences.getString(LANGUAGE_KEY, defaultLanguage)

    private val defaultLanguage: String = LanguagesCode.Arabic

    companion object {
        private const val LANGUAGE_KEY: String = SharedPreferencesKeys.APP_LANGUAGE
        private const val DEFAULT_SHARED_PREFERENCE_NAME: String = SharedPreferencesKeys.APP_SHARED_KEY

        var savedLanguage = LanguagesCode.Arabic
            private set

        @Volatile
        private var INSTANCE: LocaleManager? = null

        fun getInstance(context: Context): LocaleManager =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: LocaleManager(context, DEFAULT_SHARED_PREFERENCE_NAME).also {
                    INSTANCE = it
                }
            }

        fun getLocale(res: Resources): Locale {
            val config = res.configuration
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) config.locales[0] else config.locale
        }
    }

    fun setLocale(c: Context): Context {
        return updateResources(c, savedLanguage ?: defaultLanguage)
    }

    /**
     * @param c            Context
     * @param languageCode language code (ex: 'en', 'ar', ...), you can see [LanguagesCode]
     */
    fun setNewLocale(c: Context, languageCode: String): Context {
        saveLanguage(languageCode)
        return updateResources(c, languageCode)
    }

    private fun saveLanguage(newLanguage: String) {
        sharedPreferences.edit {
            putString(LANGUAGE_KEY, newLanguage)
        }
    }

    private fun updateResources(context: Context, language: String): Context {
        val locale = Locale(language)
        Locale.setDefault(locale)

        val res = context.resources
        val config = Configuration(res.configuration)
        return when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.N -> {
                setLocaleForApi24(config, locale)
                config.setLayoutDirection(locale)
                context.createConfigurationContext(config)
            }
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
                config.setLayoutDirection(locale)
                config.setLocale(locale)
                context.createConfigurationContext(config)
            }
            else -> {
                config.locale = locale
                res.updateConfiguration(config, res.displayMetrics)
                context
            }
        }.also {
            Companion.savedLanguage = language
            GlobalScope.launch { _resourcesFlow.emit(it.resources) }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private fun setLocaleForApi24(config: Configuration, target: Locale) {
        val set = LinkedHashSet<Locale>()
        // bring the target locale to the front of the list
        set.add(target)

        val all = LocaleList.getDefault()
        repeat(all.size()) { i ->
            // append other locales supported by the user
            all.get(i)?.let { set.add(it) }
        }
        val locales = set.toTypedArray()
        config.setLocales(LocaleList(*locales))
    }
}