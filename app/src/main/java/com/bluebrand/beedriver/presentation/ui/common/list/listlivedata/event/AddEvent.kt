package com.bluebrand.beedriver.presentation.ui.common.list.listlivedata.event

import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseListAdapter

/**
 * Adds new data to the existing data.
 */
class AddEvent<T>(
    allData: List<T>,
    private val dataToAdd: List<T>
) : ListEvent<T>(allData) {
    override fun applyEvent(adapter: BaseListAdapter<T, *>) {
        adapter.insertData(dataToAdd)
    }
}