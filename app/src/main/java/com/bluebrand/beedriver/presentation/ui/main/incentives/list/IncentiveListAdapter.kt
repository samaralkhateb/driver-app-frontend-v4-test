package com.bluebrand.beedriver.presentation.ui.main.incentives.list

import android.content.Context
import android.view.ViewGroup
import com.bluebrand.beedriver.data.model.Incentive
import com.bluebrand.beedriver.databinding.ItemIncentiveBinding
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseListAdapter
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseViewHolder

class IncentiveListAdapter(
    context: Context,
    private val showIncentiveStatus: Boolean
) : BaseListAdapter<Incentive, IncentiveListAdapter.IncentiveViewHolder>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IncentiveViewHolder {
        val binding = ItemIncentiveBinding.inflate(provideInflater(), parent, false)
        return IncentiveViewHolder(binding, showIncentiveStatus)
    }

    class IncentiveViewHolder(
        binding: ItemIncentiveBinding,
        private val showIncentiveStatus: Boolean
    ) : BaseViewHolder<Incentive, ItemIncentiveBinding>(binding) {
        override fun onBind(item: Incentive, position: Int) {
            binding.incentive = item
            binding.showStatus = showIncentiveStatus
        }
    }
}