package com.bluebrand.beedriver.presentation.util.notification

import android.app.NotificationManager
import android.content.Context
import android.os.Handler
import android.os.Looper

object NotificationsUtil {
    fun dismissNotificationById(context: Context, id: Int) {
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        Handler(Looper.getMainLooper()).postDelayed({ notificationManager.cancel(id) }, 500)
    }
}