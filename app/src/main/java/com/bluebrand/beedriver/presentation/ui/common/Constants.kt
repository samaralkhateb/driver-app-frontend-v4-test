package com.bluebrand.beedriver.presentation.ui.common

object Constants {
    const val EXTRA_NOTIFICATION = "extra_notification"

    const val BILL_DRIVER_STATUS_PENDING = 1
    const val BILL_STATUS_OUT_FOR_DELIVERY = 5

    const val NTF_ORDER_DISPATCH = "15"
    const val NTF_ORDER_CANCEL = "17"
    const val NTF_ORDER_HIDDEN_CANCEL = "40"
    const val NTF_ORDER_HIDDEN_DISPATCH = "70"
    const val NTF_ORDER_RE_DISPATCH = "4"
    const val NTF_INCENTIVE = "55"
}