package com.bluebrand.beedriver.presentation.di

import com.bluebrand.beedriver.BuildConfig
import com.bluebrand.beedriver.data.model.IncomeDetails
import com.bluebrand.beedriver.data.model.IncomeIncentiveDetails
import com.bluebrand.beedriver.data.model.IncomeOrderDetails
import com.bluebrand.beedriver.data.model.adapter.IncomeDetailsAdapter
import com.bluebrand.beedriver.data.provider.remote.api.ApiProvider
import com.bluebrand.beedriver.data.provider.remote.api.ExceptionFactory
import com.bluebrand.beedriver.data.provider.remote.api.IApiProvider
import com.bluebrand.beedriver.data.provider.remote.interceptor.RequestInterceptor
import com.bluebrand.beedriver.data.provider.remote.networkchecker.NetworkChecker
import com.bluebrand.beedriver.data.provider.remote.networkchecker.NetworkCheckerImpl
import com.bluebrand.beedriver.data.source.remote.service.IRetrofitService
import com.bluebrand.beedriver.data.util.constants.RemoteConstants
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideNetworkChecker(checker: NetworkCheckerImpl): NetworkChecker {
        return checker
    }

    @Provides
    @Singleton
    fun providerApiProvider(
        networkChecker: NetworkChecker,
        exceptionFactory: ExceptionFactory
    ): IApiProvider {
        return ApiProvider(networkChecker, exceptionFactory)
    }

    @Provides
    @Singleton
    fun provideClient(
        requestInterceptor: RequestInterceptor,
    ): OkHttpClient {

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder().apply {
            addInterceptor(requestInterceptor)
            if (BuildConfig.DEBUG) addInterceptor(loggingInterceptor)
            connectTimeout(10, TimeUnit.MINUTES)
            readTimeout(10, TimeUnit.MINUTES)
            writeTimeout(10, TimeUnit.MINUTES)
        }.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(
        client: OkHttpClient
    ): Retrofit {
        return buildRetrofit(client)
    }

    private fun buildRetrofit(client: OkHttpClient): Retrofit {
        val moshi = Moshi.Builder()
            .add(IncomeDetailsAdapter())
            .add(KotlinJsonAdapterFactory())
            .build()

        return Retrofit.Builder().apply {
            baseUrl(RemoteConstants.BASE_URL)
            client(client)
            addConverterFactory(
                MoshiConverterFactory.create(moshi)
            )
        }.build()
    }

    @Provides
    @Singleton
    fun provideRetrofitService(retrofit: Retrofit): IRetrofitService {
        return retrofit.create(IRetrofitService::class.java)
    }
}