package com.bluebrand.beedriver.presentation.ui.main.incentives

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bluebrand.beedriver.data.model.Incentive
import com.bluebrand.beedriver.data.repository.driver.IDriverRepository
import com.bluebrand.beedriver.presentation.ui.base.view.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class IncentivesViewModel @Inject constructor(
    private val driverRepository: IDriverRepository
) : BaseViewModel() {
    private val _incentivesList = MutableLiveData<List<Incentive>>()
    val incentivesList: LiveData<List<Incentive>>
        get() = _incentivesList

    fun fetchActiveIncentives(){
        if (isLoading()) return
        startLoading()
        driverRepository.getActiveIncentives()
            .catch(errorHandler)
            .onEach {
                stopLoading()
                _incentivesList.value = it
            }
            .launchIn(viewModelScope)
    }

    fun fetchIncentiveHistory(){
        if (isLoading()) return
        startLoading()
        driverRepository.getIncentiveHistory()
            .catch(errorHandler)
            .onEach {
                stopLoading()
                _incentivesList.value = it
            }
            .launchIn(viewModelScope)
    }
}