package com.bluebrand.beedriver.presentation.util.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow


object NetworkConnectivity {
    fun getConnectivityUpdatesFlow(context: Context): Flow<Boolean> = callbackFlow {
        val networkRequest = NetworkRequest.Builder()
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .addTransportType(NetworkCapabilities.TRANSPORT_VPN)
            .build()

        val networkCallback = object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network)
                trySend(true)
            }

            override fun onLost(network: Network) {
                super.onLost(network)
                trySend(false)
            }
        }

        val connectivityManager = getConnectivityManager(context)
        connectivityManager.requestNetwork(networkRequest, networkCallback)

        trySend(isConnected(context))

        awaitClose { connectivityManager.unregisterNetworkCallback(networkCallback) }
    }


    fun isConnected(context: Context): Boolean {
        val connectivityManager = getConnectivityManager(context)

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            listOf(
                networkCapabilities?.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ?: false,
                networkCapabilities?.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ?: false,
                networkCapabilities?.hasTransport(NetworkCapabilities.TRANSPORT_VPN) ?: false
            ).any { it }
        } else {
            connectivityManager.activeNetworkInfo?.isConnectedOrConnecting ?: false
        }
    }

    private fun getConnectivityManager(context: Context): ConnectivityManager {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            context.getSystemService(ConnectivityManager::class.java) as ConnectivityManager
        } else {
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        }
    }
}