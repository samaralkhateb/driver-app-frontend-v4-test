package com.bluebrand.beedriver.presentation.ui.base.view.activity

import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.presentation.busevent.UnAuthorizedUserEvent
import com.bluebrand.beedriver.presentation.ui.base.view.IBaseView
import com.bluebrand.beedriver.presentation.ui.base.view.alert.IAlertDialog
import com.bluebrand.beedriver.presentation.ui.splash.SplashActivity
import com.bluebrand.beedriver.presentation.util.bus.Bus
import com.bluebrand.beedriver.presentation.util.navigation.ActivityResultObserver
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

abstract class BaseActivity : LocalizationActivity(), IBaseView {

    // checks if the activity running now (not on foreground or background)
    var isActivityRunning = false
        private set

    protected abstract val layoutId: Int
    private lateinit var dialogAlert: IAlertDialog

    lateinit var activityResultObserver: ActivityResultObserver<Intent, ActivityResult>
        private set

    override fun onResume() {
        super.onResume()
        isActivityRunning = true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initAlert()
        setView()
        registerActivityResultObserver()
        setupToolbar()
        registerEvents()
    }

    protected open fun setView() {
        setContentView(layoutId)
    }

    private fun registerActivityResultObserver() {
        activityResultObserver = ActivityResultObserver(
            activityResultRegistry,
            ActivityResultContracts.StartActivityForResult(),
            javaClass.name
        )
        lifecycle.addObserver(activityResultObserver)
    }

    private fun setupToolbar() {
        try {
            val toolbar = findViewById<Toolbar>(R.id.toolbar)
            if (toolbar != null) {
                setSupportActionBar(toolbar)
                changeToolbarTitle("")
            }
        } catch (ignored: Exception) {
        }
        try {
            findViewById<View>(R.id.back_button).setOnClickListener { onBackPressed() }
        } catch (ignored: Exception) {
        }
    }

    private fun initAlert() {
        dialogAlert = IAlertDialog.Factory.defaultAlert(this)
    }

    override fun onPause() {
        super.onPause()
        isActivityRunning = false
    }

    /*
     * Called when the language was changed
     * and the activity will be recreated by default
     * */
    override fun updateUI(newResources: Resources) {
        recreate()
    }

    // base methods
    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showMessage(stringId: Int) {
        showMessage(getString(stringId))
    }

    override fun showDialogMessage(title: Int, message: Int) {
        showDialogMessage(getString(title), getString(message))
    }

    override fun showDialogMessage(title: String?, message: String) {
        showDialogMessage(title, message, android.R.string.ok, android.R.string.cancel)
    }

    fun showDialogMessage(
        title: String?,
        message: String,
        @StringRes positiveText: Int = android.R.string.ok,
        @StringRes negativeText: Int = android.R.string.cancel,
        positiveListener: DialogInterface.OnClickListener? = null,
        negativeListener: DialogInterface.OnClickListener? = null,
        cancelable: Boolean = false,
        @ColorRes tintColorId: Int = R.color.colorPrimary
    ) {
        dialogAlert.showDialogMessage(
            title, message,
            getString(positiveText), getString(negativeText),
            positiveListener, negativeListener,
            cancelable,
            tintColorId
        )
    }

    fun buildDialog(
        title: String,
        message: String,
        @StringRes positiveText: Int = android.R.string.ok,
        @StringRes negativeText: Int = android.R.string.cancel,
        positiveListener: DialogInterface.OnClickListener? = null,
        negativeListener: DialogInterface.OnClickListener? = null,
        cancelable: Boolean = false
    ): Dialog {
        return dialogAlert.buildDialog(
            title, message,
            getString(positiveText), getString(negativeText),
            positiveListener, negativeListener,
            cancelable
        )
    }

    open fun showSnackBar(
        view: View,
        message: String,
        actionText: String,
        action: View.OnClickListener
    ) {
        Snackbar
            .make(view, message, Snackbar.LENGTH_INDEFINITE)
            .setActionTextColor(ContextCompat.getColor(this, R.color.white))
            .setAction(actionText, action)
            .show()
    }

    override fun hideKeyboard() {
        var currentFocus = currentFocus
        if (currentFocus == null) currentFocus = View(this)
        val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as? InputMethodManager
        inputMethodManager?.hideSoftInputFromWindow(currentFocus.windowToken, 0)
    }

    fun changeToolbarTitle(title: String) {
        supportActionBar?.title = title
    }

    fun setFullScreen() {
        val w = window
        w.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
    }

    protected open fun registerEvents() {
        registerUnAuthorizedUserEvent()
    }

    private fun registerUnAuthorizedUserEvent() {
        Bus.eventFlowOf(UnAuthorizedUserEvent::class.java)
            .onEach {
                onUnAuthorizedUserEvent()
            }
            .launchIn(lifecycleScope)
    }

    private fun onUnAuthorizedUserEvent() {
        showMessage(R.string.msg_token_invalid)
        startActivity(Intent(this, SplashActivity::class.java))
        finishAffinity()
        isActivityRunning = false
    }
}