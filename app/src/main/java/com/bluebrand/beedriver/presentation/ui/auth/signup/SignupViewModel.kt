package com.bluebrand.beedriver.presentation.ui.auth.signup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bluebrand.beedriver.data.model.City
import com.bluebrand.beedriver.data.model.Vehicle
import com.bluebrand.beedriver.data.repository.registration.IRegistrationRepository
import com.bluebrand.beedriver.presentation.ui.base.view.BaseViewModel
import com.bluebrand.beedriver.presentation.ui.common.list.listlivedata.ListLiveData
import com.bluebrand.beedriver.presentation.util.lifecycle.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class SignupViewModel @Inject constructor(
    private val registrationRepository: IRegistrationRepository
) : BaseViewModel() {
    val signupForm = SignupForm()

    private val _dataError = MutableLiveData<Event<String>>()
    val dataError: LiveData<Event<String>>
        get() = _dataError

    private val _cityListLiveData = MutableLiveData<List<City>>()
    val cityListLiveData: LiveData<List<City>>
        get() = _cityListLiveData

    private val _vehicleTypesLiveData = MutableLiveData<List<Vehicle>>()
    val vehicleTypesLiveData: LiveData<List<Vehicle>>
        get() = _vehicleTypesLiveData

    private val _signupStatus = MutableLiveData<Event<Boolean>>()
    val signupStatus: LiveData<Event<Boolean>>
        get() = _signupStatus

    fun fetchData() {
        startLoading()

        val citesFlow = registrationRepository.getCites()
            .take(1)
            .onEach {
                _cityListLiveData.value = it
            }

        val vehiclesFlow = registrationRepository.getVehicleTypes()
            .take(1)
            .onEach {
                _vehicleTypesLiveData.value = it
            }

        flowOf(citesFlow, vehiclesFlow)
            .flattenMerge()
            .catch { error ->
                error.printStackTrace()
                stopLoading()
                _dataError.value = Event(error.message ?: "")
            }
            .onCompletion {
                stopLoading()
            }
            .launchIn(viewModelScope)
    }

    fun signup(cityId: Int, vehicleTypeId: Int) {
        hideKeyboard()

        if (isLoading()) return

        if (signupForm.allowed()) {
            startLoading()
            registrationRepository.signup(
                cityId,
                vehicleTypeId,
                signupForm.phoneNumber.value!!,
                signupForm.firstName.value!!,
                signupForm.lastName.value!!,
                signupForm.licensePlateNumber.value!!
            )
                .catch(errorHandler)
                .onEach {
                    stopLoading()
                    _signupStatus.value = Event(it)
                }
                .launchIn(viewModelScope)
        } else {
            showMessage(signupForm.errorResource)
        }
    }
}