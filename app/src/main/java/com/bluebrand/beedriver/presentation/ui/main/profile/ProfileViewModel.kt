package com.bluebrand.beedriver.presentation.ui.main.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bluebrand.beedriver.data.repository.driver.IDriverRepository
import com.bluebrand.beedriver.data.source.remote.model.driver.profile.ProfileData
import com.bluebrand.beedriver.presentation.ui.base.view.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val driverRepository: IDriverRepository
) : BaseViewModel() {
    private val _profileData = MutableLiveData<ProfileData>()
    val profileData: LiveData<ProfileData> = _profileData

    init {
        fetchProfileData()
    }

    override fun refresh() {
        super.refresh()
        fetchProfileData()
    }

    fun fetchProfileData() {
        if (isLoading()) return
        startLoading()
        driverRepository.getProfileData()
            .catch(snackErrorHandler)
            .onEach {
                _profileData.value = it
                stopLoading()
            }
            .launchIn(viewModelScope)
    }
}