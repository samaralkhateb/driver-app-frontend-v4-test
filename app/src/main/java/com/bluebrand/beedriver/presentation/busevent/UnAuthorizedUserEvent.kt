package com.bluebrand.beedriver.presentation.busevent

import com.bluebrand.beedriver.presentation.util.bus.BusEvent

object UnAuthorizedUserEvent : BusEvent()