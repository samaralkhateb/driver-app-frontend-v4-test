package com.bluebrand.beedriver.presentation.ui.tutorial

import android.os.Bundle
import android.view.View
import com.bluebrand.beedriver.BR
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.databinding.ActivityTutorialBinding
import com.bluebrand.beedriver.presentation.ui.auth.AuthenticationActivity
import com.bluebrand.beedriver.presentation.ui.base.view.activity.MVVMActivity
import com.bluebrand.beedriver.presentation.util.lifecycle.EventObserver
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TutorialActivity : MVVMActivity<TutorialViewModel, ActivityTutorialBinding>() {
    private val tutorialPagesData = listOf(
        TutorialPageData(R.string.lbl_step1, R.string.lbl_step1_msg, R.drawable.bg_splash_one),
        TutorialPageData(R.string.lbl_step2, R.string.lbl_step2_msg, R.drawable.bg_splash_two),
        TutorialPageData(
            R.string.lbl_step3,
            R.string.lbl_step3_msg,
            R.drawable.bg_splash_three,
            true
        )
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupPagesAdapters()
        registerObservers()
    }

    private val onStartButtonClicked = { _: View, _: Int ->
        viewModel.checkIfSignupEnabled()
    }

    private fun setupPagesAdapters() {
        val adapter = TutorialPagesAdapter(this, onStartButtonClicked)
        mActivityBinding.viewPager.adapter = adapter
        adapter.submitData(tutorialPagesData)

        TabLayoutMediator(
            mActivityBinding.dotsIndicator,
            mActivityBinding.viewPager
        ) { _, _ ->
        }.attach()
    }

    private fun registerObservers() {
        viewModel.isSignupEnabled.observe(
            this,
            EventObserver { result ->
                startActivity(
                    AuthenticationActivity.getIntent(this, result)
                )
            }
        )
    }

    override val layoutId: Int
        get() = R.layout.activity_tutorial

    override val viewModelId: Int
        get() = BR.viewModel

    override val viewModelClass: Class<TutorialViewModel>
        get() = TutorialViewModel::class.java
}