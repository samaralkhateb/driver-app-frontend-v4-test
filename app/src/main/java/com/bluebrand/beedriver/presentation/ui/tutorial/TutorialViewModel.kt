package com.bluebrand.beedriver.presentation.ui.tutorial

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bluebrand.beedriver.data.repository.registration.IRegistrationRepository
import com.bluebrand.beedriver.presentation.ui.base.view.BaseViewModel
import com.bluebrand.beedriver.presentation.util.lifecycle.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class TutorialViewModel @Inject constructor(
    private val registrationRepository: IRegistrationRepository
) : BaseViewModel() {

    private val _isSignupEnabled = MutableLiveData<Event<Boolean>>()
    val isSignupEnabled: LiveData<Event<Boolean>>
        get() = _isSignupEnabled

    fun checkIfSignupEnabled() {
        if (isLoading()) return
        startLoading()

        registrationRepository.checkIfSignupEnabled()
            .catch(errorHandler)
            .onEach {
                stopLoading()
                _isSignupEnabled.value = Event(it)
            }
            .launchIn(viewModelScope)
    }
}