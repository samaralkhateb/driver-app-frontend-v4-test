package com.bluebrand.beedriver.presentation.ui.main.transactions.income.weekly

import androidx.recyclerview.widget.RecyclerView
import com.bluebrand.beedriver.presentation.ui.main.transactions.income.base.BaseIncomeFragment
import com.bluebrand.beedriver.presentation.ui.main.transactions.income.base.IncomeInfoListAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WeeklyIncomeHistoryFragment : BaseIncomeFragment<WeeklyIncomeHistoryViewModel>() {

    override fun provideRecyclerAdapter(): RecyclerView.Adapter<*> {
        return IncomeInfoListAdapter(requireContext())
    }

    override val viewModelClass: Class<WeeklyIncomeHistoryViewModel>
        get() = WeeklyIncomeHistoryViewModel::class.java
}