package com.bluebrand.beedriver.presentation.exception

import androidx.annotation.StringRes
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.exception.NoInternetException
import com.bluebrand.beedriver.data.exception.RequestTimedOutException
import com.bluebrand.beedriver.data.exception.RouteFetchException
import com.bluebrand.beedriver.data.exception.ServerException
import java.io.IOException
import java.net.SocketTimeoutException

object ExceptionFactory {
    @StringRes
    fun getStringResOf(e: Exception): Int {
        return when (e) {
            is NoInternetException -> R.string.please_check_your_internet
            is ServerException, is IOException, is SocketTimeoutException -> R.string.msg_something_went_wrong
            is RequestTimedOutException -> R.string.msg_request_timed_out
            is RouteFetchException -> R.string.msg_failed_fetch_route
            else -> R.string.unknown_error
        }
    }

    @StringRes
    fun getString(throwable: Throwable): Int {
        return (throwable as? Exception)?.let { getStringResOf(it) }
            ?: R.string.unknown_error
    }
}