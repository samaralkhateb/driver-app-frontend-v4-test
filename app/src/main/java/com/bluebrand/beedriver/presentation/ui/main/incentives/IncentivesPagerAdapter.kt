package com.bluebrand.beedriver.presentation.ui.main.incentives

import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.presentation.ui.main.incentives.list.active.ActiveIncentivesFragment
import com.bluebrand.beedriver.presentation.ui.main.incentives.list.history.IncentiveHistoryFragment

class IncentivesPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    override fun getItemCount() = 2

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> ActiveIncentivesFragment()
            1 -> IncentiveHistoryFragment()
            else -> throw IllegalStateException("Unexpected position: ($position)")
        }
    }

    @StringRes
    fun getTitle(position: Int): Int {
        return when (position) {
            0 -> R.string.lbl_active_incentives
            1 -> R.string.lbl_incentive_history
            else -> throw IllegalStateException("Unexpected position: ($position)")
        }
    }
}