package com.bluebrand.beedriver.presentation.ui.common.list.listlivedata.event

import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseListAdapter

class ClearAllEvent<T> : ListEvent<T>(emptyList()) {
    override fun applyEvent(adapter: BaseListAdapter<T, *>) {
        adapter.clearData()
    }
}