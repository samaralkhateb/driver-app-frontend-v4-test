package com.bluebrand.beedriver.presentation.ui.main.profile.arcr

import android.content.Context
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.model.ArCrInfo
import com.bluebrand.beedriver.databinding.RowArCrBinding
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseListAdapter
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseViewHolder

class ArCrHistoryListAdapter(context: Context) :
    BaseListAdapter<ArCrInfo, ArCrHistoryListAdapter.ArCrInfoViewHolder>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArCrInfoViewHolder {
        val binding = DataBindingUtil.inflate<RowArCrBinding>(
            provideInflater(),
            R.layout.row_ar_cr,
            parent,
            false
        )

        return ArCrInfoViewHolder(binding)
    }

    class ArCrInfoViewHolder(binding: RowArCrBinding) :
        BaseViewHolder<ArCrInfo, RowArCrBinding>(binding) {
        override fun onBind(item: ArCrInfo, position: Int) {
            binding.arInfo = item
        }
    }
}