package com.bluebrand.beedriver.presentation.ui.base.view.fragment

import android.content.Context
import androidx.databinding.ViewDataBinding
import com.bluebrand.beedriver.presentation.ui.base.view.IBaseView
import android.view.ViewGroup
import android.view.LayoutInflater
import android.os.Bundle
import android.graphics.drawable.ColorDrawable
import androidx.databinding.DataBindingUtil
import android.widget.Toast
import android.content.DialogInterface
import android.graphics.Color
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.StringRes
import androidx.fragment.app.DialogFragment
import com.bluebrand.beedriver.presentation.ui.base.view.activity.BaseActivity

abstract class BaseFragmentDialog<DB : ViewDataBinding> : DialogFragment(), IBaseView {
    protected lateinit var binding: DB

    protected abstract val layoutId: Int

    protected fun setFullScreen() {
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        dialog?.window?.setLayout(width, height)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        binding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        binding.lifecycleOwner = requireActivity()
        binding.executePendingBindings()
        return binding.root
    }

    override fun showMessage(stringId: Int) {
        showMessage(getString(stringId))
    }

    override fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun showDialogMessage(title: Int, message: Int) {
        showDialogMessage(getString(title), getString(message))
    }

    override fun showDialogMessage(title: String?, message: String) {
        showDialogMessage(title, message, android.R.string.ok, android.R.string.cancel)
    }

    fun showDialogMessage(
        title: String?,
        message: String,
        @StringRes positiveText: Int = android.R.string.ok,
        @StringRes negativeText: Int = android.R.string.cancel,
        positiveListener: DialogInterface.OnClickListener? = null,
        negativeListener: DialogInterface.OnClickListener? = null,
        cancelable: Boolean = false
    ) {
        val baseActivity = requireActivity() as? BaseActivity
        baseActivity?.showDialogMessage(
            title, message,
            positiveText, negativeText,
            positiveListener, negativeListener,
            cancelable
        )
    }

    override fun hideKeyboard() {
        var currentFocus = activity?.currentFocus
        if (currentFocus == null) currentFocus = View(context)
        val inputMethodManager =
            context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        inputMethodManager?.hideSoftInputFromWindow(currentFocus.windowToken, 0)
    }
}