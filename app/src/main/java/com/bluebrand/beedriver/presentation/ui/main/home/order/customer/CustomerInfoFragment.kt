package com.bluebrand.beedriver.presentation.ui.main.home.order.customer

import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.lifecycle.ViewModelStoreOwner
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.bluebrand.beedriver.BR
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.model.OrderDetails
import com.bluebrand.beedriver.databinding.FragmentRouteToCustomerBinding
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.BaseMapBoxFragment
import com.bluebrand.beedriver.presentation.ui.main.home.HomeViewModel
import com.bluebrand.beedriver.presentation.util.locale.LanguagesCode
import com.bluebrand.beedriver.presentation.util.locale.LocaleManager
import com.bluebrand.beedriver.presentation.util.map.toLatLngList
import com.bluebrand.beedriver.presentation.util.navigation.NavigationUtil
import com.bluebrand.beedriver.presentation.util.resourses.BitmapUtil
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.Symbol
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CustomerInfoFragment : BaseMapBoxFragment<HomeViewModel, FragmentRouteToCustomerBinding>() {

    private lateinit var navController: NavController

    private var driverMarker: Symbol? = null
    private var customerMarker: Symbol? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        viewModel.fetchCustomerRoad()

        registerListeners()
        registerObservers()
    }

    private fun registerListeners() {
        binding.motionLayoutCustomerInfo.setTransitionListener(object :
            MotionLayout.TransitionListener {
            override fun onTransitionTrigger(
                motionLayout: MotionLayout?,
                triggerId: Int,
                positive: Boolean,
                progress: Float
            ) {
            }

            override fun onTransitionStarted(
                motionLayout: MotionLayout?,
                startId: Int,
                endId: Int
            ) {

            }

            override fun onTransitionChange(
                motionLayout: MotionLayout?,
                startId: Int,
                endId: Int,
                progress: Float
            ) {
                val mirrored = if (LocaleManager.savedLanguage == LanguagesCode.Arabic) -1 else 1
                binding.ivArrow.rotation = mirrored * -90f * progress
            }

            override fun onTransitionCompleted(motionLayout: MotionLayout?, currentId: Int) {
            }
        })

        binding.buttonCall.setOnClickListener {
            val customerInfo = viewModel.currentActiveOrder.value?.customerInfo
            if (customerInfo != null) {
                startActivity(
                    NavigationUtil.dialIntent(customerInfo.mobile)
                )
            }
        }

        binding.buttonMyLocation.setOnClickListener {
            adjustZoom()
            viewModel.fetchCustomerRoad()
        }

        binding.buttonOrderDelivered.setOnClickListener {
            navController.navigate(
                CustomerInfoFragmentDirections
                    .actionCustomerInfoFragmentToCustomerPaymentGatewayDialog()
            )
        }
    }

    private fun registerObservers() {
        viewModel.currentActiveOrder.observe(viewLifecycleOwner, this::addCustomerMarker)
        viewModel.driverLocation.observe(viewLifecycleOwner, this::updateDriverMarker)
        viewModel.customerRoad.observe(viewLifecycleOwner) {
            drawRoute(it.toLatLngList())
        }
    }

    override fun onStyleReady(style: Style) {
        super.onStyleReady(style)

        viewModel.currentActiveOrder.value?.let { addCustomerMarker(it, true) }
        viewModel.driverLocation.value?.let { updateDriverMarker(it) }
        viewModel.customerRoad.value?.let { drawRoute(it.toLatLngList()) }
    }

    override fun registerIcons(style: Style) {
        super.registerIcons(style)
        BitmapUtil.bitmapFromDrawableRes(
            requireContext(),
            R.drawable.ic_customer_marker
        )?.let {
            style.addImage(MARKER_CUSTOMER, it)
        }
    }

    private fun addCustomerMarker(orderDetails: OrderDetails?, adjustZoom: Boolean = false) {
        if (orderDetails == null) return

        customerMarker?.let { removeMarker(it) }
        customerMarker = addMarker(
            orderDetails.customerInfo.latitude,
            orderDetails.customerInfo.longitude,
            MARKER_CUSTOMER
        )

        if (adjustZoom) {
            adjustZoom()
        } else {
            moveCamera(
                LatLng(
                    orderDetails.customerInfo.latitude,
                    orderDetails.customerInfo.longitude,
                ),
                map?.cameraPosition?.zoom?.toFloat() ?: DEFAULT_ZOOM
            )
        }
    }

    private fun updateDriverMarker(location: Location) {
        driverMarker?.let { removeMarker(it) }
        driverMarker = addMarker(location.latitude, location.longitude)

        adjustZoom()
    }

    private fun adjustZoom() {
        val customerInfo = viewModel.currentActiveOrder.value?.customerInfo
        val driverLocation = viewModel.driverLocation.value

        if (customerInfo != null && driverLocation != null) {
            moveCamera(
                LatLng(
                    customerInfo.latitude,
                    customerInfo.longitude,
                ),
                LatLng(
                    driverLocation.latitude,
                    driverLocation.longitude,
                ),
            )
        }
    }

    override val mapView: MapView
        get() = binding.mapView

    override val layoutId: Int
        get() = R.layout.fragment_route_to_customer

    override val viewModelClass: Class<HomeViewModel>
        get() = HomeViewModel::class.java

    override val viewModelOwner: ViewModelStoreOwner
        get() = requireActivity()

    override val viewModelId: Int
        get() = BR.viewModel

    companion object {
        const val MARKER_CUSTOMER = "marker_customer"
    }
}