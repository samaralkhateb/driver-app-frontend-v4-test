package com.bluebrand.beedriver.presentation.ui.main.profile.arcr

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.model.ArCrHistory
import com.bluebrand.beedriver.databinding.ActivityArCrHistoryBinding
import com.bluebrand.beedriver.presentation.ui.base.view.activity.BindingActivity

class ArCrHistoryActivity : BindingActivity<ActivityArCrHistoryBinding>() {
    companion object {
        const val KEY_AR_CR_HISTORY_MODEL = "key_ar_cr_history_model"

        fun getIntent(context: Context, arCrHistory: ArCrHistory): Intent {
            return Intent(context, ArCrHistoryActivity::class.java).apply {
                putExtra(KEY_AR_CR_HISTORY_MODEL, arCrHistory)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkExtras()
    }

    private fun checkExtras() {
        val arCrHistory =
            intent.getSerializableExtra(KEY_AR_CR_HISTORY_MODEL) as? ArCrHistory
        if (arCrHistory != null){
            val adapter = ArCrHistoryListAdapter(this)
            mActivityBinding.layoutArCrHistoryTable.rvArCrHistory.adapter = adapter

            adapter.submitData(arCrHistory.arCrInfoList)
        }
    }

    override val layoutId: Int
        get() = R.layout.activity_ar_cr_history
}