package com.bluebrand.beedriver.presentation.ui.main.home.order

import android.content.Context
import android.view.LayoutInflater
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.model.OrderDetails
import com.bluebrand.beedriver.presentation.ui.base.adapter.TabLayoutAdapter
import com.google.android.material.tabs.TabLayout

class ActiveOrdersTabLayoutAdapter(
    context: Context,
    tabLayout: TabLayout
) : TabLayoutAdapter<OrderDetails>(context, tabLayout) {

    override fun onBindTab(tab: TabLayout.Tab, position: Int) {
        tab.text = context.getString(R.string.format_order_no, data[position].restaurantQueue);
        tab.view.isEnabled = false
    }

    override fun onCreateTab(inflater: LayoutInflater, position: Int): TabLayout.Tab {
        return tabLayout.newTab()
    }
}