package com.bluebrand.beedriver.presentation.ui.main.transactions.income.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bluebrand.beedriver.data.model.IncomeInfo
import com.bluebrand.beedriver.presentation.ui.base.view.BaseViewModel

abstract class BaseIncomeViewModel : BaseViewModel(){
    private val _incomeList = MutableLiveData<List<IncomeInfo>>()
    val incomeList: LiveData<List<IncomeInfo>> = _incomeList

    override fun refresh() {
        fetchData()
    }

    abstract fun fetchData()

    protected fun setIncomeData(data: List<IncomeInfo>){
        _incomeList.value = data
    }
}