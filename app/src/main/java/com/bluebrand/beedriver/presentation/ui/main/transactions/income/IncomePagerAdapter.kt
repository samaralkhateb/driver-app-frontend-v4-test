package com.bluebrand.beedriver.presentation.ui.main.transactions.income

import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.presentation.ui.main.transactions.income.daily.DailyIncomeHistoryFragment
import com.bluebrand.beedriver.presentation.ui.main.transactions.income.monthly.MonthlyIncomeHistoryFragment
import com.bluebrand.beedriver.presentation.ui.main.transactions.income.weekly.WeeklyIncomeHistoryFragment

class IncomePagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    override fun getItemCount() = 3

    override fun createFragment(position: Int): Fragment {
        return when(position){
            0 -> DailyIncomeHistoryFragment()
            1 -> WeeklyIncomeHistoryFragment()
            2 -> MonthlyIncomeHistoryFragment()
            else -> throw IllegalStateException("Unexpected position: ($position)")
        }
    }

    @StringRes
    fun getTitle(position: Int): Int {
        return when (position) {
            0 -> R.string.lbl_daily
            1 -> R.string.lbl_weekly
            2 -> R.string.lbl_monthly
            else -> throw IllegalStateException("Unexpected position: ($position)")
        }
    }
}