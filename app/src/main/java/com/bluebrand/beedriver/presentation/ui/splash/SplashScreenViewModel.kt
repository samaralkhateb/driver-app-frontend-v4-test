package com.bluebrand.beedriver.presentation.ui.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bluebrand.beedriver.data.repository.auth.IAuthRepository
import com.bluebrand.beedriver.presentation.ui.base.view.BaseViewModel
import com.bluebrand.beedriver.presentation.util.lifecycle.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class SplashScreenViewModel @Inject constructor(
    private val authRepository: IAuthRepository
) : BaseViewModel() {
    private val _isLoggedIn = MutableLiveData<Event<Boolean>>()
    val isLoggedIn: LiveData<Event<Boolean>>
        get() = _isLoggedIn

    fun checkIfLoggedIn(){
        authRepository.isLoggedIn()
            .catch(errorHandler)
            .onEach { result ->
                _isLoggedIn.value = Event(result)
            }
            .launchIn(viewModelScope)
    }
}