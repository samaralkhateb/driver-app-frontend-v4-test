package com.bluebrand.beedriver.presentation.ui.main.home.order

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelStoreOwner
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import com.bluebrand.beedriver.BR
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.model.OrderDetails
import com.bluebrand.beedriver.databinding.FragmentOngoingOrderBinding
import com.bluebrand.beedriver.presentation.busevent.EnterFullScreenEvent
import com.bluebrand.beedriver.presentation.busevent.ExitFullScreenEvent
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.MVVMFragment
import com.bluebrand.beedriver.presentation.ui.main.home.HomeViewModel
import com.bluebrand.beedriver.presentation.util.bus.Bus
import com.bluebrand.beedriver.presentation.util.resourses.ProgressDialogUtil
import com.google.android.material.tabs.TabLayout
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OnGoingOrderFragment : MVVMFragment<HomeViewModel, FragmentOngoingOrderBinding>() {
    private lateinit var parentNavController: NavController
    private lateinit var childNavController: NavController

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        parentNavController = Navigation.findNavController(view)

        val fragmentContainer = view.findViewById<View>(R.id.nav_host_ongoing_orders)
        childNavController = Navigation.findNavController(fragmentContainer)

        setupTabLayout()
        registerListeners()
        registerObservers()
    }

    private fun setupTabLayout() {
        val tabLayoutAdapter =
            ActiveOrdersTabLayoutAdapter(requireContext(), binding.tabLayoutOrders)
        viewModel.activeOrders.observe(viewLifecycleOwner, tabLayoutAdapter::setData)

        binding.tabLayoutOrders.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab != null) {
                    viewModel.setCurrentActiveOrder(
                        tabLayoutAdapter.data[tab.position]
                    )
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })
    }

    private fun registerListeners() {
        binding.buttonMinimize.setOnClickListener {
            parentNavController.popBackStack(R.id.homeFragment, false)
        }
    }

    private fun registerObservers() {
        viewModel.currentActiveOrder.observe(viewLifecycleOwner) { activeOrder: OrderDetails? ->
            if (activeOrder == null) {
                parentNavController.popBackStack(R.id.homeFragment, false)
            } else if (activeOrder.isOutForDelivery) {
                navigateToCustomerInfoFragment()
            } else {
                if (activeOrder.arrivedToRestaurant) {
                    navigateToOrderItemsFragment()
                } else {
                    navigateToRestaurantInfoFragment()
                }
            }
        }

        viewModel.isLoading.observe(viewLifecycleOwner) {
            if (it) ProgressDialogUtil.showProgressDialog(requireContext())
            else ProgressDialogUtil.hideProgressDialog()
        }
    }

    private fun navigateToCustomerInfoFragment() {
        val navOption = NavOptions.Builder()
            .setPopUpTo(R.id.nav_ongoing_orders, false)
            .build()

        childNavController.navigate(R.id.customerInfoFragment, null, navOption)
    }

    private fun navigateToRestaurantInfoFragment() {
        val navOption = NavOptions.Builder()
            .setPopUpTo(R.id.nav_ongoing_orders, false)
            .build()

        childNavController.navigate(R.id.restaurantInfoFragment, null, navOption)
    }

    private fun navigateToOrderItemsFragment() {
        val navOption = NavOptions.Builder()
            .setPopUpTo(R.id.nav_ongoing_orders, false)
            .build()

        childNavController.navigate(R.id.orderItemsFragment, null, navOption)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        changeStatusBarColor(R.color.colorPrimary)
        Bus.publishEvent(EnterFullScreenEvent)
    }

    override fun onDetach() {
        super.onDetach()
        changeStatusBarColor(R.color.white)
        Bus.publishEvent(ExitFullScreenEvent)
    }

    override val layoutId: Int
        get() = R.layout.fragment_ongoing_order

    override val viewModelClass: Class<HomeViewModel>
        get() = HomeViewModel::class.java

    override val viewModelOwner: ViewModelStoreOwner
        get() = requireActivity()

    override val viewModelId: Int
        get() = BR.viewModel
}