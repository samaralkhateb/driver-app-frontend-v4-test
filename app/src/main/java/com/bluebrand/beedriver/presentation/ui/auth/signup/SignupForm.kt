package com.bluebrand.beedriver.presentation.ui.auth.signup

import androidx.lifecycle.MutableLiveData
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.presentation.ui.base.view.BaseForm

class SignupForm : BaseForm() {
    val phoneNumber = MutableLiveData("")
    val firstName = MutableLiveData("")
    val lastName = MutableLiveData("")
    val licensePlateNumber = MutableLiveData("")

    init {
        addMutableLiveDataStringField(
            phoneNumber,
            firstName,
            lastName,
            licensePlateNumber
        )
    }
}