package com.bluebrand.beedriver.presentation.ui.common.list.listlivedata.event

import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseListAdapter

/**
 * Removes the item in the passed index.
 */
class RemoveIndexEvent<T>(allData: List<T>?, private val indexToRemove: Int) : ListEvent<T>(
    allData!!
) {
    override fun applyEvent(adapter: BaseListAdapter<T, *>) {
        adapter.removeItem(indexToRemove)
    }
}