package com.bluebrand.beedriver.presentation.ui.main.home

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.exception.FetchOrdersException
import com.bluebrand.beedriver.data.exception.OrderRemovedException
import com.bluebrand.beedriver.data.exception.StatusSyncException
import com.bluebrand.beedriver.data.model.DriverArCrInfo
import com.bluebrand.beedriver.data.model.DriverInfo
import com.bluebrand.beedriver.data.model.OrderDetails
import com.bluebrand.beedriver.data.repository.driver.IDriverRepository
import com.bluebrand.beedriver.data.repository.location.ILocationRepository
import com.bluebrand.beedriver.data.repository.order.IOrdersRepository
import com.bluebrand.beedriver.data.repository.socket.ISocketRepository
import com.bluebrand.beedriver.data.source.remote.model.order.DriverOrdersData
import com.bluebrand.beedriver.presentation.ui.base.view.BaseViewModel
import com.bluebrand.beedriver.presentation.ui.common.Constants
import com.bluebrand.beedriver.presentation.util.lifecycle.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.osmdroid.bonuspack.routing.Road
import org.osmdroid.util.GeoPoint
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val driverRepository: IDriverRepository,
    private val locationRepository: ILocationRepository,
    private val ordersRepository: IOrdersRepository,
    private var socketRepository: ISocketRepository
) : BaseViewModel() {

    lateinit var supportPhoneNumber: String
        private set

    private val _supportPhoneNumberVisibility = MutableLiveData(false)
    val supportPhoneNumberVisibility: LiveData<Boolean> = _supportPhoneNumberVisibility

    private val _isLastOrder = MutableLiveData<Boolean>()
    val isLastOrder: LiveData<Boolean> = _isLastOrder

    private val _isAutoAccept = MutableLiveData<Boolean>()
    val isAutoAccept: LiveData<Boolean> = _isAutoAccept

    private val _driverLocation = MutableLiveData<Location>()
    val driverLocation: LiveData<Location> = _driverLocation

    private val _driverInfo = MutableLiveData<DriverInfo>()
    val driverInfo: LiveData<DriverInfo> = _driverInfo

    private val _driverArCr = MutableLiveData<DriverArCrInfo?>()
    val driverArCr: LiveData<DriverArCrInfo?> = _driverArCr

    private val _activeOrders = MutableLiveData<List<OrderDetails>>()
    val activeOrders: LiveData<List<OrderDetails>> = _activeOrders

    private val _pendingOrders = MutableLiveData<List<OrderDetails>>()
    val pendingOrders: LiveData<List<OrderDetails>> = _pendingOrders

    private val _currentPendingOrder = MutableLiveData<OrderDetails?>()
    val currentPendingOrder: LiveData<OrderDetails?> = _currentPendingOrder

    private val _currentActiveOrder = MutableLiveData<OrderDetails?>()
    val currentActiveOrder: LiveData<OrderDetails?> = _currentActiveOrder

    private val _openOngoingOrders = MutableLiveData<Event<Boolean>>()
    val openOngoingOrders: LiveData<Event<Boolean>> = _openOngoingOrders

    private val _restaurantRoad = MutableLiveData<Road>()
    val restaurantRoad: LiveData<Road> = _restaurantRoad

    private val _customerRoad = MutableLiveData<Road>()
    val customerRoad: LiveData<Road> = _customerRoad

    init {
        fetchSupportPhoneNumber()
        listenForSocketConnectivity()
    }

    override fun refresh() {
        super.refresh()
        fetchDriverOrders()
    }

    private fun fetchSupportPhoneNumber() {
        driverRepository.getSupportPhoneNumber()
            .onEach { phone ->
                if (!phone.isNullOrBlank()) {
                    supportPhoneNumber = phone
                    _supportPhoneNumberVisibility.value = true
                }
            }
            .launchIn(viewModelScope)
    }

    fun toggleLastOrder() {
        if (isLoading()) return
        startLoading()

        val newStatus = !(_isLastOrder.value ?: false)

        _isLastOrder.value = newStatus

        driverRepository.setLastOrder(newStatus)
            .catch { error ->
                errorHandler.invoke(this, error)
                if (error is StatusSyncException) {
                    _isLastOrder.value = newStatus
                } else {
                    _isLastOrder.value = !newStatus
                }
            }
            .onEach { result ->
                stopLoading()
                val message = result.message
                if (result.isSuccessful()) {
                    if (message != null) showMessage(message)
                    else showMessage(R.string.msg_last_order_updated_successfully)
                } else {
                    _isLastOrder.value = !newStatus
                    if (message != null) showMessage(message)
                    else showMessage(R.string.msg_something_went_wrong)
                }
            }
            .launchIn(viewModelScope)
    }


    fun toggleAutoAccept() {
        val newStatus = !(_isAutoAccept.value ?: false)

        if (isLastOrder.value == true) {
            _isAutoAccept.value = !newStatus
            showMessage(R.string.msg_auto_accept_last_order_on_error)
            return
        }

        if (isLoading()) return
        startLoading()

        _isAutoAccept.value = newStatus

        driverRepository.setAutoAccept(newStatus)
            .catch { error ->
                errorHandler.invoke(this, error)
                if (error is StatusSyncException) {
                    _isAutoAccept.value = newStatus
                } else {
                    _isAutoAccept.value = !newStatus
                }
            }
            .onEach { result ->
                stopLoading()
                val message = result.message
                if (result.isSuccessful()) {
                    if (message != null) showMessage(message)
                    else showMessage(R.string.msg_last_order_updated_successfully)
                } else {
                    _isAutoAccept.value = !newStatus
                    if (message != null) showMessage(message)
                    else showMessage(R.string.msg_something_went_wrong)
                }
            }
            .launchIn(viewModelScope)
    }

    fun fetchLocationUpdates() {
        locationRepository
            .getLocationUpdates(10000)
            .filter { currentLocation ->
                val prevLocation = _driverLocation.value

                prevLocation == null ||
                        (currentLocation.distanceTo(prevLocation) > 10f)
            }
            .onEach(_driverLocation::setValue)
            .launchIn(viewModelScope)
    }

    fun fetchDriverOrders(isSocketReconnection: Boolean = false) {
        if (isLoading()) return
        startLoading()

        ordersRepository.getDriverOrders(isSocketReconnection)
            .catch { error ->
                if (error is FetchOrdersException) {
                    stopLoading()
                } else {
                    snackErrorHandler.invoke(this, error)
                }
            }
            .onEach {
                stopLoading()
                updateDriverOrders(it)
            }
            .launchIn(viewModelScope)
    }

    private fun setDriverInfo(driverInfo: DriverInfo) {
        _driverInfo.value = driverInfo
        _isLastOrder.value = driverInfo.lastOrderEnabled
        _isAutoAccept.value = driverInfo.autoAcceptEnabled
    }

    fun acceptOrder() {
        val currentPendingOrder = _currentPendingOrder.value
        val location = _driverLocation.value
        if (isLoading() || currentPendingOrder == null || location == null) return
        startLoading()

        ordersRepository.acceptOrder(
            location.latitude,
            location.longitude,
            currentPendingOrder.billId,
            currentPendingOrder.billDriverId
        ).catch { error ->
            if (error is OrderRemovedException) {
                removePendingOrder(currentPendingOrder)
            }
            errorHandler.invoke(this, error)
            setCurrentOrders()
        }.onEach {
            stopLoading()
            if (it) {
                removePendingOrder(currentPendingOrder)
                addActiveOrder(currentPendingOrder)
                if (_pendingOrders.value?.isEmpty() == true) _openOngoingOrders.value = Event(true)
            } else showMessage(R.string.msg_something_went_wrong)
            setCurrentOrders()
        }.launchIn(viewModelScope)
    }

    fun declineOrder() {
        val currentPendingOrder = _currentPendingOrder.value
        if (isLoading() || currentPendingOrder == null) return
        startLoading()

        ordersRepository.declineOrder(
            currentPendingOrder.billId,
        ).catch { error ->
            if (error is OrderRemovedException) {
                removePendingOrder(currentPendingOrder)
            }
            errorHandler.invoke(this, error)
            setCurrentOrders()
        }.onEach {
            stopLoading()
            if (it) {
                removePendingOrder(currentPendingOrder)
            } else showMessage(R.string.msg_something_went_wrong)
            setCurrentOrders()
        }.launchIn(viewModelScope)
    }

    fun removeOrderById(orderId: Long?) {
        val activeOrder = activeOrders.value?.firstOrNull { it.orderId == orderId }
        val pendingOrder = pendingOrders.value?.firstOrNull { it.orderId == orderId }

        activeOrder?.let { removeActiveOrder(it) }
        pendingOrder?.let { removePendingOrder(it) }
        setCurrentOrders()
    }

    private fun removePendingOrder(order: OrderDetails) {
        val orders = _pendingOrders.value?.toMutableList()
        orders?.remove(order)
        _pendingOrders.value = orders ?: emptyList()
    }

    private fun addActiveOrder(order: OrderDetails) {
        val orders = _activeOrders.value?.toMutableList()
        orders?.add(order)
        _activeOrders.value = orders ?: emptyList()
    }

    private fun setCurrentOrders() {
        _currentPendingOrder.value = _pendingOrders.value?.getOrNull(0)
        _currentActiveOrder.value = _activeOrders.value?.getOrNull(0)
    }

    fun setCurrentActiveOrder(order: OrderDetails) {
        _currentActiveOrder.value = order
    }

    fun arrivedToRestaurant() {
        val currentActiveOrder = _currentActiveOrder.value
        val driverLocation = driverLocation.value

        if (isLoading() || currentActiveOrder == null || driverLocation == null) return
        startLoading()

        ordersRepository.arrivedToRestaurant(
            currentActiveOrder.billId,
            driverLocation.latitude,
            driverLocation.longitude
        )
            .catch(errorHandler)
            .onEach {
                stopLoading()
                if (it) {
                    currentActiveOrder.arrivedToRestaurant = true
                    _currentActiveOrder.value = currentActiveOrder
                } else {
                    showMessage(R.string.msg_something_went_wrong)
                }
            }
            .launchIn(viewModelScope)
    }

    fun pickupOrder() {
        val currentActiveOrder = _currentActiveOrder.value
        if (isLoading() || currentActiveOrder == null) return
        startLoading()

        ordersRepository.pickupOrder(
            currentActiveOrder.billId,
            currentActiveOrder.billDriverId,
            currentActiveOrder.total
        )
            .catch(errorHandler)
            .onEach {
                stopLoading()
                if (it) {
                    currentActiveOrder.billStatus = Constants.BILL_STATUS_OUT_FOR_DELIVERY
                    _currentActiveOrder.value = currentActiveOrder
                } else {
                    showMessage(R.string.msg_something_went_wrong)
                }
            }
            .launchIn(viewModelScope)
    }

    fun orderDelivered() {
        val currentActiveOrder = _currentActiveOrder.value
        val driverLocation = driverLocation.value
        if (isLoading() || currentActiveOrder == null || driverLocation == null) return
        startLoading()

        ordersRepository.orderPaid(
            driverLocation.latitude,
            driverLocation.longitude,
            currentActiveOrder.billId,
            currentActiveOrder.netTotal,
            ""
        )
            .catch(errorHandler)
            .onEach {
                stopLoading()
                if (it) {
                    removeActiveOrder(currentActiveOrder)
                    setCurrentOrders()
                } else {
                    showMessage(R.string.msg_something_went_wrong)
                }
            }
            .launchIn(viewModelScope)
    }

    private fun removeActiveOrder(order: OrderDetails) {
        val orders = _activeOrders.value?.toMutableList()
        orders?.remove(order)
        _activeOrders.value = orders ?: emptyList()
    }

    fun fetchRestaurantRoad() {
        val currentActiveOrder = _currentActiveOrder.value
        val driverLocation = _driverLocation.value

        if (currentActiveOrder == null || driverLocation == null) return

        val geoPoints = mutableListOf<GeoPoint>()
        geoPoints.add(GeoPoint(driverLocation.latitude, driverLocation.longitude))
        geoPoints.add(
            GeoPoint(
                currentActiveOrder.restaurantLatitude,
                currentActiveOrder.restaurantLongitude
            )
        )

        locationRepository.getRoute(*geoPoints.toTypedArray())
            .catch { error ->
                error.printStackTrace()
            }
            .onEach {
                _restaurantRoad.value = it
            }
            .launchIn(viewModelScope)
    }

    fun fetchCustomerRoad() {
        val currentActiveOrder = _currentActiveOrder.value
        val driverLocation = _driverLocation.value

        if (currentActiveOrder == null || driverLocation == null) return

        val geoPoints = mutableListOf<GeoPoint>()
        geoPoints.add(GeoPoint(driverLocation.latitude, driverLocation.longitude))
        geoPoints.add(
            GeoPoint(
                currentActiveOrder.customerInfo.latitude,
                currentActiveOrder.customerInfo.longitude
            )
        )

        locationRepository.getRoute(*geoPoints.toTypedArray())
            .catch { error ->
                error.printStackTrace()
            }
            .onEach {
                _customerRoad.value = it
            }
            .launchIn(viewModelScope)
    }

    fun orderReceived() {
        val pendingOrder = _currentPendingOrder.value ?: return

        viewModelScope.launch {
            socketRepository.orderReceived(pendingOrder.orderId, pendingOrder.restaurantQueue)
        }
    }

    fun listenForDriverInformationUpdate() {
        socketRepository.driverInfoUpdated()
            .onEach {
                driverRepository.saveDriverInfo(it)

                _driverInfo.value = it

                val arCrInfo = _driverArCr.value
                arCrInfo?.acceptanceRate = it.acceptanceRate
                arCrInfo?.cancellationRate = it.cancellationRate

                _isAutoAccept.value = it.autoAcceptEnabled
                _isLastOrder.value = it.lastOrderEnabled

                _driverArCr.value = arCrInfo
            }
            .catch { e -> e.printStackTrace() }
            .launchIn(viewModelScope)
    }

    fun listenForNewOrders() {
        socketRepository.newOrderReceived()
            .onEach {
                updateDriverOrders(it)
                orderReceived()
            }
            .catch { e -> e.printStackTrace() }
            .launchIn(viewModelScope)
    }

    private fun updateDriverOrders(driverOrdersData: DriverOrdersData) {
        _driverArCr.value = driverOrdersData.arCrInfo
        setDriverInfo(driverOrdersData.driverInfo)
        _activeOrders.value = driverOrdersData.orders.filter { order -> !order.isPending }
        _pendingOrders.value = driverOrdersData.orders.filter { order -> order.isPending }
        setCurrentOrders()
    }

    private fun listenForSocketConnectivity() {
        socketRepository.getConnectionStatus()
            .onEach {
                if (it) fetchDriverOrders(true)
            }
            .catch { e -> e.printStackTrace() }
            .launchIn(viewModelScope)
    }
}