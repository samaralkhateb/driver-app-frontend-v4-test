package com.bluebrand.beedriver.presentation.ui.base.view

import androidx.lifecycle.MutableLiveData
import com.bluebrand.beedriver.R
import java.util.*

open class BaseForm {
    /**
     * set of mutable live data fields (any field can be filled with strings)
     * that will be mandatory for the form submission.
     */
    protected var mutableLiveDataFields: MutableList<MutableLiveData<String>> = ArrayList()

    @SafeVarargs
    protected fun addMutableLiveDataStringField(vararg mutableLiveDataFields: MutableLiveData<String>) {
        this.mutableLiveDataFields.addAll(Arrays.asList(*mutableLiveDataFields))
    }

    protected fun stringValueToInt(liveData: MutableLiveData<String?>): Int? {
        return if (liveData.value == null) null else liveData.value!!.toInt()
    }

    private fun noEmptyFields(): Boolean {
        for (field in mutableLiveDataFields) {
            if (field.value.isNullOrBlank()) {
                return false
            }
        }
        return true
    }

    /**
     * public method used to check for authority of submitting the fields on the base level.
     * this method can be overridden to perform checking on concrete levels.
     * @return true if fields are allowed to be submitted.
     */
    fun allowed(): Boolean {
        return noEmptyFields()
    }

    /**
     * public method used in getting the error resource.
     * usually this method is overridden to use provide error resources on concrete levels.
     * @return error string resource
     */
    open val errorResource: Int
        get() = if (!noEmptyFields()) {
            R.string.msg_please_fill_all_fields
        } else {
            0
        }
}