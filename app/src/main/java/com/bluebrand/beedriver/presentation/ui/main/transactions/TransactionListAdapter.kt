package com.bluebrand.beedriver.presentation.ui.main.transactions

import android.content.Context
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.model.Transaction
import com.bluebrand.beedriver.databinding.RowTransactionBinding
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseListAdapter
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseViewHolder

class TransactionListAdapter(context: Context) :
    BaseListAdapter<Transaction, TransactionListAdapter.TransactionRowViewHolder>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionRowViewHolder {
        val binding = DataBindingUtil.inflate<RowTransactionBinding>(
            provideInflater(),
            R.layout.row_transaction,
            parent,
            false
        )

        return TransactionRowViewHolder(binding)
    }

    class TransactionRowViewHolder(binding: RowTransactionBinding) :
        BaseViewHolder<Transaction, RowTransactionBinding>(binding) {
        override fun onBind(item: Transaction, position: Int) {
            binding.transaction = item
        }
    }
}