package com.bluebrand.beedriver.presentation.ui.main.home.order.restaurant

import android.content.Context
import android.view.ViewGroup
import com.bluebrand.beedriver.data.model.OrderItem
import com.bluebrand.beedriver.databinding.ItemOrderListBinding
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseListAdapter
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseViewHolder
import com.bluebrand.beedriver.presentation.ui.callback.OnItemSelectedListener

class OrderItemsAdapter(
    private val onItemChecked: OnItemSelectedListener<OrderItem>,
    context: Context
) : BaseListAdapter<OrderItem, OrderItemsAdapter.OrderItemViewHolder>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderItemViewHolder {
        val binding = ItemOrderListBinding.inflate(provideInflater(), parent, false)
        return OrderItemViewHolder(binding)
    }

    inner class OrderItemViewHolder(
        binding: ItemOrderListBinding
    ) : BaseViewHolder<OrderItem, ItemOrderListBinding>(binding) {
        override fun onBind(item: OrderItem, position: Int) {

            binding.orderItem = item
            binding.checkbox.isChecked = item.isChecked

            binding.checkbox.setOnClickListener { _ ->
                item.isChecked = !item.isChecked
                onItemChecked.onItemSelected(item, item.isChecked)
            }

            binding.root.setOnClickListener {
                item.isChecked = !item.isChecked
                binding.checkbox.isChecked = !binding.checkbox.isChecked

                onItemChecked.onItemSelected(item, item.isChecked)
            }
        }
    }
}