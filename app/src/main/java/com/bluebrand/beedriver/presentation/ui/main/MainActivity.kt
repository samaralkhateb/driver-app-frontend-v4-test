package com.bluebrand.beedriver.presentation.ui.main

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.os.*
import android.provider.Settings
import android.util.Log
import android.view.View
import androidx.activity.result.ActivityResultLauncher
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.bluebrand.beedriver.BR
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.model.DriverInfo
import com.bluebrand.beedriver.data.model.Notification
import com.bluebrand.beedriver.databinding.ActivityMainBinding
import com.bluebrand.beedriver.presentation.busevent.*
import com.bluebrand.beedriver.presentation.service.DriverLocationService
import com.bluebrand.beedriver.presentation.ui.base.view.activity.MVVMActivity
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.BaseMapBoxFragment
import com.bluebrand.beedriver.presentation.ui.common.Constants
import com.bluebrand.beedriver.presentation.util.application.AppUtil
import com.bluebrand.beedriver.presentation.util.bus.Bus
import com.bluebrand.beedriver.presentation.util.lifecycle.EventObserver
import com.bluebrand.beedriver.presentation.util.location.LocationUtil
import com.bluebrand.beedriver.presentation.util.navigation.NavigationUtil
import com.bluebrand.beedriver.presentation.util.network.NetworkConnectivity
import com.bluebrand.beedriver.presentation.util.notification.NotificationsUtil
import com.bluebrand.beedriver.presentation.util.permission.LocationPermissionUtil
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.offline.OfflineTilePyramidRegionDefinition
import com.mapbox.mapboxsdk.plugins.offline.model.NotificationOptions
import com.mapbox.mapboxsdk.plugins.offline.model.OfflineDownloadOptions
import com.mapbox.mapboxsdk.plugins.offline.offline.OfflineConstants
import com.mapbox.mapboxsdk.plugins.offline.offline.OfflineDownloadChangeListener
import com.mapbox.mapboxsdk.plugins.offline.offline.OfflinePlugin
import com.mapbox.mapboxsdk.plugins.offline.utils.OfflineUtils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach


@AndroidEntryPoint
class MainActivity : MVVMActivity<MainViewModel, ActivityMainBinding>() {

    private lateinit var navController: NavController
    private lateinit var permissionResultLauncher: ActivityResultLauncher<Array<String>>

    private var locationDialog: Dialog? = null
    private var locationPermissionDialog: Dialog? = null
    private var connectionDialog: Dialog? = null
    private var mapDialog: Dialog? = null
    private var batteryOptimizationDialog: Dialog? = null

    private var isDownloadingMap = false

    private val locationServiceIntent: Intent by lazy {
        Intent(this, DriverLocationService::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        navController = Navigation.findNavController(this, R.id.nav_host_main)
        permissionResultLauncher = LocationPermissionUtil.registerForResult(this)

        requestLocationPermissions()
        setupBottomNavView()
        setupListeners()
        registerObservers()
        checkIntent(intent)

        viewModel.connectToSocket()
        viewModel.listenForSocketNotifications()
        viewModel.listenForDriverInformationUpdate()
    }

    private fun setupBottomNavView() {
        mActivityBinding.bottomNavView.itemIconTintList = null
        mActivityBinding.bottomNavView.setupWithNavController(navController)

        mActivityBinding.bottomNavView.setOnItemSelectedListener {
            navController.popBackStack(R.id.homeFragment, false)
            NavigationUI.onNavDestinationSelected(it, navController)
        }
    }

    private fun requestLocationPermissions() {
        LocationPermissionUtil
            .requestLocationPermission(this, permissionResultLauncher)
            .onEach { result ->
                if (result) onLocationPermissionGranted()
                else showLocationPermissionDialog()
            }
            .launchIn(lifecycleScope)
    }

    private fun onLocationPermissionGranted() {
        startLocationService()
        if (LocationUtil.isLocationEnabled(this)) {
            viewModel.fetchGpsStatus()
        } else showLocationDialog()
    }

    private fun startLocationService() {
        checkPhoneStatePermission()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(locationServiceIntent)
        } else {
            startService(locationServiceIntent)
        }
    }

    private fun checkPhoneStatePermission() {
        val permissionCheck =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_PHONE_STATE),
                REQUEST_READ_PHONE_STATE
            )
        }
    }

    fun showLocationPermissionDialog() {
        locationPermissionDialog?.hide()

        locationPermissionDialog = buildDialog(
            title = getString(R.string.my_location),
            message = getString(R.string.msg_location_permission),
            positiveListener = { _, _ -> requestLocationPermissions() },
            negativeListener = { _, _ -> finishAffinity() },
            cancelable = false
        )

        locationPermissionDialog?.show()
    }

    private fun setupListeners() {
        mActivityBinding.switchStatus.setOnClickListener {
            viewModel.toggleStatus()
        }

        mActivityBinding.layoutDriverRank.setOnClickListener {
            if (viewModel.driverInfo.value?.isVip != true) {
                navController.navigate(R.id.ranksAndBenefitsFragment)
            }
        }

        mActivityBinding.layoutSocketConnected.buttonClose.setOnClickListener {
            mActivityBinding.layoutSocketConnected.root.visibility = View.GONE
        }
    }

    private fun registerObservers() {
        viewModel.gpsStatus.observe(this) { status ->
            if (!status) showLocationDialog()
            else hideLocationDialog()
        }

        viewModel.driverInfo.observe(this) { driverInfo: DriverInfo? ->
            driverInfo?.let {
                if (!viewModel.isMapDownloaded && !isDownloadingMap) showDownloadMapDialog(
                    it.cityId
                )
            }
        }

        viewModel.socketConnectionStatus.observe(this) {
            if (it == true) {
                Handler(Looper.getMainLooper()).postDelayed(
                    { mActivityBinding.layoutSocketConnected.root.visibility = View.GONE },
                    10 * 1000L
                )
            }
        }

        viewModel.socketNotification.observe(
            this,
            EventObserver { notification -> displayNotificationDialog(notification, true) }
        )

        Bus.eventFlowOf(HideBottomBarEvent::class.java)
            .onEach { mActivityBinding.bottomNavView.visibility = View.GONE }
            .launchIn(lifecycleScope)

        Bus.eventFlowOf(ShowBottomBarEvent::class.java)
            .onEach { mActivityBinding.bottomNavView.visibility = View.VISIBLE }
            .launchIn(lifecycleScope)

        Bus.eventFlowOf(EnterFullScreenEvent::class.java)
            .onEach {
                mActivityBinding.layoutHeader.visibility = View.GONE
                mActivityBinding.bottomNavView.visibility = View.GONE
            }
            .launchIn(lifecycleScope)

        Bus.eventFlowOf(ExitFullScreenEvent::class.java)
            .onEach {
                mActivityBinding.layoutHeader.visibility = View.VISIBLE
                mActivityBinding.bottomNavView.visibility = View.VISIBLE
            }
            .launchIn(lifecycleScope)

        Bus.eventFlowOf(NotificationReceivedEvent::class.java)
            .onEach { notificationEvent ->
                displayNotificationDialog(notificationEvent.notification, false)
            }
            .launchIn(lifecycleScope)

        listenForConnectivityChanges()
    }

    private fun listenForConnectivityChanges() {
        NetworkConnectivity
            .getConnectivityUpdatesFlow(this)
            .onEach { status ->
                if (!status) showConnectionDialog()
                else hideConnectionDialog()
            }
            .launchIn(lifecycleScope)
    }

    private fun showDownloadMapDialog(cityId: Int) {
        mapDialog?.dismiss()

        mapDialog = buildDialog(
            title = getString(R.string.app_name),
            message = getString(R.string.msg_map_download),
            positiveListener = { _, _ -> downloadMap(cityId) },
            cancelable = true
        )

        mapDialog?.show()
    }

    protected fun downloadMap(cityId: Int) {
        val latLngBounds = LatLngBounds.Builder()
        var region = ""
        when (cityId) {
            1 -> {
                region = "Damascus"
                latLngBounds.include(LatLng(33.559882, 36.332758))
                latLngBounds.include(LatLng(33.445674, 36.170702))
            }
            2 -> {
                region = "Rural Damascus"
                latLngBounds.include(LatLng(33.539797, 36.282205))
                latLngBounds.include(LatLng(33.453907, 36.294565))
            }
            3 -> {
                region = "Aleppo"
                latLngBounds.include(LatLng(36.275259, 37.162604))
                latLngBounds.include(LatLng(36.135954, 37.146437))
            }
            4 -> {
                region = "Lattakia"
                latLngBounds.include(LatLng(35.593583, 35.753709))
                latLngBounds.include(LatLng(35.496929, 35.778429))
            }
            5 -> {
                region = "Tartous"
                latLngBounds.include(LatLng(34.929848, 35.956724))
                latLngBounds.include(LatLng(34.846907, 35.895915))
            }
            7 -> {
                region = "Homs"
                latLngBounds.include(LatLng(34.772581, 36.764432))
                latLngBounds.include(LatLng(34.681955, 36.593367))
            }
            8 -> {
                region = "Hama"
                latLngBounds.include(LatLng(35.168810, 36.799159))
                latLngBounds.include(LatLng(35.097493, 36.696848))
            }
        }

        val definition = OfflineTilePyramidRegionDefinition(
            Style.MAPBOX_STREETS,
            latLngBounds.build(),
            BaseMapBoxFragment.DEFAULT_ZOOM - 2.0,
            BaseMapBoxFragment.DEFAULT_ZOOM + 2.0,
            resources.displayMetrics.density
        )

        val notificationOptions: NotificationOptions = NotificationOptions.builder(this)
            .contentTitle(getString(R.string.app_name))
            .contentText(getString(R.string.msg_map_downloading))
            .smallIconRes(com.mapbox.mapboxsdk.plugins.annotation.R.drawable.mapbox_logo_icon)
            .requestMapSnapshot(false)
            .returnActivity(javaClass.name)
            .build()

        OfflinePlugin.getInstance(this)
            .addOfflineDownloadStateChangeListener(object : OfflineDownloadChangeListener {
                override fun onCreate(offlineDownload: OfflineDownloadOptions?) {
                    Log.d(TAG, "Map downloading: download started")
                    isDownloadingMap = true
                }

                override fun onSuccess(offlineDownload: OfflineDownloadOptions?) {
                    Log.d(TAG, "Map downloading: download Success")

                    viewModel.isMapDownloaded = true
                    isDownloadingMap = false

                    NotificationManagerCompat.from(applicationContext)
                        .deleteNotificationChannel(OfflineConstants.NOTIFICATION_CHANNEL)
                    NotificationManagerCompat.from(applicationContext).cancelAll()
                }

                override fun onCancel(offlineDownload: OfflineDownloadOptions?) {
                    Log.d(TAG, "Map downloading: download Canceled")
                    isDownloadingMap = false

                    NotificationManagerCompat.from(applicationContext)
                        .deleteNotificationChannel(OfflineConstants.NOTIFICATION_CHANNEL)
                    NotificationManagerCompat.from(applicationContext).cancelAll()
                }

                override fun onError(
                    offlineDownload: OfflineDownloadOptions?,
                    error: String?,
                    message: String?
                ) {
                    Log.d(TAG, "Map downloading: download failed")
                    showMessage(R.string.msg_failed_downloading_the_map)
                    OfflinePlugin.getInstance(applicationContext).cancelDownload(offlineDownload)
                }

                override fun onProgress(offlineDownload: OfflineDownloadOptions?, progress: Int) {
                }
            })

        OfflinePlugin.getInstance(applicationContext).startDownload(
            OfflineDownloadOptions.builder()
                .definition(definition)
                .metadata(OfflineUtils.convertRegionName(region))
                .notificationOptions(notificationOptions)
                .build()
        );
    }

    private fun checkIntent(intent: Intent?) {
        val notification =
            intent?.getSerializableExtra(Constants.EXTRA_NOTIFICATION) as? Notification
        if (notification != null) displayNotificationDialog(notification, false)
    }

    private fun displayNotificationDialog(notification: Notification, fromSocket: Boolean) {
        val mediaPlayer =
            notification.sound?.let { MediaPlayer.create(this, it.resId) }

        when (notification.type) {
            Constants.NTF_ORDER_DISPATCH ->
                if (!fromSocket) Bus.publishEvent(RefreshOrdersEvent())
            Constants.NTF_ORDER_HIDDEN_DISPATCH ->
                if (!fromSocket) Bus.publishEvent(RefreshOrdersEvent(true, notification.orderId))
            Constants.NTF_ORDER_CANCEL,
            Constants.NTF_ORDER_HIDDEN_CANCEL,
            Constants.NTF_ORDER_RE_DISPATCH -> Bus.publishEvent(OrderCanceledEvent(notification.orderId))
            Constants.NTF_INCENTIVE -> navController.navigate(R.id.incentivesFragment)
        }

        if (notification.type != Constants.NTF_ORDER_HIDDEN_CANCEL
            && notification.type != Constants.NTF_ORDER_HIDDEN_DISPATCH
            && notification.type != Constants.NTF_INCENTIVE
        ) {
            mediaPlayer?.isLooping = true
            mediaPlayer?.start()

            showDialogMessage(
                title = "",
                message = notification.text,
                tintColorId = notification.color?.resId ?: R.color.colorPrimary,
                positiveListener = { _, _ -> mediaPlayer?.stop() },
                negativeListener = { _, _ -> mediaPlayer?.stop() }
            )
        }

        NotificationsUtil.dismissNotificationById(this, notification.id)
    }

    private fun showLocationDialog() {
        hideLocationDialog()
        locationDialog = buildDialog(
            "",
            getString(R.string.msg_gps_off),
            positiveListener = { _, _ -> goToLocationSettings() },
            cancelable = false
        )

        locationDialog?.show()
    }

    private fun goToLocationSettings() {
        activityResultObserver.launch(
            Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        ) {
            if (LocationUtil.isLocationEnabled(this)) {
                hideLocationDialog()
                viewModel.fetchGpsStatus()
            } else showLocationDialog()
        }
    }

    private fun hideLocationDialog() {
        locationDialog?.dismiss()
    }

    private fun showConnectionDialog() {
        hideConnectionDialog()
        connectionDialog = buildDialog(
            "",
            getString(R.string.msg_mobile_data_off),
            positiveListener = { _, _ -> goToMobileDataSettings() },
            cancelable = false
        )

        connectionDialog?.show()
    }

    private fun goToMobileDataSettings() {
        startActivity(NavigationUtil.mobileNetworksIntent())
    }

    private fun hideConnectionDialog() {
        connectionDialog?.dismiss()
    }

    private fun checkBatteryOptimization() {
        if (AppUtil.isIgnoringBatteryOptimizations(this)) {
            hideBatteryOptimizationDialog()
        } else {
            showBatteryOptimizationDialog()
        }
    }

    private fun showBatteryOptimizationDialog() {
        hideBatteryOptimizationDialog()

        batteryOptimizationDialog = buildDialog(
            "",
            getString(R.string.msg_battery_optimization),
            positiveListener = { _, _ -> goToBatteryOptimizationSettings() },
            cancelable = true
        )

        batteryOptimizationDialog?.show()
    }

    private fun goToBatteryOptimizationSettings() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            startActivity(NavigationUtil.batteryOptimizationIntent())
        }
    }

    private fun hideBatteryOptimizationDialog() {
        batteryOptimizationDialog?.hide()
    }

    private val navigationListener =
        NavController.OnDestinationChangedListener { _, destination, _ ->
            mActivityBinding.hideDriverRank = when (destination.id) {
                R.id.homeFragment,
                R.id.transactionsFragment,
                R.id.incomeBreakdownFragment,
                R.id.incentivesFragment -> false
                else -> true
            }
        }

    override fun onResume() {
        super.onResume()
        navController.addOnDestinationChangedListener(navigationListener)

        checkBatteryOptimization()

        if (!NetworkConnectivity.isConnected(this)) {
            showConnectionDialog()
        } else hideConnectionDialog()

        if (LocationUtil.isLocationEnabled(this)) {
            hideLocationDialog()
        } else showLocationDialog()
    }

    override fun onPause() {
        super.onPause()
        navController.removeOnDestinationChangedListener(navigationListener)
    }

    override fun onDestroy() {
        super.onDestroy()
        stopService(locationServiceIntent)
    }

    override val layoutId: Int
        get() = R.layout.activity_main

    override val viewModelId: Int
        get() = BR.viewModel

    override val viewModelClass: Class<MainViewModel>
        get() = MainViewModel::class.java


    companion object {
        private const val TAG = "MainActivity"
        private const val REQUEST_READ_PHONE_STATE = 122
    }
}