package com.bluebrand.beedriver.presentation.ui.base.view.alert

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import com.bluebrand.beedriver.R

interface IAlertDialog {
    fun buildDialog(
        title: String? = null, message: String,
        positiveText: String? = null, negativeText: String? = null,
        onPositiveClickListener: DialogInterface.OnClickListener? = null,
        onNegativeClickListener: DialogInterface.OnClickListener? = null,
        cancelable: Boolean = true,
        @ColorRes tintColorId: Int = R.color.colorPrimary
    ) : Dialog

    fun showDialogMessage(
        title: String? = null, message: String,
        positiveText: String? = null, negativeText: String? = null,
        onPositiveClickListener: DialogInterface.OnClickListener? = null,
        onNegativeClickListener: DialogInterface.OnClickListener? = null,
        cancelable: Boolean = true,
        @ColorRes tintColorId: Int = R.color.colorPrimary
    )

    fun showDialogMessage(
        @StringRes titleId: Int? = null, @StringRes messageId: Int,
        positiveText: String? = null, negativeText: String? = null,
        onPositiveClickListener: DialogInterface.OnClickListener? = null,
        onNegativeClickListener: DialogInterface.OnClickListener? = null,
        cancelable: Boolean = true,
        @ColorRes tintColorId: Int = R.color.colorPrimary
    )


    object Factory {
        fun defaultAlert(context: Context): IAlertDialog {
            return AlertDialog(context)
        }
    }
}