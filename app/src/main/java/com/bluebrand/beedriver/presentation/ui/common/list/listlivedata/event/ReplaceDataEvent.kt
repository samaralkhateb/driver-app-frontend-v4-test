package com.bluebrand.beedriver.presentation.ui.common.list.listlivedata.event

import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseListAdapter

/**
 * Clears the old data then add the new one.
 */
class ReplaceDataEvent<T>(
    allData: List<T>,
    private val newData: List<T>
) : ListEvent<T>(allData) {
    override fun applyEvent(adapter: BaseListAdapter<T, *>) {
        adapter.submitData(newData)
    }
}