package com.bluebrand.beedriver.presentation.ui.common.list.listlivedata.event

import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseListAdapter

abstract class ListEvent<T>(val allData: List<T>) {

    protected abstract fun applyEvent(adapter: BaseListAdapter<T, *>)

    fun attach(adapter: BaseListAdapter<T, *>) {
        if (adapter.isEmptyData) {
            adapter.submitData(allData)
        } else {
            applyEvent(adapter)
        }
    }

    fun allDataSize(): Int {
        return allData.size
    }
}