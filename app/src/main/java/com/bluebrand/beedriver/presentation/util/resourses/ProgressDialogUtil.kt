package com.bluebrand.beedriver.presentation.util.resourses

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import com.bluebrand.beedriver.R

object ProgressDialogUtil {
    private var dialog: Dialog? = null
    fun showProgressDialog(context: Context) {
        try {
            if (dialog == null) {
                dialog = Dialog(context)
                val progressBar = ProgressBar(context)
                progressBar.isIndeterminate = true
                dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog?.setContentView(progressBar)
                dialog?.window?.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(context, R.color.blackTransparent)))
                dialog?.setCancelable(false)
            }
            dialog?.show()
        } catch (e: Exception) {
        }
    }
    fun hideProgressDialog() {
        try {
            if (dialog != null) {
                if (dialog!!.isShowing) {
                    dialog?.dismiss()
                }
                dialog = null
            }
        } catch (e: Exception) {
        }
    }
}