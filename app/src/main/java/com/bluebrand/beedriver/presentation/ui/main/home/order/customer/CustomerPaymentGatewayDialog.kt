package com.bluebrand.beedriver.presentation.ui.main.home.order.customer

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelStoreOwner
import com.bluebrand.beedriver.BR
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.databinding.DialogCustomerPaymentGatewayBinding
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.MVVMFragmentDialog
import com.bluebrand.beedriver.presentation.ui.main.home.HomeViewModel
import com.ncorti.slidetoact.SlideToActView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CustomerPaymentGatewayDialog :
    MVVMFragmentDialog<HomeViewModel, DialogCustomerPaymentGatewayBinding>() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setFullScreen()
        registerListeners()
    }

    private fun registerListeners() {
        binding.buttonConfirm.onSlideCompleteListener =
            object : SlideToActView.OnSlideCompleteListener {
                override fun onSlideComplete(view: SlideToActView) {
                    viewModel.orderDelivered()
                    dismiss()
                }
            }
    }

    override val layoutId: Int
        get() = R.layout.dialog_customer_payment_gateway

    override val viewModelClass: Class<HomeViewModel>
        get() = HomeViewModel::class.java

    override val viewModelOwner: ViewModelStoreOwner
        get() = requireActivity()

    override val viewModelId: Int
        get() = BR.viewModel
}