package com.bluebrand.beedriver.presentation.ui.base.view.fragment

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.annotation.StringRes
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.presentation.ui.base.view.IBaseView
import com.bluebrand.beedriver.presentation.ui.base.view.activity.BaseActivity
import com.bluebrand.beedriver.presentation.util.navigation.ActivityResultObserver
import com.google.android.material.snackbar.Snackbar

abstract class BaseFragment<DB : ViewDataBinding> : Fragment(), IBaseView {
    protected lateinit var rootView: View
    protected lateinit var binding: DB

    protected var baseActivity: BaseActivity? = null

    protected val activityResultObserver: ActivityResultObserver<Intent, ActivityResult>?
        get() = baseActivity?.activityResultObserver


    protected abstract val layoutId: Int

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity) {
            baseActivity = context
        }
    }

    // Check if the data-binding is enable in the layout or inflate the layout normally
    protected fun createView(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) {
        try {
            binding = DataBindingUtil.inflate(inflater, layoutId, container, false)
            rootView = binding.root
        } catch (e: Exception) {
            rootView = inflater.inflate(layoutId, container, false)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        createView(inflater, container)
        setupDataBinding()
        setupToolbar(rootView)
        return rootView
    }

    private fun setupDataBinding() {
        binding.lifecycleOwner = viewLifecycleOwner
        binding.executePendingBindings()
    }

    private fun setupToolbar(view: View) {
        try {
            val toolbar: Toolbar = view.findViewById(R.id.toolbar)
            baseActivity?.setSupportActionBar(toolbar)
            changeToolbarTitle("")
        } catch (ignored: Exception) {
        }
        try {
            view.findViewById<View>(R.id.back_button).setOnClickListener { onBackPressed() }
        } catch (ignored: Exception) {
        }
    }

    fun onBackPressed() {
        requireActivity().onBackPressed()
    }

    // Base methods
    override fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun showMessage(stringId: Int) {
        showMessage(getString(stringId))
    }


    override fun showDialogMessage(title: Int, message: Int) {
        showDialogMessage(getString(title), getString(message))
    }

    override fun showDialogMessage(title: String?, message: String) {
        showDialogMessage(title, message, android.R.string.ok, android.R.string.cancel)
    }

    fun showDialogMessage(
        title: String?,
        message: String,
        @StringRes positiveText: Int = android.R.string.ok,
        @StringRes negativeText: Int = android.R.string.cancel,
        positiveListener: DialogInterface.OnClickListener? = null,
        negativeListener: DialogInterface.OnClickListener? = null,
        cancelable: Boolean = false
    ) {
        baseActivity?.showDialogMessage(
            title, message,
            positiveText, negativeText,
            positiveListener, negativeListener,
            cancelable
        )
    }

    open fun showSnackBar(
        message: String,
        actionText: Int = R.string.lbl_refresh,
        action: View.OnClickListener
    ) {
        showSnackBar(
            rootView,
            message,
            getString(actionText),
            action
        )
    }

    open fun showSnackBar(
        view: View,
        message: String,
        actionText: String,
        action: View.OnClickListener
    ) {
        Snackbar
            .make(view, message, Snackbar.LENGTH_INDEFINITE)
            .setActionTextColor(ContextCompat.getColor(requireContext(), R.color.white))
            .setAction(actionText, action)
            .show()
    }

    override fun hideKeyboard() {
        var currentFocus = activity?.currentFocus
        if (currentFocus == null) currentFocus = View(context)
        val inputMethodManager =
            context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        inputMethodManager?.hideSoftInputFromWindow(currentFocus.windowToken, 0)
    }

    fun changeToolbarTitle(title: String) {
        baseActivity?.changeToolbarTitle(title)
    }

    fun changeStatusBarColor(colorId: Int) {
        val window = activity?.window
        window?.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window?.statusBarColor = activity?.resources?.getColor(colorId) ?: R.color.colorPrimaryDark
    }

    fun setActivityFullScreen() {
        val w = requireActivity().window
        w.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
    }

    fun exitActivityFullScreen() {
        val w = requireActivity().window
        w.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
    }
}