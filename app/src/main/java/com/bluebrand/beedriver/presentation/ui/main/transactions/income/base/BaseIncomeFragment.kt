package com.bluebrand.beedriver.presentation.ui.main.transactions.income.base

import android.os.Bundle
import android.view.View
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.databinding.FragmentIncomeListBinding
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.MVVMFragment

abstract class BaseIncomeFragment<VM : BaseIncomeViewModel> :
    MVVMFragment<VM, FragmentIncomeListBinding>() {

    protected abstract fun provideRecyclerAdapter(): RecyclerView.Adapter<*>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupIncomeListAdapter()

        viewModel.fetchData()
    }

    private fun setupIncomeListAdapter() {
        binding.rvIncomeHistory.adapter = provideRecyclerAdapter()
    }

    override val layoutId: Int
        get() = R.layout.fragment_income_list

    override val viewModelId: Int
        get() = BR.viewModel
}