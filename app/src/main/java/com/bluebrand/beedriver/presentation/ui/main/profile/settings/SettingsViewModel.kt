package com.bluebrand.beedriver.presentation.ui.main.profile.settings

import com.bluebrand.beedriver.presentation.ui.base.view.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor() : BaseViewModel()