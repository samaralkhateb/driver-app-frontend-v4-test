package com.bluebrand.beedriver.presentation.ui.base.view.activity

import android.content.Context
import android.content.res.Resources
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.bluebrand.beedriver.presentation.util.locale.LocaleManager
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

abstract class LocalizationActivity : AppCompatActivity() {
    private var currentLanguage: String? = null

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleManager.getInstance(newBase).setLocale(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupLanguageChecker()
    }

    private fun setupLanguageChecker() {
        currentLanguage = LocaleManager.getInstance(this).savedLanguage

        lifecycleScope.launch {
            LocaleManager
                .getInstance(applicationContext)
                .resourcesFlow
                .collect { checkLanguageChanging() }
        }
    }

    private fun checkLanguageChanging() {
        val newLanguage = LocaleManager.getInstance(this).savedLanguage
        if (newLanguage != currentLanguage) {
            updateUI(LocaleManager.getInstance(this).setLocale(this).resources)
        }
        currentLanguage = newLanguage
    }

    protected open fun updateUI(newResources: Resources) {}
}