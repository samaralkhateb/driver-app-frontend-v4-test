package com.bluebrand.beedriver.presentation.ui.auth.login

import android.os.Build
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.repository.auth.IAuthRepository
import com.bluebrand.beedriver.presentation.ui.base.view.BaseViewModel
import com.bluebrand.beedriver.presentation.util.lifecycle.Event
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val authRepository: IAuthRepository
) : BaseViewModel() {
    val mobileLiveData = MutableLiveData("")
    val otpLiveData = MutableLiveData("")

    private val _loginStatus = MutableLiveData<Event<Boolean>>()
    val loginStatus: LiveData<Event<Boolean>>
        get() = _loginStatus

    private val _otpResend = MutableLiveData<Event<Boolean>>()
    val otpResend: LiveData<Event<Boolean>>
        get() = _otpResend

    private val _loginError = MutableLiveData<Event<Int>>()
    val loginError: LiveData<Event<Int>>
        get() = _loginError

    fun login() {
        hideKeyboard()
        val mobileString = mobileLiveData.value
        if (!mobileString.isNullOrBlank()) {
            startLoading()
            authRepository.login(mobileString)
                .catch(errorHandler)
                .onEach { result ->
                    stopLoading()
                    _loginStatus.value = Event(result)
                }
                .launchIn(viewModelScope)
        }
    }

    fun resendOtp() {
        if (isLoading()) return

        startLoading()

        authRepository.resendOtp(mobileLiveData.value!!)
            .catch(errorHandler)
            .onEach { result ->
                stopLoading()
                _otpResend.value = Event(result)
                if (result) showMessage(R.string.msg_resendOTP)
                else showMessage(R.string.unknown_error)
            }
            .launchIn(viewModelScope)
    }

    fun verifyOtp(androidVersion: String) {
        hideKeyboard()

        val mobileString = mobileLiveData.value
        val otpString = otpLiveData.value

        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL

        if (!otpString.isNullOrBlank()) {
            startLoading()

            FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
                val fcmToken =
                    if (task.isSuccessful) {
                        task.result
                    } else null

                Log.d(TAG, "Fcm Token: $fcmToken")

                authRepository.verifyOtp(mobileString!!, otpString, androidVersion, manufacturer, model, fcmToken)
                    .catch(errorHandler)
                    .onEach { result ->
                        stopLoading()
                        _loginStatus.value = Event(result)
                    }
                    .launchIn(viewModelScope)
            }
        }
    }

    companion object {
        private const val TAG = "LoginViewModel"
    }
}