package com.bluebrand.beedriver.presentation.di

import com.bluebrand.beedriver.data.repository.auth.AuthRepositoryImpl
import com.bluebrand.beedriver.data.repository.auth.IAuthRepository
import com.bluebrand.beedriver.data.repository.driver.DriverRepositoryImpl
import com.bluebrand.beedriver.data.repository.driver.IDriverRepository
import com.bluebrand.beedriver.data.repository.location.ILocationRepository
import com.bluebrand.beedriver.data.repository.location.LocationRepositoryImpl
import com.bluebrand.beedriver.data.repository.order.IOrdersRepository
import com.bluebrand.beedriver.data.repository.order.OrdersRepositoryImpl
import com.bluebrand.beedriver.data.repository.registration.IRegistrationRepository
import com.bluebrand.beedriver.data.repository.registration.RegistrationRepositoryImpl
import com.bluebrand.beedriver.data.repository.socket.ISocketRepository
import com.bluebrand.beedriver.data.repository.socket.SocketRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
    @Binds
    @Singleton
    abstract fun provideAuthRepository(repository: AuthRepositoryImpl): IAuthRepository

    @Binds
    @Singleton
    abstract fun provideRegistrationRepository(repository: RegistrationRepositoryImpl): IRegistrationRepository

    @Binds
    @Singleton
    abstract fun provideDriverRepository(repository: DriverRepositoryImpl): IDriverRepository

    @Binds
    @Singleton
    abstract fun provideLocationRepository(repository: LocationRepositoryImpl): ILocationRepository

    @Binds
    @Singleton
    abstract fun provideOrdersRepository(repository: OrdersRepositoryImpl): IOrdersRepository

    @Binds
    @Singleton
    abstract fun provideSocketRepository(repository: SocketRepositoryImpl): ISocketRepository
}