package com.bluebrand.beedriver.presentation.util.device

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.BatteryManager
import android.telephony.CellInfoCdma
import android.telephony.CellInfoGsm
import android.telephony.CellInfoLte
import android.telephony.TelephonyManager
import androidx.core.app.ActivityCompat

object DeviceUtil {
    fun getBatteryLevel(context: Context): Int {
        val batteryManager: BatteryManager =
            context.getSystemService(Context.BATTERY_SERVICE) as BatteryManager
        return batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
    }

    fun getSignalStrength(context: Context): Int? {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.READ_PHONE_STATE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val telephonyManager =
                context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            return telephonyManager.let {
                if (it.allCellInfo != null && it.allCellInfo.isNotEmpty()) {
                    val cellInfo = it.allCellInfo[0]
                    var cellSignalStrength = 0
                    if (cellInfo is CellInfoLte) {
                        cellSignalStrength = cellInfo.cellSignalStrength.level
                    } else if (cellInfo is CellInfoGsm) {
                        cellSignalStrength = cellInfo.cellSignalStrength.level
                    } else if (cellInfo is CellInfoCdma) {
                        cellSignalStrength = cellInfo.cellSignalStrength.level
                    }
                    return cellSignalStrength
                }
                null
            }
        }
        return null
    }
}