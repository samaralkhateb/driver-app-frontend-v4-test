package com.bluebrand.beedriver.presentation.ui.main.home.order.restaurant

import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelStoreOwner
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.bluebrand.beedriver.BR
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.data.model.OrderDetails
import com.bluebrand.beedriver.databinding.FragmentRouteToRestaurantBinding
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.BaseMapBoxFragment
import com.bluebrand.beedriver.presentation.ui.main.home.HomeViewModel
import com.bluebrand.beedriver.presentation.util.map.toLatLngList
import com.bluebrand.beedriver.presentation.util.resourses.BitmapUtil
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.Symbol
import com.ncorti.slidetoact.SlideToActView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RestaurantInfoFragment :
    BaseMapBoxFragment<HomeViewModel, FragmentRouteToRestaurantBinding>() {

    private lateinit var navController: NavController

    private var driverMarker: Symbol? = null
    private var restaurantMarker: Symbol? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        viewModel.fetchRestaurantRoad()

        registerListeners()
        registerObservers()
    }

    private fun registerListeners() {
        binding.buttonMyLocation.setOnClickListener {
            adjustZoom()
            viewModel.fetchRestaurantRoad()
        }

        binding.buttonArrival.onSlideCompleteListener =
            object : SlideToActView.OnSlideCompleteListener {
                override fun onSlideComplete(view: SlideToActView) {
                    viewModel.arrivedToRestaurant()
                    binding.buttonArrival.resetSlider()
                }
            }
    }

    private fun registerObservers() {
        viewModel.currentActiveOrder.observe(viewLifecycleOwner, this::addRestaurantMarker)
        viewModel.driverLocation.observe(viewLifecycleOwner, this::updateDriverMarker)
        viewModel.restaurantRoad.observe(viewLifecycleOwner) {
            drawRoute(it.toLatLngList())
        }
    }


    override fun onStyleReady(style: Style) {
        super.onStyleReady(style)
        viewModel.currentActiveOrder.value?.let { addRestaurantMarker(it) }
        viewModel.driverLocation.value?.let { updateDriverMarker(it, true) }
        viewModel.restaurantRoad.value?.let { drawRoute(it.toLatLngList()) }
    }

    override fun registerIcons(style: Style) {
        super.registerIcons(style)
        BitmapUtil.bitmapFromDrawableRes(
            requireContext(),
            R.drawable.ic_restaurant_marker
        )?.let {
            style.addImage(MARKER_RESTAURANT, it)
        }
    }

    private fun addRestaurantMarker(orderDetails: OrderDetails?) {
        if (orderDetails == null) return

        restaurantMarker?.let { removeMarker(it) }
        restaurantMarker = addMarker(
            orderDetails.restaurantLatitude,
            orderDetails.restaurantLongitude,
            MARKER_RESTAURANT
        )

        adjustZoom()
    }

    private fun updateDriverMarker(location: Location, adjustZoom: Boolean = false) {
        driverMarker?.let { removeMarker(it) }
        driverMarker = addMarker(location.latitude, location.longitude)

        if (adjustZoom) {
            adjustZoom()
        } else {
            moveCamera(
                LatLng(
                    location.latitude,
                    location.longitude,
                ),
                map?.cameraPosition?.zoom?.toFloat() ?: DEFAULT_ZOOM
            )
        }
    }

    private fun adjustZoom() {
        val currentOrder = viewModel.currentActiveOrder.value
        val driverLocation = viewModel.driverLocation.value

        if (currentOrder != null && driverLocation != null) {
            moveCamera(
                LatLng(
                    currentOrder.restaurantLatitude,
                    currentOrder.restaurantLongitude,
                ),
                LatLng(
                    driverLocation.latitude,
                    driverLocation.longitude,
                ),
            )
        }
    }

    override val mapView: MapView
        get() = binding.mapView

    override val layoutId: Int
        get() = R.layout.fragment_route_to_restaurant

    override val viewModelClass: Class<HomeViewModel>
        get() = HomeViewModel::class.java

    override val viewModelOwner: ViewModelStoreOwner
        get() = requireActivity()

    override val viewModelId: Int
        get() = BR.viewModel


    companion object {
        const val MARKER_RESTAURANT = "marker_restaurant"
    }
}