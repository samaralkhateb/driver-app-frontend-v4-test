package com.bluebrand.beedriver.presentation.di

import com.bluebrand.beedriver.data.source.remote.service.auth.AuthApiServiceImpl
import com.bluebrand.beedriver.data.source.remote.service.auth.IAuthApiService
import com.bluebrand.beedriver.data.source.remote.service.driver.DriverApiServiceImpl
import com.bluebrand.beedriver.data.source.remote.service.driver.IDriverApiService
import com.bluebrand.beedriver.data.source.remote.service.order.IOrdersApiService
import com.bluebrand.beedriver.data.source.remote.service.order.OrdersApiServiceImpl
import com.bluebrand.beedriver.data.source.remote.service.registration.IRegistrationApiService
import com.bluebrand.beedriver.data.source.remote.service.registration.RegistrationApiServiceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class ApiServicesModule {
    @Singleton
    @Binds
    abstract fun provideAuthApiService(service: AuthApiServiceImpl): IAuthApiService

    @Singleton
    @Binds
    abstract fun provideRegistrationApiService(service: RegistrationApiServiceImpl): IRegistrationApiService

    @Singleton
    @Binds
    abstract fun provideDriverApiService(service: DriverApiServiceImpl): IDriverApiService

    @Singleton
    @Binds
    abstract fun provideOrdersApiService(service: OrdersApiServiceImpl): IOrdersApiService
}