package com.bluebrand.beedriver.presentation.ui.main.transactions.income

import com.bluebrand.beedriver.presentation.ui.base.view.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class IncomeBreakdownViewModel @Inject constructor() : BaseViewModel() {
}