package com.bluebrand.beedriver.presentation.ui.auth.verifyotp

import com.bluebrand.beedriver.presentation.ui.base.view.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class VerifyCodeViewModel @Inject constructor() : BaseViewModel() {
}