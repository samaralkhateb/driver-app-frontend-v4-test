package com.bluebrand.beedriver.presentation.ui.base.view

import androidx.annotation.StringRes

interface IBaseView {
    fun showMessage(message: String)
    fun showMessage(@StringRes stringId: Int)
    fun showDialogMessage(title: String?, message: String)
    fun showDialogMessage(@StringRes title: Int, @StringRes message: Int)
    fun hideKeyboard()
}