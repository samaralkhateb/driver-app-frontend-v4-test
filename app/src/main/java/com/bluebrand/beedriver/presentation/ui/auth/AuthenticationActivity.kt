package com.bluebrand.beedriver.presentation.ui.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.navigation.Navigation
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.databinding.ActivityAuthenticationBinding
import com.bluebrand.beedriver.presentation.ui.base.view.activity.BindingActivity
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class AuthenticationActivity : BindingActivity<ActivityAuthenticationBinding>() {
    companion object {
        const val KEY_SIGNUP_ENABLED = "key_signup_enabled"

        fun getIntent(context: Context, signupEnabled: Boolean): Intent {
            return Intent(context, AuthenticationActivity::class.java)
                .apply {
                    putExtra(KEY_SIGNUP_ENABLED, signupEnabled)
                }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setNavArgs()
    }

    private fun setNavArgs() {
        val args = Bundle().apply {
            putBoolean(
                "is_signup_enabled",
                intent.getBooleanExtra(KEY_SIGNUP_ENABLED, false)
            )
        }

        val navController = Navigation.findNavController(this, R.id.nav_host_auth)
        navController.setGraph(R.navigation.nav_auth, args)
    }

    override val layoutId: Int
        get() = R.layout.activity_authentication
}