package com.bluebrand.beedriver.presentation.ui.main.profile.arcr

import android.os.Bundle
import android.view.View
import com.bluebrand.beedriver.BR
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.databinding.FragmentArCrHistoryBinding
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.MVVMFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ArCrHistoryFragment : MVVMFragment<ArCrHistoryViewModel, FragmentArCrHistoryBinding>() {

    private lateinit var adapter: ArCrHistoryListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupArCrHistoryTable()
        setupClickListeners()
        registerObservers()
    }

    private fun setupArCrHistoryTable() {
        adapter = ArCrHistoryListAdapter(requireContext())
        binding.layoutTable.rvArCrHistory.adapter = adapter
    }

    private fun setupClickListeners() {
        binding.buttonMaximize.setOnClickListener {
            openInLandscapeMode()
        }
    }

    private fun openInLandscapeMode() {
        startActivity(
            ArCrHistoryActivity
                .getIntent(requireContext(), viewModel.arCrHistory.value!!)
        )
    }

    private fun registerObservers() {
        viewModel.arCrHistory
            .observe(viewLifecycleOwner) { arCrHistory ->
                adapter.submitData(arCrHistory?.arCrInfoList ?: emptyList())
            }
    }

    override val layoutId: Int
        get() = R.layout.fragment_ar_cr_history

    override val viewModelClass: Class<ArCrHistoryViewModel>
        get() = ArCrHistoryViewModel::class.java

    override val viewModelId: Int
        get() = BR.viewModel
}