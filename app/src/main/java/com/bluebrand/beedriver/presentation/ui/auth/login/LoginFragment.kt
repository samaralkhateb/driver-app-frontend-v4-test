package com.bluebrand.beedriver.presentation.ui.auth.login

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.ViewModelStoreOwner
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.bluebrand.beedriver.BR
import com.bluebrand.beedriver.R
import com.bluebrand.beedriver.databinding.FragmentLoginBinding
import com.bluebrand.beedriver.presentation.ui.base.view.fragment.MVVMFragment
import com.bluebrand.beedriver.presentation.util.lifecycle.EventObserver
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_login.*


@AndroidEntryPoint
class LoginFragment : MVVMFragment<LoginViewModel, FragmentLoginBinding>() {
    private lateinit var navController: NavController

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        checkArgs()

        setupClickListeners()
        setupObservers()
    }

    private fun checkArgs() {
        arguments?.let {
            val isSignupEnabled =
                LoginFragmentArgs
                    .fromBundle(it)
                    .isSignupEnabled

            binding.layoutSignup.visibility = if (isSignupEnabled) View.VISIBLE else View.INVISIBLE
        }
    }

    private fun setupClickListeners() {
        binding.buttonLogin.setOnClickListener { viewModel.login() }
        binding.buttonSignup.setOnClickListener { navigateToSignupFragment() }
    }

    private fun navigateToSignupFragment() {
        navController.navigate(
            LoginFragmentDirections.actionLoginFragmentToSignupFragment()
        )
    }

    private fun setupObservers() {
        viewModel.loginStatus.observe(
            viewLifecycleOwner,
            EventObserver { status ->
                if (status) {
                    navigateToVerifyCodeFragment()
                }
            }
        )

        viewModel.loginError.observe(
            viewLifecycleOwner,
            EventObserver {
                showDialogMessage(R.string.app_name, it)
            }
        )
    }

    private fun navigateToVerifyCodeFragment() {
        navController.navigate(
            LoginFragmentDirections.actionLoginFragmentToVerifyCodeFragment()
        )
    }

    override val layoutId: Int
        get() = R.layout.fragment_login

    override val viewModelOwner: ViewModelStoreOwner
        get() = requireActivity()

    override val viewModelClass: Class<LoginViewModel>
        get() = LoginViewModel::class.java

    override val viewModelId: Int
        get() = BR.viewModel
}

