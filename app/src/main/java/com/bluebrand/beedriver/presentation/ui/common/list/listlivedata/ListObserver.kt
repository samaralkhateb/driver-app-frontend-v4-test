package com.bluebrand.beedriver.presentation.ui.common.list.listlivedata

import androidx.lifecycle.Observer
import com.bluebrand.beedriver.presentation.ui.base.adapter.BaseListAdapter
import com.bluebrand.beedriver.presentation.ui.common.list.listlivedata.event.ListEvent

class ListObserver<T>(
    private val adapter: BaseListAdapter<T, *>
) : Observer<ListEvent<T>> {
    override fun onChanged(event: ListEvent<T>) {
        event.attach(adapter)
    }
}