package com.bluebrand.beedriver.presentation.ui.main.profile.arcr

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bluebrand.beedriver.data.model.ArCrHistory
import com.bluebrand.beedriver.data.repository.driver.IDriverRepository
import com.bluebrand.beedriver.presentation.ui.base.view.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class ArCrHistoryViewModel @Inject constructor(
    private val driverRepository: IDriverRepository
) : BaseViewModel() {
    private val _arCrHistory = MutableLiveData<ArCrHistory>()
    val arCrHistory: LiveData<ArCrHistory>
    get() = _arCrHistory

    init {
        fetchArCrHistory()
    }

    override fun refresh() {
        fetchArCrHistory()
    }

    fun fetchArCrHistory() {
        if (isLoading()) return
        startLoading()

        driverRepository.getArCrHistory()
            .catch { error ->
                error.printStackTrace()
                stopLoading()
                showMessage(error, true)
            }
            .onEach {
                stopLoading()
                _arCrHistory.value = it
            }
            .launchIn(viewModelScope)
    }
}