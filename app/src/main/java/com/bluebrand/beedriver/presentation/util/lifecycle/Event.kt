package com.bluebrand.beedriver.presentation.util.lifecycle

/**
 * Data Wrapper that can be handled only once
 *
 * @param <T> data type
</T> */
class Event<T>(private val data: T) {
    private var isHandled = false

    fun getDataOrNull(): T? {
        return if (isHandled) null
        else {
            isHandled = true
            data
        }
    }
}