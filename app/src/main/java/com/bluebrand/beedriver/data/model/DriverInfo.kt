package com.bluebrand.beedriver.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DriverInfo(
    @Json(name = "id") val id: Long,
    @Json(name = "name") val firstName: String,
    @Json(name = "middle_name") val middleName: String?,
    @Json(name = "last_name") val lastName: String,
    @Json(name = "driver_city") val cityId: Int,
    @Json(name = "driver_rate") val rate: Double,
    @Json(name = "flag") val flag: Int,
    @Json(name = "flag_string") val rank: String,
    @Json(name = "is_online") val isOnline: Boolean,
    @Json(name = "auto_accept") val autoAcceptEnabled: Boolean,
    @Json(name = "last_order") val lastOrderEnabled: Boolean,
    @Json(name = "maxToShowFlag") val maxToShow: Boolean?,
    @Json(name = "acceptance_rate") val acceptanceRate: Int,
    @Json(name = "cancellation_rate") val cancellationRate: Int,
) {
    val driverName: String
        get() =
            if (middleName != null) firstName.plus(" ").plus(middleName).plus(" ").plus(lastName)
        else firstName.plus(" ").plus(lastName)

    val isVip: Boolean
        get() = flag == 4

    override fun equals(other: Any?): Boolean {
        return false
    }
}