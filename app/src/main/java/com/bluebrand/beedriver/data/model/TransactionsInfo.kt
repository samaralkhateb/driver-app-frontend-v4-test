package com.bluebrand.beedriver.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class TransactionsInfo(
    @Json(name = "transaction") val transactionList: List<Transaction>,
    @Json(name = "balance") val balance: Double,
    @Json(name = "pending_funds") val pendingFunds: Double
): Serializable