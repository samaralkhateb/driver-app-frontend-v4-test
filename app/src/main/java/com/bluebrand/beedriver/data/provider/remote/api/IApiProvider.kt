package com.bluebrand.beedriver.data.provider.remote.api

import kotlinx.coroutines.flow.Flow
import retrofit2.Response

interface IApiProvider {
    fun <T> proceedRequest(call: suspend () -> Response<T>): Flow<T>
}