package com.bluebrand.beedriver.data.source.remote.model.driver.incentive

import com.bluebrand.beedriver.data.model.Incentive
import com.bluebrand.beedriver.data.source.remote.model.base.BaseResponse
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

class GetIncentivesResponse : BaseResponse<IncentivesData>()

@JsonClass(generateAdapter = true)
data class IncentivesData(
    @Json(name = "incentive") val incentiveList: List<Incentive>
)