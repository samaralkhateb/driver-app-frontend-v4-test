package com.bluebrand.beedriver.data.source.remote.model.driver.profile

import com.bluebrand.beedriver.data.model.DriverInfo
import com.bluebrand.beedriver.data.model.DriverRankDetails
import com.bluebrand.beedriver.data.source.remote.model.base.BaseResponse
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

class GetProfileResponse : BaseResponse<ProfileData>()

@JsonClass(generateAdapter = true)
data class ProfileData(
    @Json(name = "rankDetails") val rankDetails: DriverRankDetails,
    @Json(name = "driver") val driverInfo: DriverInfo
)