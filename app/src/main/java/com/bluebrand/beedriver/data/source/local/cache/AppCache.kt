package com.bluebrand.beedriver.data.source.local.cache

import com.bluebrand.beedriver.data.model.DriverInfo
import kotlinx.coroutines.flow.Flow

interface AppCache {
    fun saveDriverPhoneNumber(phoneNumber: String?)
    fun getDriverPhoneNumber(): String?

    fun saveAccessToken(token: String?)
    fun getSavedToken(): String?

    fun saveSupportPhoneNumber(phoneNumber: String?)
    fun getSupportPhoneNumber(): String?

    fun saveDriverInfo(driverInfo: DriverInfo?)
    fun getDriverInfo(): Flow<DriverInfo?>

    fun isMapDownloaded(): Boolean
    fun setMapDownloaded(isDownloaded: Boolean)

    fun logout()
}