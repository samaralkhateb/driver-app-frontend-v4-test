package com.bluebrand.beedriver.data.source.remote.model.registration

import com.bluebrand.beedriver.data.model.Vehicle
import com.bluebrand.beedriver.data.source.remote.model.base.BaseResponse

class GetVehicleTypesResponse : BaseResponse<List<Vehicle>>()