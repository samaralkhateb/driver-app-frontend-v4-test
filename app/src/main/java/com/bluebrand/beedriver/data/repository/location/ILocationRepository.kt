package com.bluebrand.beedriver.data.repository.location

import android.location.Location
import kotlinx.coroutines.flow.Flow
import org.osmdroid.bonuspack.routing.Road
import org.osmdroid.util.GeoPoint

interface ILocationRepository {

    /**
     * @param interval milliseconds's interval
     * @return Location updates
     */
    fun getLocationUpdates(interval: Long): Flow<Location>

    fun getLocation(): Flow<Location>

    fun onGpsStatusChanged(): Flow<Boolean>

    fun getRoute(vararg wayPoints: GeoPoint): Flow<Road>
}