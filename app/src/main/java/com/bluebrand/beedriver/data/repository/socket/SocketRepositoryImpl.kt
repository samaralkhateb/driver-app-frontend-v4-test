package com.bluebrand.beedriver.data.repository.socket

import android.location.Location
import android.util.Log
import com.bluebrand.beedriver.data.model.DriverInfo
import com.bluebrand.beedriver.data.model.mapper.NotificationMapper
import com.bluebrand.beedriver.data.repository.Repository
import com.bluebrand.beedriver.data.source.local.cache.AppCache
import com.bluebrand.beedriver.data.source.remote.model.order.DriverOrdersData
import com.bluebrand.beedriver.data.util.constants.RemoteConstants
import com.bluebrand.beedriver.data.util.unMap
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.first
import org.json.JSONException
import org.json.JSONObject
import java.lang.Exception
import javax.inject.Inject

class SocketRepositoryImpl @Inject constructor(
    private val socket: Socket,
    private val appCash: AppCache,
    private val notificationMapper: NotificationMapper
) : ISocketRepository, Repository() {

    override fun connect() {
        socket.connect()
    }

    override fun disconnect() {
        socket.disconnect()
    }

    override fun getConnectionStatus() = callbackFlow {
        socket.on(Socket.EVENT_CONNECT) {
            trySend(true)
            Log.d(TAG, "Socket Connected")
        }

        socket.on(Socket.EVENT_DISCONNECT) {
            trySend(false)
            Log.d(TAG, "Socket Disconnected")
        }

        socket.on(Socket.EVENT_CONNECT_ERROR) {
            trySend(false)
            Log.d(TAG, "Socket Connection Error: ${it.getOrNull(0)}")
        }

        awaitClose {
            socket.off(Socket.EVENT_CONNECT)
            socket.off(Socket.EVENT_DISCONNECT)
            socket.off(Socket.EVENT_CONNECT_ERROR)
        }
    }

    override suspend fun updateDriverLocation(
        location: Location,
        batteryPercentage: Int,
        signalStrength: Int?
    ) {
        socket.connect()

        val mJsonObjectType = JSONObject()

        try {
            mJsonObjectType.put("driver_id", appCash.getDriverInfo().first()?.id)
            mJsonObjectType.put("latitude", location.latitude)
            mJsonObjectType.put("longitude", location.longitude)
            mJsonObjectType.put("battery_percentage", batteryPercentage)
            mJsonObjectType.put("signal_strength", signalStrength)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        socket.emit(RemoteConstants.Socket.EVENT_UPDATE_DRIVER_LOCATION, mJsonObjectType)

        Log.d(TAG, "${RemoteConstants.Socket.EVENT_UPDATE_DRIVER_LOCATION} : $mJsonObjectType")
    }

    override suspend fun orderReceived(orderId: Long, restaurantQueue: String) {
        val mJsonObjectType = JSONObject()

        try {
            mJsonObjectType.put("driver_id", appCash.getDriverInfo().first()?.id)
            mJsonObjectType.put("order_id", orderId)
            mJsonObjectType.put("restaurant_queue", restaurantQueue)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        socket.emit(RemoteConstants.Socket.EVENT_ORDER_RECEIVED, mJsonObjectType)
        Log.d(TAG, "${RemoteConstants.Socket.EVENT_ORDER_RECEIVED} : $mJsonObjectType")
    }

    override fun socketNotificationReceived() = callbackFlow {
        val listener = Emitter.Listener {
            try {
                val notification = notificationMapper.map(
                    JSONObject(it[0].toString())
                        .getJSONObject("payload")
                        .getJSONObject("data").toString()
                )
                if (notification != null) trySend(notification)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            Log.d(TAG, "${RemoteConstants.Socket.EVENT_SOCKET_NOTIFICATION}: ${it.getOrNull(0)}")
        }

        socket.on(RemoteConstants.Socket.EVENT_SOCKET_NOTIFICATION, listener)

        awaitClose {
            socket.off(RemoteConstants.Socket.EVENT_SOCKET_NOTIFICATION, listener)
        }
    }

    override fun driverInfoUpdated() = callbackFlow {
        val listener = Emitter.Listener {
            try {
                val driverInfo = it[0].toString().unMap(DriverInfo::class.java)
                if (driverInfo != null) trySend(driverInfo)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            Log.d(
                TAG,
                "${RemoteConstants.Socket.EVENT_UPDATE_DRIVER_INFORMATION}: ${it.getOrNull(0)}"
            )
        }

        socket.on(RemoteConstants.Socket.EVENT_UPDATE_DRIVER_INFORMATION, listener)

        awaitClose {
            socket.off(RemoteConstants.Socket.EVENT_UPDATE_DRIVER_INFORMATION, listener)
        }
    }

    override fun newOrderReceived() = callbackFlow{
        val listener = Emitter.Listener {
            try {
                val driverInfo = JSONObject(it[0].toString())
                    .getJSONObject("orders").toString()
                    .unMap(DriverOrdersData::class.java)
                if (driverInfo != null) trySend(driverInfo)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            Log.d(
                TAG,
                "${RemoteConstants.Socket.EVENT_NEW_ORDER_DISPATCHED}: ${it.getOrNull(0)}"
            )
        }

        socket.on(RemoteConstants.Socket.EVENT_NEW_ORDER_DISPATCHED, listener)

        awaitClose {
            socket.off(RemoteConstants.Socket.EVENT_NEW_ORDER_DISPATCHED, listener)
        }
    }

    companion object {
        private const val TAG = "SocketRepository"
    }
}