package com.bluebrand.beedriver.data.source.remote.service.order

import com.bluebrand.beedriver.data.source.remote.model.order.*
import kotlinx.coroutines.flow.Flow

interface IOrdersApiService {
    fun getDriverOrders(isSocketReconnection: Boolean): Flow<GetDriverOrdersResponse>

    fun acceptOrder(acceptOrderRequest: AcceptOrderRequest): Flow<UpdateOrderResponse>

    fun declineOrder(declineOrderRequest: DeclineOrderRequest): Flow<UpdateOrderResponse>

    fun driverArrivedToRestaurant(arrivedToRestaurantRequest: ArrivedToRestaurantRequest): Flow<UpdateOrderResponse>

    fun pickupOrder(pickupOrderRequest: PickupOrderRequest): Flow<UpdateOrderResponse>

    fun orderPaid(orderPaidRequest: OrderPaidRequest): Flow<UpdateOrderResponse>
}