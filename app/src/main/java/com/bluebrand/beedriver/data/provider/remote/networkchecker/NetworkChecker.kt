package com.bluebrand.beedriver.data.provider.remote.networkchecker

interface NetworkChecker {
    suspend fun isConnected(): Boolean
}