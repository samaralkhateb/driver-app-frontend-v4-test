package com.bluebrand.beedriver.data.model.mapper.base

interface IMappable<TO, FROM> {
    fun map(model: FROM): TO
}