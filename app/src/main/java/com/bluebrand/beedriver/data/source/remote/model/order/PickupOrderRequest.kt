package com.bluebrand.beedriver.data.source.remote.model.order

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PickupOrderRequest(
    @Json(name = "bill_id") val billId: Long,
    @Json(name = "bill_driver_id") val billDriverId: Long,
    @Json(name = "amount") val amount: Double,
)
