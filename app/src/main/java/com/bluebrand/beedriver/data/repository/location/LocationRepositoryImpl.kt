package com.bluebrand.beedriver.data.repository.location

import android.annotation.SuppressLint
import android.location.*
import android.location.LocationListener
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.annotation.RequiresApi
import com.bluebrand.beedriver.data.exception.RouteFetchException
import com.google.android.gms.location.*
import com.google.android.gms.location.LocationRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import org.osmdroid.bonuspack.routing.OSRMRoadManager
import org.osmdroid.bonuspack.routing.Road
import org.osmdroid.util.GeoPoint
import javax.inject.Inject
import javax.inject.Singleton

@SuppressLint("MissingPermission")
@Singleton
class LocationRepositoryImpl @Inject constructor(
    private val fusedLocation: FusedLocationProviderClient,
    private val locationManager: LocationManager,
    private val roadManager: OSRMRoadManager
) : ILocationRepository {

    override fun getLocationUpdates(interval: Long) = callbackFlow {
        val locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                if (locationResult.locations.isEmpty()) return

                val result = locationResult.locations[0]
                trySend(result)
            }
        }

        val locationListener = object : LocationListener {
            override fun onLocationChanged(location: Location) {
                trySend(location)
            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
            }

            override fun onProviderEnabled(provider: String) {

            }

            override fun onProviderDisabled(provider: String) {
            }
        }

        val request = LocationRequest.create().apply {
            this.interval = interval
            this.priority = Priority.PRIORITY_HIGH_ACCURACY
            this.smallestDisplacement = 0f
        }

        fusedLocation.requestLocationUpdates(
            request,
            locationCallback,
            Looper.getMainLooper()
        ).addOnFailureListener { e ->
            close(e)
        }

        val isNetworkProviderExist =
            locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        val isGPSProviderExist =
            locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (isNetworkProviderExist) {
            locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                interval,
                0f,
                locationListener
            )
        }

        if (isGPSProviderExist) {
            locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                interval,
                0f,
                locationListener
            )
        }

        awaitClose {
            fusedLocation.removeLocationUpdates(locationCallback)
            locationManager.removeUpdates(locationListener)
        }
    }

    override fun getLocation(): Flow<Location> {
        return getLocationUpdates(0).take(1)
    }

    override fun onGpsStatusChanged(): Flow<Boolean> {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            onGpsStatusChangedApi24()
        } else {
            onGpsStatusChangedLegacy()
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun onGpsStatusChangedApi24() = callbackFlow {
        val gnssStatusCallback =
            object : GnssStatus.Callback() {
                override fun onStarted() {
                    trySend(true)
                }

                override fun onStopped() {
                    trySend(false)
                }
            }

        locationManager.registerGnssStatusCallback(
            gnssStatusCallback,
            Handler(Looper.getMainLooper())
        )

        awaitClose {
            locationManager.unregisterGnssStatusCallback(gnssStatusCallback)
        }
    }

    @Suppress("DEPRECATION")
    private fun onGpsStatusChangedLegacy() = callbackFlow {
        val gpsStatusListener = GpsStatus.Listener { status ->
            if (status == GpsStatus.GPS_EVENT_STARTED) trySend(true)
            else if (status == GpsStatus.GPS_EVENT_STOPPED) trySend(false)
        }

        locationManager.addGpsStatusListener(gpsStatusListener)

        awaitClose {
            locationManager.removeGpsStatusListener(gpsStatusListener)
        }
    }


    override fun getRoute(vararg wayPoints: GeoPoint) = flow {
        val road = roadManager.getRoad(ArrayList(wayPoints.toList()))
        if (road == null
            || road.mStatus == Road.STATUS_INVALID
            || road.mStatus == Road.STATUS_TECHNICAL_ISSUE
        ) {
            throw RouteFetchException()
        } else {
            emit(road)
        }
    }.flowOn(Dispatchers.IO)
}