package com.bluebrand.beedriver.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RankInfo(
    @Json(name = "Name") val name: String,
    @Json(name = "description") val description: String
)