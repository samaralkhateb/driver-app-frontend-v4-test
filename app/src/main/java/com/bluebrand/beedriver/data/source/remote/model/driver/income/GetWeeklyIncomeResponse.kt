package com.bluebrand.beedriver.data.source.remote.model.driver.income

import com.bluebrand.beedriver.data.model.WeeklyIncomeInfo

class GetWeeklyIncomeResponse : GetIncomeBaseResponse<WeeklyIncomeInfo>()