package com.bluebrand.beedriver.data.repository.driver

import com.bluebrand.beedriver.data.model.*
import com.bluebrand.beedriver.data.source.remote.model.driver.profile.ProfileData
import com.bluebrand.beedriver.data.source.remote.model.driver.status.SetStatusResponse
import kotlinx.coroutines.flow.Flow

interface IDriverRepository {
    fun isMapDownloaded(): Boolean

    fun setMapDownloaded(isDownloaded: Boolean)

    fun getSupportPhoneNumber(): Flow<String?>

    fun setStatus(status: Boolean): Flow<SetStatusResponse>

    fun setLastOrder(status: Boolean): Flow<SetStatusResponse>

    fun setAutoAccept(status: Boolean): Flow<SetStatusResponse>

    fun getTransactions(): Flow<TransactionsInfo>

    fun getArCrHistory(): Flow<ArCrHistory>

    fun getActiveIncentives(): Flow<List<Incentive>>

    fun getIncentiveHistory(): Flow<List<Incentive>>

    fun getDailyIncomeHistory(): Flow<List<DailyIncomeInfo>>

    fun getWeeklyIncomeHistory(): Flow<List<WeeklyIncomeInfo>>

    fun getMonthlyIncomeHistory(): Flow<List<MonthlyIncomeInfo>>

    fun getRankGuide(): Flow<List<RankInfo>>

    fun getProfileData(): Flow<ProfileData>

    fun getDriverInfo(): Flow<DriverInfo?>

    fun saveDriverInfo(driverInfo: DriverInfo?)
}