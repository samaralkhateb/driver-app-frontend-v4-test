package com.bluebrand.beedriver.data.model.mapper

import android.os.SystemClock
import com.bluebrand.beedriver.data.model.Notification
import com.bluebrand.beedriver.data.model.mapper.base.Mapper
import org.json.JSONObject
import javax.inject.Inject

class NotificationMapper @Inject constructor() : Mapper<Notification?, String>() {

    override fun map(model: String): Notification? {
        val notificationId = SystemClock.uptimeMillis().toInt()

        val jsonObject = JSONObject(model)
        val type = jsonObject.getString("ntf_type")
        val text = jsonObject.getString("ntf_text")
        val body = jsonObject.getString("ntf_body")

        return try {
            val bodyJson = JSONObject(body ?: "")

            val orderId: Long? = bodyJson.optString("bill_id").toLongOrNull()

            var color: String? = null
            var soundLevel: String? = null

            val soundInfo =
                bodyJson.optString("sound_info").takeUnless { it.isBlank() || it == "null" }
            if (soundInfo != null) {
                val soundInfoJson = JSONObject(soundInfo)
                color =
                    soundInfoJson.optString("color").takeUnless { it.isBlank() || it == "null" }
                soundLevel =
                    soundInfoJson.optString("soundLevel")
                        .takeUnless { it.isBlank() || it == "null" }
            }
            Notification(
                notificationId,
                type,
                text,
                orderId,
                color?.let { Notification.Color.of(it) },
                soundLevel?.let {
                    Notification.Sound.of(it.toIntOrNull() ?: Notification.Sound.MEDIUM.level)
                }
            )
        } catch (e: Exception) {
            e.printStackTrace()
            Notification(
                notificationId,
                type,
                text,
                null,
                null,
                null
            )
        }
    }
}