package com.bluebrand.beedriver.data.provider.remote.api

interface ExceptionFactory {
    /**
     * @param code    response code
     * @param message Exception message
     * @return The picked Exception
     */
    fun fromCode(code: Int, message: String? = null): Exception

    fun fromCode(responseCode: String, statusCode:Int?, message: String? = null): Exception
}