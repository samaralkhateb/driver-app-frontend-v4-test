package com.bluebrand.beedriver.data.source.remote.service.auth

import com.bluebrand.beedriver.data.source.remote.model.auth.login.LoginRequest
import com.bluebrand.beedriver.data.source.remote.model.auth.login.LoginResponse
import com.bluebrand.beedriver.data.source.remote.model.auth.verify.VerifyOtpRequest
import com.bluebrand.beedriver.data.source.remote.model.auth.verify.VerifyOtpResponse
import kotlinx.coroutines.flow.Flow

interface IAuthApiService{
    fun login(loginRequest: LoginRequest): Flow<LoginResponse>

    fun verifyOtp(verifyRequest: VerifyOtpRequest): Flow<VerifyOtpResponse>

    fun resendOtp(request: LoginRequest): Flow<LoginResponse>
}