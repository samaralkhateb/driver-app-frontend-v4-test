package com.bluebrand.beedriver.data.source.remote.model.driver.rank

import com.bluebrand.beedriver.data.model.RankInfo
import com.bluebrand.beedriver.data.source.remote.model.base.BaseResponse

class GetRankGuideResponse : BaseResponse<List<RankInfo>>() {
}