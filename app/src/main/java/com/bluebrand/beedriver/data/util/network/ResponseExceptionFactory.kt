package com.bluebrand.beedriver.data.util.network

import com.bluebrand.beedriver.data.exception.*
import com.bluebrand.beedriver.data.provider.remote.api.ExceptionFactory
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ResponseExceptionFactory @Inject constructor() : ExceptionFactory {

    override fun fromCode(code: Int, message: String?): Exception {
        return when (code) {
            ResponseCode.NETWORK_ERROR -> NoInternetException(message)
            ResponseCode.SERVER_ERROR -> ServerException(message)
            else -> Exception(message)
        }
    }

    override fun fromCode(responseCode: String, statusCode: Int?, message: String?): Exception {
        return when (responseCode) {
            ResponseCode.ALREADY_ON_ERROR -> StatusSyncException(message)
            ResponseCode.ALREADY_OFF_ERROR -> StatusSyncException(message)
            ResponseCode.ORDER_REMOVED_ERROR -> OrderRemovedException(message)
            ResponseCode.FETCH_ORDERS_ERROR -> FetchOrdersException(message)
            else -> fromCode(statusCode ?: -1, message)
        }
    }
}