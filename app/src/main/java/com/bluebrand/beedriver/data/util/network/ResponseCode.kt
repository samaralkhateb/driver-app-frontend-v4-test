package com.bluebrand.beedriver.data.util.network

object ResponseCode {
    const val SUCCESS = 200
    const val SERVER_ERROR = 500
    const val NETWORK_ERROR = 0
    const val UNAUTHORIZED_USER = 401

    const val ALREADY_ON_ERROR = "E009"
    const val ALREADY_OFF_ERROR = "E010"
    const val ORDER_REMOVED_ERROR = "E011"
    const val FETCH_ORDERS_ERROR = "E012"
}