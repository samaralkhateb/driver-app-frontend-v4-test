package com.bluebrand.beedriver.data.source.remote.service.registration

import com.bluebrand.beedriver.data.source.remote.model.registration.GetAppInfoResponse
import com.bluebrand.beedriver.data.source.remote.model.registration.GetCitesResponse
import com.bluebrand.beedriver.data.source.remote.model.registration.GetVehicleTypesResponse
import com.bluebrand.beedriver.data.source.remote.model.registration.signup.SignupRequest
import com.bluebrand.beedriver.data.source.remote.model.registration.signup.SignupResponse
import kotlinx.coroutines.flow.Flow

interface IRegistrationApiService {
    fun getAppInfo(): Flow<GetAppInfoResponse>

    fun getCites(): Flow<GetCitesResponse>

    fun getVehicleTypes(): Flow<GetVehicleTypesResponse>

    fun signup(signupRequest: SignupRequest): Flow<SignupResponse>
}