package com.bluebrand.beedriver.data.source.remote.model.auth.verify

import com.bluebrand.beedriver.data.source.remote.model.base.BaseResponse
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


class VerifyOtpResponse : BaseResponse<VerifyOtpResponseData>()

@JsonClass(generateAdapter = true)
data class VerifyOtpResponseData(
    @Json(name = "access_token") val accessToken: String
)