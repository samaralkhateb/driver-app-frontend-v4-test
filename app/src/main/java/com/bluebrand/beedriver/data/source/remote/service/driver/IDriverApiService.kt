package com.bluebrand.beedriver.data.source.remote.service.driver

import com.bluebrand.beedriver.data.source.remote.model.driver.arcr.GetArCrResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.incentive.GetIncentivesResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.income.GetDailyIncomeResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.income.GetMonthlyIncomeResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.income.GetWeeklyIncomeResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.profile.GetProfileResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.rank.GetRankGuideResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.status.SetStatusRequest
import com.bluebrand.beedriver.data.source.remote.model.driver.status.SetStatusResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.transaction.GetTransactionsResponse
import kotlinx.coroutines.flow.Flow

interface IDriverApiService {
    fun setStatus(statusRequest: SetStatusRequest): Flow<SetStatusResponse>

    fun setLastOrder(statusRequest: SetStatusRequest): Flow<SetStatusResponse>

    fun setAutoAccept(statusRequest: SetStatusRequest): Flow<SetStatusResponse>

    fun getTransactions(): Flow<GetTransactionsResponse>

    fun getArCrHistory(): Flow<GetArCrResponse>

    fun getActiveIncentives(): Flow<GetIncentivesResponse>

    fun getIncentiveHistory(): Flow<GetIncentivesResponse>

    fun getDailyIncomeHistory(): Flow<GetDailyIncomeResponse>

    fun getWeeklyIncomeHistory(): Flow<GetWeeklyIncomeResponse>

    fun getMonthlyIncomeHistory(): Flow<GetMonthlyIncomeResponse>

    fun getRankGuide(): Flow<GetRankGuideResponse>

    fun getProfileData(): Flow<GetProfileResponse>
}