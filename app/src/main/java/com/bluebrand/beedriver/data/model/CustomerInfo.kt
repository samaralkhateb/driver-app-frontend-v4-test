package com.bluebrand.beedriver.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CustomerInfo(
    @Json(name = "name") val name: String,
    @Json(name = "mobile") val mobile: String,
    @Json(name = "street") val street: String,
    @Json(name = "near") val near: String,
    @Json(name = "floor") val floor: String,
    @Json(name = "area") val area: String,
    @Json(name = "details") val details: String,
    @Json(name = "latitude") val latitude: Double,
    @Json(name = "longitude") val longitude: Double,
    @Json(name = "note") val note: String?
) {
    val addressDetails: String
        get() = area.trim().plus(", ")
            .plus(street.trim()).plus(", ")
            .plus(near.trim()).plus(", ")
            .plus(floor.trim()).plus(", ")
            .plus(details.trim())
}