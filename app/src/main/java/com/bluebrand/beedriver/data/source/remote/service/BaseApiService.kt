package com.bluebrand.beedriver.data.source.remote.service

import com.bluebrand.beedriver.data.provider.remote.api.IApiProvider
import retrofit2.Response


abstract class BaseApiService(
    protected val retrofitService: IRetrofitService,
    private val apiProvider: IApiProvider
) {

    fun <R> makeRequest(call: suspend () -> Response<R>) = apiProvider.proceedRequest(call)
}