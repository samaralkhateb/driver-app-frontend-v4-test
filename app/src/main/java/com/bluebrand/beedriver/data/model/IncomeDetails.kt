package com.bluebrand.beedriver.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

sealed class IncomeDetails

@JsonClass(generateAdapter = true)
data class IncomeOrderDetails(
    @Json(name = "title") val title: String,
    @Json(name = "date") val date: String,
    @Json(name = "delivery") val delivery: Double,
    @Json(name = "your_fee") val driverFee: Double,
    @Json(name = "total") val total: Double?,
) : IncomeDetails()

@JsonClass(generateAdapter = true)
data class IncomeIncentiveDetails(
    @Json(name = "name") val name: String,
    @Json(name = "date") val date: String,
    @Json(name = "start_date") val startDate: String,
    @Json(name = "end_date") val endDate: String,
    @Json(name = "target") val target: Int,
    @Json(name = "incentive") val incentive: Double
) : IncomeDetails()