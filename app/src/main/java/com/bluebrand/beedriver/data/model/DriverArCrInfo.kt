package com.bluebrand.beedriver.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DriverArCrInfo(
    @Json(name = "acceptance_rate") var acceptanceRate: Int,
    @Json(name = "cancellation_rate") var cancellationRate: Int,
    @Json(name = "arWarningBlocking") val arWarningThresholds: WarningThresholds,
    @Json(name = "crWarningBlocking") val crWarningThresholds: WarningThresholds
)

@JsonClass(generateAdapter = true)
data class WarningThresholds(
    @Json(name = "warning_threshold") val warningThreshold: Int,
    @Json(name = "blocking_threshold") val blockingThreshold: Int
)