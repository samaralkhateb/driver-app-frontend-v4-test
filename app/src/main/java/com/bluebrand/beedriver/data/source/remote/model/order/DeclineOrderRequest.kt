package com.bluebrand.beedriver.data.source.remote.model.order

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DeclineOrderRequest(
    @Json(name = "bill_id") val billId: Long
)
