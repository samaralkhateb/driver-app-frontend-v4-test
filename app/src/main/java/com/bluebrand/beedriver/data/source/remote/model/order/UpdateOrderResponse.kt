package com.bluebrand.beedriver.data.source.remote.model.order

import com.bluebrand.beedriver.data.source.remote.model.base.BaseResponse

class UpdateOrderResponse : BaseResponse<Any>()