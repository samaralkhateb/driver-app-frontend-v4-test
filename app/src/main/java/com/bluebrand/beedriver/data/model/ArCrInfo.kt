package com.bluebrand.beedriver.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class ArCrInfo(
    @Json(name = "id") val id: Long,
    @Json(name = "RestaurantQueue") val restaurantQueue: String?,
    @Json(name = "RestaurantName") val restaurantName: String?,
    @Json(name = "AR") val acceptanceRate: String?,
    @Json(name = "CR") val cancellationRate: String?,
    @Json(name = "Status") val status: String?,
    @Json(name = "FinalOrderState") val finalOrderState: String?,
    @Json(name = "DateTime") val date: String?,
) : Serializable