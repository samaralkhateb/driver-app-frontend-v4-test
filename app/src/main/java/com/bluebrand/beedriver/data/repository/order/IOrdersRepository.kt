package com.bluebrand.beedriver.data.repository.order

import com.bluebrand.beedriver.data.source.remote.model.order.*
import kotlinx.coroutines.flow.Flow

interface IOrdersRepository {
    fun getDriverOrders(isSocketReconnection: Boolean): Flow<DriverOrdersData>

    fun acceptOrder(
        latitude: Double,
        longitude: Double,
        billId: Long,
        billDriverId: Long
    ): Flow<Boolean>

    fun declineOrder(
        billId: Long
    ): Flow<Boolean>

    fun arrivedToRestaurant(
        billId: Long,
        latitude: Double,
        longitude: Double
    ): Flow<Boolean>

    fun pickupOrder(
        billId: Long,
        billDriverId: Long,
        amount: Double
    ): Flow<Boolean>

    fun orderPaid(
        latitude: Double,
        longitude: Double,
        billId: Long,
        amount: Double,
        note: String
    ): Flow<Boolean>
}