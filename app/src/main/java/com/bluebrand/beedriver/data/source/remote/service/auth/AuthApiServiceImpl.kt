package com.bluebrand.beedriver.data.source.remote.service.auth

import com.bluebrand.beedriver.data.provider.remote.api.IApiProvider
import com.bluebrand.beedriver.data.source.remote.model.auth.login.LoginRequest
import com.bluebrand.beedriver.data.source.remote.model.auth.login.LoginResponse
import com.bluebrand.beedriver.data.source.remote.model.auth.verify.VerifyOtpRequest
import com.bluebrand.beedriver.data.source.remote.model.auth.verify.VerifyOtpResponse
import com.bluebrand.beedriver.data.source.remote.service.BaseApiService
import com.bluebrand.beedriver.data.source.remote.service.IRetrofitService
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class AuthApiServiceImpl @Inject constructor(
    retrofitService: IRetrofitService,
    apiProvider: IApiProvider
) : IAuthApiService, BaseApiService(retrofitService, apiProvider) {

    override fun login(loginRequest: LoginRequest): Flow<LoginResponse> {
        return makeRequest {
            retrofitService.login(loginRequest)
        }
    }

    override fun verifyOtp(verifyRequest: VerifyOtpRequest): Flow<VerifyOtpResponse> {
        return makeRequest {
            retrofitService.verifyOtp(verifyRequest)
        }
    }

    override fun resendOtp(request: LoginRequest): Flow<LoginResponse> {
        return makeRequest {
            retrofitService.resendOtp(request)
        }
    }
}