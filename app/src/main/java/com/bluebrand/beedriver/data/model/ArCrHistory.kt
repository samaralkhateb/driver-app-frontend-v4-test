package com.bluebrand.beedriver.data.model

import java.io.Serializable

data class ArCrHistory(
    val arCrInfoList: List<ArCrInfo>
) : Serializable