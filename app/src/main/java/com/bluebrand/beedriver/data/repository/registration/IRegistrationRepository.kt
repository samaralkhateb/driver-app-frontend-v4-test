package com.bluebrand.beedriver.data.repository.registration

import com.bluebrand.beedriver.data.model.City
import com.bluebrand.beedriver.data.model.Vehicle
import kotlinx.coroutines.flow.Flow

interface IRegistrationRepository {
    fun checkIfSignupEnabled(): Flow<Boolean>

    fun getCites(): Flow<List<City>>

    fun getVehicleTypes(): Flow<List<Vehicle>>

    fun signup(
        cityId: Int,
        vehicleTypeId: Int,
        phoneNumber: String,
        firstName: String,
        lastName: String,
        licensePlateNumber: String
    ) : Flow<Boolean>
}