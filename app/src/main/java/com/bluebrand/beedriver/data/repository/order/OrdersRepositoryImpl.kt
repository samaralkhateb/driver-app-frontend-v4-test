package com.bluebrand.beedriver.data.repository.order

import com.bluebrand.beedriver.data.repository.Repository
import com.bluebrand.beedriver.data.source.local.cache.AppCache
import com.bluebrand.beedriver.data.source.remote.model.order.*
import com.bluebrand.beedriver.data.source.remote.service.order.IOrdersApiService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class OrdersRepositoryImpl @Inject constructor(
    private val appCache: AppCache,
    private val ordersApiService: IOrdersApiService
) : Repository(), IOrdersRepository {

    override fun getDriverOrders(isSocketReconnection: Boolean): Flow<DriverOrdersData> {
        return ordersApiService.getDriverOrders(isSocketReconnection)
            .map {
                appCache.saveDriverInfo(it.data?.driverInfo)
                it.data!!
            }
    }

    override fun acceptOrder(
        latitude: Double,
        longitude: Double,
        billId: Long,
        billDriverId: Long
    ): Flow<Boolean> {
        return ordersApiService.acceptOrder(
            AcceptOrderRequest(latitude, longitude, billId, billDriverId)
        ).map {
            it.isSuccessful()
        }
    }

    override fun declineOrder(billId: Long): Flow<Boolean> {
        return ordersApiService.declineOrder(
            DeclineOrderRequest(billId)
        ).map {
            it.isSuccessful()
        }
    }

    override fun arrivedToRestaurant(
        billId: Long,
        latitude: Double,
        longitude: Double
    ): Flow<Boolean> {
        return ordersApiService.driverArrivedToRestaurant(
            ArrivedToRestaurantRequest(billId, latitude, longitude)
        ).map {
            it.isSuccessful()
        }
    }

    override fun pickupOrder(billId: Long, billDriverId: Long, amount: Double): Flow<Boolean> {
        return ordersApiService.pickupOrder(
            PickupOrderRequest(billId, billDriverId, amount)
        ).map {
            it.isSuccessful()
        }
    }

    override fun orderPaid(
        latitude: Double,
        longitude: Double,
        billId: Long,
        amount: Double,
        note: String
    ): Flow<Boolean> {
        return ordersApiService.orderPaid(
            OrderPaidRequest(latitude, longitude, billId, amount, note)
        ).map {
            it.isSuccessful()
        }
    }
}