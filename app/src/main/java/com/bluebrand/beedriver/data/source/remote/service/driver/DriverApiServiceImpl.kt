package com.bluebrand.beedriver.data.source.remote.service.driver

import com.bluebrand.beedriver.data.provider.remote.api.IApiProvider
import com.bluebrand.beedriver.data.source.remote.model.driver.arcr.GetArCrResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.incentive.GetIncentivesResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.income.GetDailyIncomeResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.income.GetMonthlyIncomeResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.income.GetWeeklyIncomeResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.profile.GetProfileResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.rank.GetRankGuideResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.status.SetStatusRequest
import com.bluebrand.beedriver.data.source.remote.model.driver.status.SetStatusResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.transaction.GetTransactionsResponse
import com.bluebrand.beedriver.data.source.remote.service.BaseApiService
import com.bluebrand.beedriver.data.source.remote.service.IRetrofitService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import javax.inject.Inject

class DriverApiServiceImpl @Inject constructor(
    retrofitService: IRetrofitService,
    apiProvider: IApiProvider
) : IDriverApiService,
    BaseApiService(retrofitService, apiProvider) {

    override fun setStatus(statusRequest: SetStatusRequest): Flow<SetStatusResponse> {
        return makeRequest {
            retrofitService.setDriverStatus(statusRequest)
        }
    }

    override fun setLastOrder(statusRequest: SetStatusRequest): Flow<SetStatusResponse> {
        return makeRequest {
            retrofitService.setDriverLastOrder(statusRequest)
        }
    }

    override fun setAutoAccept(statusRequest: SetStatusRequest): Flow<SetStatusResponse> {
        return makeRequest {
            retrofitService.setDriverAutoAccept(statusRequest)
        }
    }

    override fun getTransactions(): Flow<GetTransactionsResponse> {
        return makeRequest {
            retrofitService.getDriverTransactions()
        }
    }

    override fun getArCrHistory(): Flow<GetArCrResponse> {
        return makeRequest {
            retrofitService.getDriverArCrHistory()
        }
    }

    override fun getActiveIncentives(): Flow<GetIncentivesResponse> {
        return makeRequest {
            retrofitService.getIncentives(1)
        }
    }

    override fun getIncentiveHistory(): Flow<GetIncentivesResponse> {
        return makeRequest {
            retrofitService.getIncentives(0)
        }
    }

    override fun getDailyIncomeHistory(): Flow<GetDailyIncomeResponse> {
        return makeRequest {
            retrofitService.getDriverDailyIncomeHistory()
        }
    }

    override fun getWeeklyIncomeHistory(): Flow<GetWeeklyIncomeResponse> {
        return makeRequest {
            retrofitService.getDriverWeeklyIncomeHistory()
        }
    }

    override fun getMonthlyIncomeHistory(): Flow<GetMonthlyIncomeResponse> {
        return makeRequest {
            retrofitService.getDriverMonthlyIncomeHistory()
        }
    }

    override fun getRankGuide(): Flow<GetRankGuideResponse> {
        return makeRequest {
            retrofitService.getRankGuide()
        }
    }

    override fun getProfileData(): Flow<GetProfileResponse> {
        return makeRequest {
            retrofitService.getProfileData()
        }
    }
}