package com.bluebrand.beedriver.data.repository.auth

import com.bluebrand.beedriver.data.repository.Repository
import com.bluebrand.beedriver.data.source.local.cache.AppCache
import com.bluebrand.beedriver.data.source.remote.model.auth.login.LoginRequest
import com.bluebrand.beedriver.data.source.remote.model.auth.verify.VerifyOtpRequest
import com.bluebrand.beedriver.data.source.remote.service.auth.IAuthApiService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val appCash: AppCache,
    private val authApiService: IAuthApiService
) : IAuthRepository, Repository() {

    override fun isLoggedIn(): Flow<Boolean> {
        return flowOf(!appCash.getSavedToken().isNullOrEmpty())
    }

    override fun login(phoneNumber: String): Flow<Boolean> {
        return authApiService.login(
            LoginRequest(phoneNumber)
        ).map { it.isSuccessful() }
    }

    override fun verifyOtp(
        phoneNumber: String,
        otp: String,
        androidVersion: String,
        manufacturer: String,
        mobileModel: String,
        fcmToken: String?
    ): Flow<Boolean> {
        return authApiService.verifyOtp(
            VerifyOtpRequest(phoneNumber, otp, androidVersion, manufacturer, mobileModel, fcmToken)
        ).map {
            if (it.isSuccessful()) {
                appCash.saveDriverPhoneNumber(phoneNumber)
                appCash.saveAccessToken(it.data?.accessToken)
            }
            it.isSuccessful()
        }
    }

    override fun resendOtp(phoneNumber: String): Flow<Boolean> {
        return authApiService.resendOtp(
            LoginRequest(phoneNumber)
        ).map { it.isSuccessful() }
    }
}