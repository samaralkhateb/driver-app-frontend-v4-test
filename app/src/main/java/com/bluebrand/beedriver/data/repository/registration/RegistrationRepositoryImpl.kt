package com.bluebrand.beedriver.data.repository.registration

import com.bluebrand.beedriver.data.model.City
import com.bluebrand.beedriver.data.model.Vehicle
import com.bluebrand.beedriver.data.repository.Repository
import com.bluebrand.beedriver.data.source.local.cache.AppCache
import com.bluebrand.beedriver.data.source.remote.model.registration.signup.SignupRequest
import com.bluebrand.beedriver.data.source.remote.service.registration.IRegistrationApiService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class RegistrationRepositoryImpl @Inject constructor(
    private val appCache: AppCache,
    private val registrationApiService: IRegistrationApiService
) : IRegistrationRepository, Repository() {

    override fun checkIfSignupEnabled(): Flow<Boolean> {
        return registrationApiService.getAppInfo()
            .map { response ->
                response.data?.let { appInfo ->
                    appCache.saveSupportPhoneNumber(appInfo.supportPhoneNumber)
                    appInfo.isSignupEnabled
                } ?: false
            }
    }

    override fun getCites(): Flow<List<City>> {
        return registrationApiService.getCites()
            .map {
                it.data ?: emptyList()
            }
    }

    override fun getVehicleTypes(): Flow<List<Vehicle>> {
        return registrationApiService.getVehicleTypes()
            .map {
                it.data ?: emptyList()
            }
    }

    override fun signup(
        cityId: Int,
        vehicleTypeId: Int,
        phoneNumber: String,
        firstName: String,
        lastName: String,
        licensePlateNumber: String
    ): Flow<Boolean> {
        return registrationApiService.signup(
            SignupRequest(
                cityId,
                vehicleTypeId,
                phoneNumber,
                firstName,
                lastName,
                licensePlateNumber
            )
        ).map { it.isSuccessful() }
    }
}