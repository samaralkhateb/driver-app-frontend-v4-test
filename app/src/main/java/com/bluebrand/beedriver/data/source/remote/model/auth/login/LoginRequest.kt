package com.bluebrand.beedriver.data.source.remote.model.auth.login

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LoginRequest(
    @Json(name = "mobile") val phoneNumber: String
)