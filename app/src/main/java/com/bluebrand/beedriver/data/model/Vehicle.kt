package com.bluebrand.beedriver.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Vehicle(
    @Json(name = "id") val id: Int,
    @Json(name = "name") val name: String
){
    override fun toString(): String {
        return name
    }
}