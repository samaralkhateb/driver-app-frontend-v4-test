package com.bluebrand.beedriver.data.source.remote.model.driver.income

import com.bluebrand.beedriver.data.model.IncomeInfo
import com.bluebrand.beedriver.data.source.remote.model.base.BaseResponse
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

open class GetIncomeBaseResponse<T : IncomeInfo> : BaseResponse<IncomeData<T>>()

@JsonClass(generateAdapter = true)
data class IncomeData<out T>(
    @Json(name = "income") val incomeList: List<T>
)