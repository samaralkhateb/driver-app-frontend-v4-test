package com.bluebrand.beedriver.data.provider.remote.interceptor

import com.bluebrand.beedriver.BuildConfig
import com.bluebrand.beedriver.data.source.local.cache.AppCache
import com.bluebrand.beedriver.data.util.constants.RemoteConstants
import com.bluebrand.beedriver.data.util.network.ResponseCode
import com.bluebrand.beedriver.presentation.busevent.UnAuthorizedUserEvent
import com.bluebrand.beedriver.presentation.util.bus.Bus
import com.bluebrand.beedriver.presentation.util.locale.LocaleManager
import okhttp3.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RequestInterceptor @Inject constructor(
    private val cache: AppCache
) : Interceptor {
    private val appVersion by lazy {
        BuildConfig.VERSION_NAME
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val newRequest = chain.request().newBuilder()
            .addHeader(RemoteConstants.Headers.APP_VERSION_HEADER, appVersion)
            .addHeader(RemoteConstants.Headers.LANGUAGE_HEADER, LocaleManager.savedLanguage)
            .run {
                val token = cache.getSavedToken()
                token?.let { addHeader(RemoteConstants.Headers.TOKEN_HEADER, it) } ?: this
            }
            .run {
                val driverPhoneNumber = cache.getDriverPhoneNumber()
                driverPhoneNumber?.let { addHeader(RemoteConstants.Headers.MOBILE_HEADER, it) } ?: this
            }
            .build()
        val response = chain.proceed(newRequest)
        if (response.code == ResponseCode.UNAUTHORIZED_USER){
            publishUnAuthorizedUserEvent()
        }
        return response
    }

    private fun publishUnAuthorizedUserEvent() {
        cache.logout()
        Bus.publishEvent(UnAuthorizedUserEvent)
    }
}