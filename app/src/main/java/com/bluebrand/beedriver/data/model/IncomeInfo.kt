package com.bluebrand.beedriver.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

sealed class IncomeInfo(
    val date: String,
    val total: Double,
    val incomeDetails: List<IncomeDetails> = emptyList()
)

@JsonClass(generateAdapter = true)
class DailyIncomeInfo(
    @Json(name = "date") date: String,
    @Json(name = "total") total: Double,
    @Json(name = "data") incomeDetails: List<IncomeDetails> = emptyList()
) : IncomeInfo(date, total, incomeDetails)


@JsonClass(generateAdapter = true)
class WeeklyIncomeInfo(
    @Json(name = "from") val startDate: String,
    @Json(name = "to") val endDate: String,
    @Json(name = "total") total: Double
) : IncomeInfo(startDate.plus(" - ").plus(endDate), total)


@JsonClass(generateAdapter = true)
class MonthlyIncomeInfo(
    @Json(name = "date") date: String,
    @Json(name = "total") total: Double
) : IncomeInfo(date, total)