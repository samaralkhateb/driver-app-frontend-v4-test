package com.bluebrand.beedriver.data.provider.remote.api

import com.bluebrand.beedriver.data.exception.NoInternetException
import com.bluebrand.beedriver.data.exception.RequestTimedOutException
import com.bluebrand.beedriver.data.exception.ServerException
import com.bluebrand.beedriver.data.provider.remote.networkchecker.NetworkChecker
import com.bluebrand.beedriver.data.source.remote.model.base.BaseResponse
import com.bluebrand.beedriver.data.util.unMap
import com.squareup.moshi.Types
import kotlinx.coroutines.flow.flow
import retrofit2.Response
import java.io.InterruptedIOException

class ApiProvider(
    private val networkChecker: NetworkChecker,
    private val exceptionFactory: ExceptionFactory
) : IApiProvider {

    override fun <T> proceedRequest(call: suspend () -> Response<T>) = flow {
        if (isConnectedOrFail()) {
            val response = getResponseOrFail(call)
            val body = getResponseBodyOrFail(response)
            emit(body)
        }
    }

    private suspend fun isConnectedOrFail(): Boolean {
        if (networkChecker.isConnected()) return true
        throw NoInternetException()
    }

    private suspend fun <T> getResponseOrFail(call: suspend () -> Response<T>): Response<T> {
        return try {
            call.invoke()
        } catch (e: Exception) {
            if (e is InterruptedIOException) throw RequestTimedOutException(e)
            else throw ServerException(e)
        }
    }

    private fun <T> getResponseBodyOrFail(response: Response<T>): T {
        val body = response.body()
        if (body != null) {
            val baseResponse = response.body() as? BaseResponse<*>
            if (baseResponse?.isSuccessful() == true) {
                return body
            } else {
                throw exceptionFactory.fromCode(
                    baseResponse?.statusCode ?: "",
                    response.code(),
                    baseResponse?.message
                )
            }
        } else {
            val baseResponse = parseErrorResponse(response)
            throw exceptionFactory.fromCode(
                baseResponse?.statusCode ?: "",
                response.code(),
                baseResponse?.message
            )
        }
    }

    private fun parseErrorResponse(response: Response<*>): BaseResponse<*>? {
        return try {
            val json = response.errorBody()!!.string()
            return json.unMap(
                Types.newParameterizedType(BaseResponse::class.java, Any::class.java)
            ) as? BaseResponse<*>
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }
}