package com.bluebrand.beedriver.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Incentive(
    @Json(name = "incentive_id") val id: Int,
    @Json(name = "incentive_name") val name: String,
    @Json(name = "city") val cityName: String,
    @Json(name = "start_date") val startDate: String,
    @Json(name = "end_date") val endDate: String,
    @Json(name = "driver_rank") val driverRank: String?,
    @Json(name = "description") val description: String?,
    @Json(name = "amount") val amount: String,
    @Json(name = "total_target") val totalTarget: Int,
    @Json(name = "current_target") val currentTarget: Int,
    @Json(name = "status") val status: Int
){
    val isAchieved: Boolean = status == 1
}