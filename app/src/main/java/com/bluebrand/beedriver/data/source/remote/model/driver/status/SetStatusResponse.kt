package com.bluebrand.beedriver.data.source.remote.model.driver.status

import com.bluebrand.beedriver.data.source.remote.model.base.BaseResponse

class SetStatusResponse : BaseResponse<Any>()