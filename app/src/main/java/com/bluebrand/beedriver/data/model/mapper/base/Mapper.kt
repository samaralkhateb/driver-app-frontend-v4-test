package com.bluebrand.beedriver.data.model.mapper.base

abstract class Mapper<TO, FROM> : IMappable<TO, FROM> {
    abstract override fun map(model: FROM): TO

    open fun unmap(model: TO): FROM {
        throw NotImplementedError()
    }

    fun map(models: List<FROM>): List<TO> {
        return models.map(this::map).toList()
    }

    fun unmap(models: List<TO>): List<FROM> {
        return models.map(this::unmap).toList()
    }
}