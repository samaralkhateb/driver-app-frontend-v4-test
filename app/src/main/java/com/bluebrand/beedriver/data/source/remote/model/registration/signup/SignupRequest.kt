package com.bluebrand.beedriver.data.source.remote.model.registration.signup

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SignupRequest(
    @Json(name = "cityId") val cityId: Int,
    @Json(name = "vehicle") val vehicleTypeId: Int,
    @Json(name = "mobile") val phoneNumber: String,
    @Json(name = "firstName") val firstName: String,
    @Json(name = "lastName") val lastName: String,
    @Json(name = "licensePlate") val licensePlateNumber: String
)