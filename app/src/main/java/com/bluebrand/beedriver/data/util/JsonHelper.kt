package com.bluebrand.beedriver.data.util

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import java.lang.reflect.ParameterizedType


private fun <T> getJsonAdapter(clz: Class<T>): JsonAdapter<T> {
    return Moshi.Builder().build().adapter(clz)
}

private fun getJsonAdapter(type: ParameterizedType): JsonAdapter<Any> {
    return Moshi.Builder().build().adapter(type)
}

fun <T : Any> T.toJson(): String {
    return getJsonAdapter(this.javaClass).toJson(this)
}

fun <K> String.unMap(clz: Class<K>): K? {
    return getJsonAdapter(clz).fromJson(this)
}

fun String.unMap(type: ParameterizedType): Any? {
    return getJsonAdapter(type).fromJson(this)
}