package com.bluebrand.beedriver.data.repository.auth

import kotlinx.coroutines.flow.Flow

interface IAuthRepository {
    fun isLoggedIn(): Flow<Boolean>

    fun login(phoneNumber: String): Flow<Boolean>

    fun verifyOtp(
        phoneNumber: String, otp: String, androidVersion: String, manufacturer: String, mobileModel: String, fcmToken: String?
    ): Flow<Boolean>

    fun resendOtp(phoneNumber: String): Flow<Boolean>
}