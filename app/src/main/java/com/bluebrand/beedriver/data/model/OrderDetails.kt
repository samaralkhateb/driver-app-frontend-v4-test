package com.bluebrand.beedriver.data.model

import com.bluebrand.beedriver.presentation.ui.common.Constants
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OrderDetails(
    @Json(name = "order_id") val orderId: Long,
    @Json(name = "bill_id") val billId: Long,
    @Json(name = "bill_driver_id") val billDriverId: Long,
    @Json(name = "restaurant_queue") val restaurantQueue: String,
    @Json(name = "restaurant_name") val restaurantName: String,
    @Json(name = "restaurant_address") val restaurantAddress: String?,
    @Json(name = "restaurant_latitude") val restaurantLatitude: Double,
    @Json(name = "restaurant_longitude") val restaurantLongitude: Double,
    @Json(name = "driver_fee") val driverFee: Double,
    @Json(name = "total") val total: Double,
    @Json(name = "net_total") val netTotal: Double,
    @Json(name = "ghost_distance") val dropOffDistance: Double,
    @Json(name = "total_distance") val totalDistance: Double,
    @Json(name = "address_restaurant_google_distance") val restaurantDistance: Double,
    @Json(name = "end_time") val acceptanceTimeout: String,
    @Json(name = "expected_time") val customerExpectedTime: String,
    @Json(name = "expected_driver_arrival_time") val restaurantExpectedTime: String,
    @Json(name = "is_epayment") val isE_Payment: Boolean,
    @Json(name = "billStatus") var billStatus: Int,
    @Json(name = "billStage") val billStage: Int,
    @Json(name = "status_as_string") val  orderStatus: String?,
    @Json(name = "billDriverStatus") val billDriverStatus: Int,
    @Json(name = "is_arrived_to_restaurant") var arrivedToRestaurant: Boolean,
    @Json(name = "customer") val customerInfo: CustomerInfo,
    @Json(name = "items") val items: List<OrderItem> = emptyList()
) {
    val isPending: Boolean
        get() = billDriverStatus == Constants.BILL_DRIVER_STATUS_PENDING

    val isOutForDelivery: Boolean
        get() = billStatus == Constants.BILL_STATUS_OUT_FOR_DELIVERY

    val itemsCount: Int
        get() = items.sumOf { it.quantity }
}