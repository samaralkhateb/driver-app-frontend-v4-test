package com.bluebrand.beedriver.data.source.remote.model.driver.status

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SetStatusRequest(
    @Json(name = "status") val status: Boolean
)
