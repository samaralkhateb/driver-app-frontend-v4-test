package com.bluebrand.beedriver.data.repository.socket

import android.location.Location
import com.bluebrand.beedriver.data.model.DriverInfo
import com.bluebrand.beedriver.data.model.Notification
import com.bluebrand.beedriver.data.source.remote.model.order.DriverOrdersData
import kotlinx.coroutines.flow.Flow

interface ISocketRepository {
    fun connect()

    fun disconnect()

    fun getConnectionStatus(): Flow<Boolean>

    suspend fun updateDriverLocation(location: Location, batteryPercentage: Int, signalStrength: Int?)

    suspend fun orderReceived(orderId: Long, restaurantQueue: String)

    fun socketNotificationReceived(): Flow<Notification>

    fun driverInfoUpdated(): Flow<DriverInfo>

    fun newOrderReceived(): Flow<DriverOrdersData>
}