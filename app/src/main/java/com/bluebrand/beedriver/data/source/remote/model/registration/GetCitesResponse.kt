package com.bluebrand.beedriver.data.source.remote.model.registration

import com.bluebrand.beedriver.data.model.City
import com.bluebrand.beedriver.data.source.remote.model.base.BaseResponse

class GetCitesResponse : BaseResponse<List<City>>()