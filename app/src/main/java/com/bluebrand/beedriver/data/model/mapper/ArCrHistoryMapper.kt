package com.bluebrand.beedriver.data.model.mapper

import com.bluebrand.beedriver.data.model.ArCrHistory
import com.bluebrand.beedriver.data.model.ArCrInfo
import com.bluebrand.beedriver.data.model.mapper.base.Mapper
import javax.inject.Inject

class ArCrHistoryMapper @Inject constructor() : Mapper<ArCrHistory, List<ArCrInfo>?>() {

    override fun map(model: List<ArCrInfo>?): ArCrHistory {
        return ArCrHistory(model ?: emptyList())
    }
}