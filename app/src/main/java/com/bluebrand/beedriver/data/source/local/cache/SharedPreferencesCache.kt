package com.bluebrand.beedriver.data.source.local.cache

import com.bluebrand.beedriver.data.model.DriverInfo
import com.bluebrand.beedriver.data.provider.local.sharedpreferences.LiveSharedPreferences
import com.bluebrand.beedriver.data.util.constants.SharedPreferencesKeys.DRIVER_INFO
import com.bluebrand.beedriver.data.util.constants.SharedPreferencesKeys.DRIVER_PHONE
import com.bluebrand.beedriver.data.util.constants.SharedPreferencesKeys.MAP_DOWNLOADED
import com.bluebrand.beedriver.data.util.constants.SharedPreferencesKeys.SUPPORT_PHONE
import com.bluebrand.beedriver.data.util.constants.SharedPreferencesKeys.TOKEN
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharedPreferencesCache @Inject constructor(
    private val sharedPreferences: LiveSharedPreferences,
) : AppCache {

    override fun saveDriverPhoneNumber(phoneNumber: String?) {
        sharedPreferences.putString(DRIVER_PHONE, phoneNumber)
    }

    override fun getDriverPhoneNumber(): String? {
        return sharedPreferences.getString(DRIVER_PHONE, null)
    }

    override fun saveAccessToken(token: String?) {
        sharedPreferences.putString(TOKEN, token)
    }

    override fun getSavedToken(): String? {
        return sharedPreferences.getString(TOKEN, null)
    }

    override fun saveSupportPhoneNumber(phoneNumber: String?) {
        sharedPreferences.putString(SUPPORT_PHONE, phoneNumber)
    }

    override fun getSupportPhoneNumber(): String? {
        return sharedPreferences.getString(SUPPORT_PHONE, null)
    }

    override fun saveDriverInfo(driverInfo: DriverInfo?) {
        sharedPreferences.putObject(DRIVER_INFO, driverInfo)
    }

    override fun getDriverInfo(): Flow<DriverInfo?> {
        return sharedPreferences.getObjectFlow(DRIVER_INFO, DriverInfo::class.java)
    }

    override fun isMapDownloaded(): Boolean {
        return sharedPreferences.getBoolean(MAP_DOWNLOADED, false)
    }

    override fun setMapDownloaded(isDownloaded: Boolean) {
        sharedPreferences.putBoolean(MAP_DOWNLOADED, isDownloaded)
    }

    override fun logout() {
        sharedPreferences.clearAll()
    }
}