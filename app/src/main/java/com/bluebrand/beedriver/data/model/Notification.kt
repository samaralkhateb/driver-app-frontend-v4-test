package com.bluebrand.beedriver.data.model

import androidx.annotation.ColorRes
import androidx.annotation.RawRes
import com.bluebrand.beedriver.R
import java.io.Serializable

data class Notification(
    val id: Int,
    val type: String,
    val text: String,
    val orderId: Long? = null,
    val color: Color? = null,
    val sound: Sound? = null
) : Serializable {

    enum class Sound(val level: Int, @RawRes val resId: Int) : Serializable {
        VERY_ANNOYING(1, R.raw.incoming),
        ANNOYING(2, R.raw.notification_sound_1),
        HIGH(3, R.raw.notification_sound_2),
        MEDIUM(4, R.raw.notification_sound_3),
        LOW(5, R.raw.notification_sound_4);

        companion object {
            fun of(level: Int): Sound {
                return values().first { it.level == level }
            }
        }
    }

    enum class Color(val value: String, @ColorRes val resId: Int) : Serializable {
        GREEN("green", R.color.green),
        RED("red", R.color.red),
        YELLOW("yellow", R.color.yellow);

        companion object {
            fun of(value: String): Color {
                return values().first { it.value == value }
            }
        }
    }
}

