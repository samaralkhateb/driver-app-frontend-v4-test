package com.bluebrand.beedriver.data.util.constants

object SharedPreferencesKeys {
    const val APP_SHARED_KEY = "bee_driver_shared_preferences"

    const val TOKEN = "key_token"
    const val APP_LANGUAGE = "key_app_language"
    const val DRIVER_PHONE = "key_driver_phone"
    const val SUPPORT_PHONE = "key_support_phone"
    const val DRIVER_INFO = "key_driver_info"
    const val MAP_DOWNLOADED = "key_map_downloaded"
}