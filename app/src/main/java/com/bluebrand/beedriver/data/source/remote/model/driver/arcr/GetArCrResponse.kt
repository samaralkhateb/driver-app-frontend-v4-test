package com.bluebrand.beedriver.data.source.remote.model.driver.arcr

import com.bluebrand.beedriver.data.model.ArCrInfo
import com.bluebrand.beedriver.data.source.remote.model.base.BaseResponse

class GetArCrResponse : BaseResponse<List<ArCrInfo>>() {
}