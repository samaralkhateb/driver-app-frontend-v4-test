package com.bluebrand.beedriver.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class OrderItem(
    @Json(name = "id") val id: Long,
    @Json(name = "dish_name") val name: String,
    @Json(name = "quantity") val quantity: Int,
    @Json(name = "dish_price") val price: Double,
    @Json(name = "purchase_total") val total: Double,
    @Json(name = "special_instructions") val note: String?,
){
    var isChecked: Boolean = false
}
