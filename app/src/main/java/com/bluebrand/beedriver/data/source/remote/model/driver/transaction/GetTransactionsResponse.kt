package com.bluebrand.beedriver.data.source.remote.model.driver.transaction

import com.bluebrand.beedriver.data.model.Transaction
import com.bluebrand.beedriver.data.model.TransactionsInfo
import com.bluebrand.beedriver.data.source.remote.model.base.BaseResponse
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

class GetTransactionsResponse : BaseResponse<TransactionsInfo>()

