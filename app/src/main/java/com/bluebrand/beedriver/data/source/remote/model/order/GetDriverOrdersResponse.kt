package com.bluebrand.beedriver.data.source.remote.model.order

import com.bluebrand.beedriver.data.model.DriverArCrInfo
import com.bluebrand.beedriver.data.model.DriverInfo
import com.bluebrand.beedriver.data.model.OrderDetails
import com.bluebrand.beedriver.data.source.remote.model.base.BaseResponse
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

class GetDriverOrdersResponse : BaseResponse<DriverOrdersData>()

@JsonClass(generateAdapter = true)
data class DriverOrdersData(
    @Json(name = "orders") val orders: List<OrderDetails> = emptyList(),
    @Json(name = "arCrValue") val arCrInfo: DriverArCrInfo,
    @Json(name = "driver") val driverInfo: DriverInfo
)