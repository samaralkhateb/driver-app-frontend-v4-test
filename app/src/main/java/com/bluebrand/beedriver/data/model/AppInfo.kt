package com.bluebrand.beedriver.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AppInfo(
    @Json(name = "mobile") val supportPhoneNumber: String,
    @Json(name = "version") val appVersion: String,
    @Json(name = "signup-able") val isSignupEnabled: Boolean
)