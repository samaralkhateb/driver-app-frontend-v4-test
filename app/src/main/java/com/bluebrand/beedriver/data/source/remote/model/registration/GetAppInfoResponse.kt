package com.bluebrand.beedriver.data.source.remote.model.registration

import com.bluebrand.beedriver.data.model.AppInfo
import com.bluebrand.beedriver.data.source.remote.model.base.BaseResponse

class GetAppInfoResponse : BaseResponse<AppInfo>()