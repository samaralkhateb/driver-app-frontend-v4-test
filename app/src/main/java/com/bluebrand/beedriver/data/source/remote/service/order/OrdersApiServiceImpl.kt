package com.bluebrand.beedriver.data.source.remote.service.order

import com.bluebrand.beedriver.data.provider.remote.api.IApiProvider
import com.bluebrand.beedriver.data.source.remote.model.order.*
import com.bluebrand.beedriver.data.source.remote.service.BaseApiService
import com.bluebrand.beedriver.data.source.remote.service.IRetrofitService
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class OrdersApiServiceImpl @Inject constructor(
    retrofitService: IRetrofitService,
    apiProvider: IApiProvider
) : BaseApiService(retrofitService, apiProvider), IOrdersApiService {

    override fun getDriverOrders(isSocketReconnection: Boolean): Flow<GetDriverOrdersResponse> {
        return makeRequest {
            retrofitService.getDriverOrders(if (isSocketReconnection) 1 else 0)
        }
    }

    override fun acceptOrder(acceptOrderRequest: AcceptOrderRequest): Flow<UpdateOrderResponse> {
        return makeRequest {
            retrofitService.acceptOrder(acceptOrderRequest)
        }
    }

    override fun declineOrder(declineOrderRequest: DeclineOrderRequest): Flow<UpdateOrderResponse> {
        return makeRequest {
            retrofitService.declineOrder(declineOrderRequest)
        }
    }

    override fun driverArrivedToRestaurant(arrivedToRestaurantRequest: ArrivedToRestaurantRequest): Flow<UpdateOrderResponse> {
        return makeRequest {
            retrofitService.driverArrivedToRestaurant(arrivedToRestaurantRequest)
        }
    }

    override fun pickupOrder(pickupOrderRequest: PickupOrderRequest): Flow<UpdateOrderResponse> {
        return makeRequest {
            retrofitService.pickupOrder(pickupOrderRequest)
        }
    }

    override fun orderPaid(orderPaidRequest: OrderPaidRequest): Flow<UpdateOrderResponse> {
        return makeRequest {
            retrofitService.orderPaid(orderPaidRequest)
        }
    }
}