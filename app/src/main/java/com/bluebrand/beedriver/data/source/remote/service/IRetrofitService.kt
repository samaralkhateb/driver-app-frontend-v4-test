package com.bluebrand.beedriver.data.source.remote.service

import com.bluebrand.beedriver.data.source.remote.model.auth.login.LoginRequest
import com.bluebrand.beedriver.data.source.remote.model.auth.login.LoginResponse
import com.bluebrand.beedriver.data.source.remote.model.auth.verify.VerifyOtpRequest
import com.bluebrand.beedriver.data.source.remote.model.auth.verify.VerifyOtpResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.arcr.GetArCrResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.incentive.GetIncentivesResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.income.GetDailyIncomeResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.income.GetMonthlyIncomeResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.income.GetWeeklyIncomeResponse
import com.bluebrand.beedriver.data.source.remote.model.order.*
import com.bluebrand.beedriver.data.source.remote.model.driver.profile.GetProfileResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.rank.GetRankGuideResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.status.SetStatusRequest
import com.bluebrand.beedriver.data.source.remote.model.driver.status.SetStatusResponse
import com.bluebrand.beedriver.data.source.remote.model.driver.transaction.GetTransactionsResponse
import com.bluebrand.beedriver.data.source.remote.model.registration.GetAppInfoResponse
import com.bluebrand.beedriver.data.source.remote.model.registration.GetCitesResponse
import com.bluebrand.beedriver.data.source.remote.model.registration.GetVehicleTypesResponse
import com.bluebrand.beedriver.data.source.remote.model.registration.signup.SignupRequest
import com.bluebrand.beedriver.data.source.remote.model.registration.signup.SignupResponse
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.ACCEPT_ORDER
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.ARRIVED_TO_RESTAURANT
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.AR_CR_HISTORY
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.AUTO_ACCEPT
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.CITY
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.DECLINE_ORDER
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.DRIVER_ORDERS
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.INCENTIVE
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.INCOME_HISTORY
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.TRANSACTIONS
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.INFO
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.LAST_ORDER
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.LOGIN
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.ORDER_PAID
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.PICKUP_ORDER
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.RANK_DETAILS
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.RANK_GUIDE
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.RESEND_OTP
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.STATUS
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.SIGNUP
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.VEHICLES
import com.bluebrand.beedriver.data.util.constants.RemoteConstants.ApiEndPoints.VERIFY_OTP
import retrofit2.Response
import retrofit2.http.*

interface IRetrofitService {
    @POST(LOGIN)
    suspend fun login(@Body request: LoginRequest): Response<LoginResponse>

    @POST(VERIFY_OTP)
    suspend fun verifyOtp(@Body request: VerifyOtpRequest): Response<VerifyOtpResponse>

    @POST(RESEND_OTP)
    suspend fun resendOtp(@Body request: LoginRequest): Response<LoginResponse>

    @GET(INFO)
    suspend fun getAppInfo(): Response<GetAppInfoResponse>

    @GET(CITY)
    suspend fun getCites(): Response<GetCitesResponse>

    @GET(VEHICLES)
    suspend fun getVehicles(): Response<GetVehicleTypesResponse>

    @POST(SIGNUP)
    suspend fun signup(@Body request: SignupRequest): Response<SignupResponse>

    @POST(STATUS)
    suspend fun setDriverStatus(@Body request: SetStatusRequest): Response<SetStatusResponse>

    @POST(LAST_ORDER)
    suspend fun setDriverLastOrder(@Body request: SetStatusRequest): Response<SetStatusResponse>

    @POST(AUTO_ACCEPT)
    suspend fun setDriverAutoAccept(@Body request: SetStatusRequest): Response<SetStatusResponse>

    @GET(TRANSACTIONS)
    suspend fun getDriverTransactions(): Response<GetTransactionsResponse>

    @GET(AR_CR_HISTORY)
    suspend fun getDriverArCrHistory(): Response<GetArCrResponse>

    @GET(INCENTIVE)
    suspend fun getIncentives(@Query("status") status: Int): Response<GetIncentivesResponse>

    @GET(INCOME_HISTORY.plus("?type=1"))
    suspend fun getDriverDailyIncomeHistory(): Response<GetDailyIncomeResponse>

    @GET(INCOME_HISTORY.plus("?type=2"))
    suspend fun getDriverWeeklyIncomeHistory(): Response<GetWeeklyIncomeResponse>

    @GET(INCOME_HISTORY.plus("?type=3"))
    suspend fun getDriverMonthlyIncomeHistory(): Response<GetMonthlyIncomeResponse>

    @GET(RANK_GUIDE)
    suspend fun getRankGuide(): Response<GetRankGuideResponse>

    @GET(RANK_DETAILS)
    suspend fun getProfileData(): Response<GetProfileResponse>

    @GET(DRIVER_ORDERS)
    suspend fun getDriverOrders(@Header("reconnecting") socketReconnection: Int): Response<GetDriverOrdersResponse>

    @POST(ACCEPT_ORDER)
    suspend fun acceptOrder(@Body request: AcceptOrderRequest): Response<UpdateOrderResponse>

    @POST(DECLINE_ORDER)
    suspend fun declineOrder(@Body request: DeclineOrderRequest): Response<UpdateOrderResponse>

    @POST(ARRIVED_TO_RESTAURANT)
    suspend fun driverArrivedToRestaurant(@Body request: ArrivedToRestaurantRequest): Response<UpdateOrderResponse>

    @POST(PICKUP_ORDER)
    suspend fun pickupOrder(@Body request: PickupOrderRequest): Response<UpdateOrderResponse>

    @POST(ORDER_PAID)
    suspend fun orderPaid(@Body request: OrderPaidRequest): Response<UpdateOrderResponse>
}