package com.bluebrand.beedriver.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class Transaction(
    @Json(name = "id") val id: Long,
    @Json(name = "balance") val balance: Double,
    @Json(name = "debit") val debit: Double,
    @Json(name = "credit") val credit: Double,
    @Json(name = "statement") val note: String,
    @Json(name = "date") val date: String,
    @Json(name = "journal_number") val journalNumber: String
): Serializable