package com.bluebrand.beedriver.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DriverRankDetails(
    @Json(name = "rankName") val rankName: String,
    @Json(name = "ArCrValue") val arCrInfo: DriverArCrInfo,
    @Json(name = "totalNumberDeliveredOrdersForLastCycle") val deliveredOrders: Int,
    @Json(name = "remainingNumber") val remainingOrders: Int,
    @Json(name = "driverCycleDate") val cycleEndDate: String?,
    @Json(name = "isLastRank") val isLastRank: Boolean?,
)