package com.bluebrand.beedriver.data.source.remote.model.registration.signup

import com.bluebrand.beedriver.data.source.remote.model.base.BaseResponse

class SignupResponse : BaseResponse<Any>()