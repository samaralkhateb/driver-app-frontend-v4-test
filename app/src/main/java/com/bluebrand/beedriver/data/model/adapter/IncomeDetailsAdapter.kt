package com.bluebrand.beedriver.data.model.adapter

import com.bluebrand.beedriver.data.model.IncomeDetails
import com.bluebrand.beedriver.data.model.IncomeIncentiveDetails
import com.bluebrand.beedriver.data.model.IncomeOrderDetails
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson


class IncomeDetailsAdapter {
    @FromJson
    fun fromJson(raw: Map<String, Any>): IncomeDetails {

        return when(val type = raw["type"] as Double){
            1.0 -> {
                IncomeOrderDetails(
                    raw["title"] as String,
                    raw["date"] as String,
                    raw["delivery"] as Double,
                    raw["your_fee"] as Double,
                    raw["total"] as Double?
                )
            }

            2.0 -> {
                IncomeIncentiveDetails(
                    raw["name"] as String,
                    raw["date"] as String,
                    raw["start_date"] as String,
                    raw["end_date"] as String,
                    (raw["target"] as Double).toInt(),
                    raw["incentive"] as Double
                )
            }

            else -> throw IllegalStateException("Could not identify type: $type")
        }
    }

    @ToJson
    fun toJson(income: IncomeDetails): String {
        throw UnsupportedOperationException()
    }
}