package com.bluebrand.beedriver.data.repository.driver

import com.bluebrand.beedriver.data.model.*
import com.bluebrand.beedriver.data.model.mapper.ArCrHistoryMapper
import com.bluebrand.beedriver.data.repository.Repository
import com.bluebrand.beedriver.data.source.local.cache.AppCache
import com.bluebrand.beedriver.data.source.remote.model.driver.profile.ProfileData
import com.bluebrand.beedriver.data.source.remote.model.driver.status.SetStatusRequest
import com.bluebrand.beedriver.data.source.remote.model.driver.status.SetStatusResponse
import com.bluebrand.beedriver.data.source.remote.service.driver.IDriverApiService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class DriverRepositoryImpl @Inject constructor(
    private val appCache: AppCache,
    private val driverApiService: IDriverApiService,
    private val arCrHistoryMapper: ArCrHistoryMapper
) : IDriverRepository, Repository() {

    override fun isMapDownloaded(): Boolean {
        return appCache.isMapDownloaded()
    }

    override fun setMapDownloaded(isDownloaded: Boolean) {
        appCache.setMapDownloaded(isDownloaded)
    }

    override fun getSupportPhoneNumber(): Flow<String?> {
        return flowOf(appCache.getSupportPhoneNumber())
    }

    override fun setStatus(status: Boolean): Flow<SetStatusResponse> {
        return driverApiService.setStatus(
            SetStatusRequest(status)
        )
    }

    override fun setLastOrder(status: Boolean): Flow<SetStatusResponse> {
        return driverApiService.setLastOrder(
            SetStatusRequest(status)
        )
    }

    override fun setAutoAccept(status: Boolean): Flow<SetStatusResponse> {
        return driverApiService.setAutoAccept(
            SetStatusRequest(status)
        )
    }

    override fun getTransactions(): Flow<TransactionsInfo> {
        return driverApiService.getTransactions()
            .map { it.data!! }
    }

    override fun getArCrHistory(): Flow<ArCrHistory> {
        return driverApiService.getArCrHistory()
            .map { arCrHistoryMapper.map(it.data) }
    }

    override fun getActiveIncentives(): Flow<List<Incentive>> {
        return driverApiService.getActiveIncentives()
            .map { it.data?.incentiveList ?: emptyList() }
    }

    override fun getIncentiveHistory(): Flow<List<Incentive>> {
        return driverApiService.getIncentiveHistory()
            .map { it.data?.incentiveList ?: emptyList() }
    }

    override fun getDailyIncomeHistory(): Flow<List<DailyIncomeInfo>> {
        return driverApiService.getDailyIncomeHistory()
            .map { it.data?.incomeList ?: emptyList() }
    }

    override fun getWeeklyIncomeHistory(): Flow<List<WeeklyIncomeInfo>> {
        return driverApiService.getWeeklyIncomeHistory()
            .map { it.data?.incomeList ?: emptyList() }
    }

    override fun getMonthlyIncomeHistory(): Flow<List<MonthlyIncomeInfo>> {
        return driverApiService.getMonthlyIncomeHistory()
            .map { it.data?.incomeList ?: emptyList() }
    }

    override fun getRankGuide(): Flow<List<RankInfo>> {
        return driverApiService.getRankGuide()
            .map { it.data ?: emptyList() }
    }

    override fun getProfileData(): Flow<ProfileData> {
        return driverApiService.getProfileData()
            .map {
                saveDriverInfo(it.data?.driverInfo)
                it.data!!
            }
    }

    override fun getDriverInfo(): Flow<DriverInfo?> {
        return appCache.getDriverInfo()
    }

    override fun saveDriverInfo(driverInfo: DriverInfo?) {
        appCache.saveDriverInfo(driverInfo)
    }
}