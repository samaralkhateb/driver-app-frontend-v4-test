package com.bluebrand.beedriver.data.source.remote.service.registration

import com.bluebrand.beedriver.data.provider.remote.api.IApiProvider
import com.bluebrand.beedriver.data.source.remote.model.registration.GetAppInfoResponse
import com.bluebrand.beedriver.data.source.remote.model.registration.GetCitesResponse
import com.bluebrand.beedriver.data.source.remote.model.registration.GetVehicleTypesResponse
import com.bluebrand.beedriver.data.source.remote.model.registration.signup.SignupRequest
import com.bluebrand.beedriver.data.source.remote.model.registration.signup.SignupResponse
import com.bluebrand.beedriver.data.source.remote.service.BaseApiService
import com.bluebrand.beedriver.data.source.remote.service.IRetrofitService
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class RegistrationApiServiceImpl @Inject constructor(
    retrofitService: IRetrofitService,
    apiProvider: IApiProvider
) : IRegistrationApiService, BaseApiService(retrofitService, apiProvider) {

    override fun getAppInfo(): Flow<GetAppInfoResponse> {
        return makeRequest {
            retrofitService.getAppInfo()
        }
    }

    override fun getCites(): Flow<GetCitesResponse> {
        return makeRequest {
            retrofitService.getCites()
        }
    }

    override fun getVehicleTypes(): Flow<GetVehicleTypesResponse> {
        return makeRequest {
            retrofitService.getVehicles()
        }
    }

    override fun signup(signupRequest: SignupRequest): Flow<SignupResponse> {
        return makeRequest {
            retrofitService.signup(signupRequest)
        }
    }
}