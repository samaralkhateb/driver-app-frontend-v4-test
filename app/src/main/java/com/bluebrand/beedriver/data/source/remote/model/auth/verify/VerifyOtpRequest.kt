package com.bluebrand.beedriver.data.source.remote.model.auth.verify

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VerifyOtpRequest(
    @Json(name = "mobile") val phoneNumber: String,
    @Json(name = "otp") val otp: String,
    @Json(name = "os_version") val androidVersion: String,
    @Json(name = "manufacturer") val manufacturer : String,
    @Json(name = "mobile_model") val mobileModel: String,
    @Json(name = "fcm_token") val fcmToken: String?
)