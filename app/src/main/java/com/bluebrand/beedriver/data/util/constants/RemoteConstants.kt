package com.bluebrand.beedriver.data.util.constants


object RemoteConstants {
    const val BASE_URL = "https://dev-delivery-v4.lemonilab.com/api/"
  
    object ApiEndPoints {
        const val LOGIN = "login"
        const val VERIFY_OTP = "verify"
        const val RESEND_OTP = "resend-otp"
        const val INFO = "info"
        const val CITY = "city"
        const val VEHICLES = "vehicles"
        const val SIGNUP = "signup"
        const val STATUS = "driver/set/status"
        const val LAST_ORDER = "driver/set/last-order"
        const val AUTO_ACCEPT = "driver/set/auto-accept"
        const val TRANSACTIONS = "driver/transaction"
        const val AR_CR_HISTORY = "driver/ar-cr-history"
        const val INCENTIVE = "incentive"
        const val INCOME_HISTORY = "driver/income-history"
        const val RANK_GUIDE = "guid-rank"
        const val RANK_DETAILS = "driver/rank-details"
        const val DRIVER_ORDERS = "driver/orders"
        const val ACCEPT_ORDER = "bill/accept"
        const val DECLINE_ORDER = "bill/decline"
        const val ARRIVED_TO_RESTAURANT = "driver/set/reached-restaurant"
        const val PICKUP_ORDER = "bill/pickup"
        const val ORDER_PAID = "bill/paid"
    }

    object Headers {
        const val APP_VERSION_HEADER = "version"
        const val LANGUAGE_HEADER = "lang"
        const val TOKEN_HEADER = "token"
        const val MOBILE_HEADER = "mobile"
    }

    object OSRM {
        const val ROUTE_HOST = "https://osrm-auth.beeorder.com/osrm/"
    }

    object Socket {
        const val SOCKET_HOST = "https://dev-sct.lemonilab.com/" 

        const val EVENT_UPDATE_DRIVER_LOCATION = "update_driver_location"
        const val EVENT_UPDATE_DRIVER_INFORMATION = "update_driver_information"
        const val EVENT_ORDER_RECEIVED = "receive_order_confirmation"
        const val EVENT_SOCKET_NOTIFICATION = "socket_notification"
        const val EVENT_NEW_ORDER_DISPATCHED = "receive_new_order"
    }
}
