package com.bluebrand.beedriver.data.source.remote.model.base

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
open class BaseResponse<T> {
    @Json(name = "data")
    var data: T? = null

    @Json(name = "code")
    var statusCode: String? = null

    @Json(name = "message")
    var message: String? = null

    @Json(name = "success")
    var success: Boolean? = null

    fun isSuccessful(): Boolean {
        return success == true
    }
}