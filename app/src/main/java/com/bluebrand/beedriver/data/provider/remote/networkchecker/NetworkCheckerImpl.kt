package com.bluebrand.beedriver.data.provider.remote.networkchecker

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.net.InetAddress
import javax.inject.Inject

@Suppress("DEPRECATION")
class NetworkCheckerImpl @Inject constructor(
    @ApplicationContext private val context: Context
) : NetworkChecker {
    private val connectivityManager by lazy {
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    override suspend fun isConnected(): Boolean =
        thereIsActiveNetwork() && isInternetAvailable()

    private suspend fun isInternetAvailable() = withContext(Dispatchers.IO) {
        try {
            InetAddress.getByName("www.google.com")
            true
        } catch (e: Exception) {
            false
        }
    }

    private fun thereIsActiveNetwork(): Boolean {
        return try {
            connectivityManager.run {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    val networkCapabilities = getNetworkCapabilities(activeNetwork)
                    listOf(
                        networkCapabilities!!.hasTransport(NetworkCapabilities.TRANSPORT_WIFI),
                        networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR),
                        networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET),
                        networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH),
                        networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_VPN)
                    ).any { it }
                } else {
                    activeNetworkInfo!!.isConnectedOrConnecting
                }
            }
        } catch (e: Exception) {
            false
        }
    }
}